
package ua.regionit.planvy;

public class Config {

    public static final boolean DEBUG = true;
    public static final String API_URL_1 = "https://mozidev.parseapp.com/api/";
    public static final String API_URL = API_URL_1;
    public static String APP_UUID = "";
    public static String COOKIES;

    public static boolean isMockupRemoteResponse() {
        return false;
    }

    public interface LoginType {
        int UNDEFENIT = -1;
        int PLANVY_REGISTRATION = 0;
        int FACEBOOK = 1;
        int TWITTER = 2;
    }
}
