package ua.regionit.planvy.util;

import android.graphics.Bitmap;

public class CharityListModel {
	
	String charityDescription, title, email, photoThumbnail, photoFull;
	
	    public CharityListModel(String charityDescription,  String title, String email, String photoFull) {
	    	this.charityDescription=  charityDescription;
	    	this.title=	title;
	    	this.email= email;
	    	this.photoFull=  photoFull;
	    }

		public String getCharityDescription() {
			return charityDescription;
		}

		public String getTitle() {
			return title;
		}

		public String getEmail() {
			return email;
		}

		public String getPhotoFull() {
			return photoFull;
		}
	   
}
