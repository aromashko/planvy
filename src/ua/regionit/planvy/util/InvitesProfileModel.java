package ua.regionit.planvy.util;

import android.graphics.Bitmap;

public class InvitesProfileModel implements Comparable<InvitesProfileModel> {
	
	    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + amountPaid;
		result = prime * result
				+ ((charityTitle == null) ? 0 : charityTitle.hashCode());
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((isPaid == null) ? 0 : isPaid.hashCode());
		result = prime * result + ((isRead == null) ? 0 : isRead.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result
				+ ((objectId == null) ? 0 : objectId.hashCode());
		result = prime * result + ((toPhone == null) ? 0 : toPhone.hashCode());
		result = prime * result
				+ ((urlToPhoto == null) ? 0 : urlToPhoto.hashCode());
		result = prime * result
				+ ((usersPhoto == null) ? 0 : usersPhoto.hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InvitesProfileModel other = (InvitesProfileModel) obj;
		if (amountPaid != other.amountPaid)
			return false;
		if (charityTitle == null) {
			if (other.charityTitle != null)
				return false;
		} else if (!charityTitle.equals(other.charityTitle))
			return false;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (isPaid == null) {
			if (other.isPaid != null)
				return false;
		} else if (!isPaid.equals(other.isPaid))
			return false;
		if (isRead == null) {
			if (other.isRead != null)
				return false;
		} else if (!isRead.equals(other.isRead))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (objectId == null) {
			if (other.objectId != null)
				return false;
		} else if (!objectId.equals(other.objectId))
			return false;
		if (toPhone == null) {
			if (other.toPhone != null)
				return false;
		} else if (!toPhone.equals(other.toPhone))
			return false;
		if (urlToPhoto == null) {
			if (other.urlToPhoto != null)
				return false;
		} else if (!urlToPhoto.equals(other.urlToPhoto))
			return false;
		if (usersPhoto == null) {
			if (other.usersPhoto != null)
				return false;
		} else if (!usersPhoto.equals(other.usersPhoto))
			return false;
		return true;
	}

		private String name, email, description, date, urlToPhoto, toPhone, objectId, charityTitle,dateOfMessage ;
	    Bitmap usersPhoto;
	    Boolean isPaid=false, isRead =false;
	    int amountPaid;
	    public InvitesProfileModel(String name,  String description, String date, String urlToPhoto, Boolean isPaid, String toPhone, Boolean isRead, String objectId, String charityTitle, String dateOfMessage, int amountPaid) {
	        this.name = name;
	        this.description = description;
	        this.date = date;
	        this.urlToPhoto =urlToPhoto;
	        this.isPaid = isPaid;
	        this.toPhone = toPhone;
	        this.isRead = isRead;
	        this.objectId = objectId;
	        this.charityTitle = charityTitle;
	        this.amountPaid = amountPaid;
	        this.dateOfMessage = dateOfMessage;
	    }
	    
	    
	    
	    public String getDateOfMessage() {
			return dateOfMessage;
		}



		public String getCharityTitle() {
			return charityTitle;
		}



		public int getAmountPaid() {
			return amountPaid;
		}



		public String getObjectId() {
			return objectId;
		}


		public void setObjectId(String objectId) {
			this.objectId = objectId;
		}


		public Boolean getIsRead() {
			return isRead;
		}


		public void setIsRead(Boolean isRead) {
			this.isRead = isRead;
		}


		public String getToPhone() {
			return toPhone;
		}


		public void setToPhone(String toPhone) {
			this.toPhone = toPhone;
		}


		public Boolean getIsPaid() {
			return isPaid;
		}


		public void setIsPaid(Boolean isPaid) {
			this.isPaid = isPaid;
		}


		public String getUrlToPhoto() {
			return urlToPhoto;
		}

		public void setUrlToPhoto(String urlToPhoto) {
			this.urlToPhoto = urlToPhoto;
		}

		public Bitmap getUsersPhoto() {
			return usersPhoto;
		}

		public void setUsersPhoto(Bitmap usersPhoto) {
			this.usersPhoto = usersPhoto;
		}

		public String getDate() {
			return date;
		}

		public void setDate(String date) {
			this.date = date;
		}

		public String getName() {
	        return name;
	    }

	    public void setName(String name) {
	        this.name = name;
	    }
	    
	    /*public String getEmail() {
	        return email;
	    }

	    public void setEmail(String email) {
	        this.email = email;
	    }
*/
		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}



		@Override
		public int compareTo(InvitesProfileModel another) {
			// TODO Auto-generated method stub
			 return getDate().compareTo(another.getDate());
		}
		
		

}
