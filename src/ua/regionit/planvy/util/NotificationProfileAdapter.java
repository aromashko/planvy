package ua.regionit.planvy.util;



import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.holoeverywhere.LayoutInflater;
import org.holoeverywhere.widget.ArrayAdapter;
import org.holoeverywhere.widget.LinearLayout;
import org.holoeverywhere.widget.TextView;

import ua.regionit.planvy.R;
import ua.regionit.planvy.ui.ProfileActivity;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;




	public class NotificationProfileAdapter extends ArrayAdapter<InvitesProfileModel> {
		int value_width_height;
	    Context mContext;
	    Bitmap resizedBitmap;
	    int sdk;
	    LinearLayout item_image_bg;
	    List<String> displayedImages;
	    String time_ago;
	    ImageLoader imageLoader;
	    DisplayImageOptions options;
	    Bitmap dnt_cellmask_resized_gray, dnt_cellmask_resized, dnt_cellmask, dnt_cellmask_gray;
	    
	    
	    int mResourse=0;
	    
	    public NotificationProfileAdapter(Context context) {
	        super(context, R.layout.notification_drop_down_list_item, android.R.layout.simple_list_item_1, new ArrayList<InvitesProfileModel>());
	        mContext = context;
	        imageLoader = ImageLoader.getInstance();
	    	options = new DisplayImageOptions.Builder()
	    	                                       .showImageOnLoading(R.drawable.prf_cell_icon_placeholder)
	    	                                       .showImageForEmptyUri(R.drawable.prf_cell_icon_placeholder)
	    	                                       .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
	    	                                       .cacheInMemory(true)
	    	                                       .cacheOnDisc(true)
	    	                                       .build();
	    }
	   
	    private class ViewHolder {
			public TextView nickname_drop_down_list_item, send_time_drop_down_list_item;
			public ImageView avatar_drop_down_list_item;
			LinearLayout lay_avatar_drop_down_list_item, lay_information_drop_down_list, lay_arrow_drop_down_list, lay_item_drop_down_list;
		}
	    
	    @SuppressWarnings("deprecation")
		@SuppressLint("NewApi")
		@Override
	    public View getView(int position, View convertView, ViewGroup parent) {
	    	sdk = android.os.Build.VERSION.SDK_INT;
	    	final ViewHolder holder;
	    	InvitesProfileModel p = getItem(position);
	        if (convertView == null) {
	        	 LayoutInflater li = (LayoutInflater) getContext().getSystemService(
	                     Context.LAYOUT_INFLATER_SERVICE);
	        	 convertView = li.inflate(R.layout.notification_drop_down_list_item, parent, false);
	        	holder = new ViewHolder();
				holder.nickname_drop_down_list_item = (TextView) convertView.findViewById(R.id.nickname_drop_down_list_item);
				holder.send_time_drop_down_list_item = (TextView) convertView.findViewById(R.id.send_time_drop_down_list_item);
				holder.avatar_drop_down_list_item = (ImageView) convertView.findViewById(R.id.avatar_drop_down_list_item);
				holder.lay_avatar_drop_down_list_item = (LinearLayout) convertView.findViewById(R.id.lay_avatar_drop_down_list_item);
				holder.lay_item_drop_down_list =(LinearLayout) convertView.findViewById(R.id.lay_item_drop_down_list);
				holder.lay_information_drop_down_list = (LinearLayout) convertView.findViewById(R.id.lay_information_drop_down_list);
				holder.lay_arrow_drop_down_list  = (LinearLayout) convertView.findViewById(R.id.lay_arrow_drop_down_list);
				convertView.setTag(holder);
	        }else {
				holder = (ViewHolder) convertView.getTag();
			}
	        
        	holder.nickname_drop_down_list_item.setText(p.getName());
        	String date = p.getDate();
        	switch(date.charAt(date.length()-1)){
        	
        	case 'h':
        		time_ago =date.substring(0, date.length()-1) + " hours ago";
        	break;
        	case 'd':
        		time_ago =date.substring(0, date.length()-1) + " days ago";
        	break;
        	case 'm':
        		time_ago =date.substring(0, date.length()-1) + " minutes ago";
        	break;
        	
        	}
        	
	    	
        	
        	holder.send_time_drop_down_list_item.setText("Sent invite for you " +time_ago);
        	
        	
        	
        	value_width_height = (int) ProfileActivity.dipToPixels(mContext, 50);
        	if (!p.getUrlToPhoto().equals("")){
        		
            	Bitmap dnt_cellmask = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.dnt_cellmask);
            	Bitmap dnt_cellmask_gray = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.dnt_cellmask_gray);
            	dnt_cellmask_resized = Bitmap.createScaledBitmap(dnt_cellmask, value_width_height, value_width_height, true);
            	dnt_cellmask_resized_gray = Bitmap.createScaledBitmap(dnt_cellmask_gray, value_width_height, value_width_height, true);
            	holder.avatar_drop_down_list_item.setImageBitmap(dnt_cellmask_resized);
        	imageLoader.loadImage(p.getUrlToPhoto(), options, new SimpleImageLoadingListener() {
        		
        	    @Override
        	    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
        	    	
        	    	Bitmap bitmap = Support.optimizeBitmap(loadedImage);
        	    	resizedBitmap=Bitmap.createScaledBitmap(bitmap, value_width_height, value_width_height, true);
        	    	Drawable drawable = new BitmapDrawable( mContext.getResources(),resizedBitmap);
        	    	if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
        	    		holder.lay_avatar_drop_down_list_item.setBackgroundDrawable(drawable);
				            } else {
				            	holder.lay_avatar_drop_down_list_item.setBackground(drawable);
				            }
        	    }
        	});
        	if (p.isRead){
        		holder.lay_item_drop_down_list.setBackgroundColor(Color.WHITE);
        	holder.lay_information_drop_down_list.setBackgroundColor(Color.WHITE);
        	holder.lay_arrow_drop_down_list.setBackgroundColor(Color.WHITE);
        	holder.avatar_drop_down_list_item.setImageBitmap(dnt_cellmask_resized);
        	}
        	else{
        		holder.lay_item_drop_down_list.setBackgroundColor(Color.parseColor("#dcdcdc"));
        		holder.avatar_drop_down_list_item.setImageBitmap(dnt_cellmask_resized_gray);
        		holder.lay_information_drop_down_list.setBackgroundColor(Color.parseColor("#dcdcdc"));
        		holder.lay_arrow_drop_down_list.setBackgroundColor(Color.parseColor("#dcdcdc"));
        	}
        	}
        	else{
        		if (p.isRead){
            		holder.lay_item_drop_down_list.setBackgroundColor(Color.WHITE);
            	holder.lay_information_drop_down_list.setBackgroundColor(Color.WHITE);
            	holder.lay_arrow_drop_down_list.setBackgroundColor(Color.WHITE);
            	holder.avatar_drop_down_list_item.setImageBitmap(dnt_cellmask_resized);
            	}
            	else{
            		holder.lay_item_drop_down_list.setBackgroundColor(Color.parseColor("#dcdcdc"));
            		holder.avatar_drop_down_list_item.setImageBitmap(dnt_cellmask_resized_gray);
            		holder.lay_information_drop_down_list.setBackgroundColor(Color.parseColor("#dcdcdc"));
            		holder.lay_arrow_drop_down_list.setBackgroundColor(Color.parseColor("#dcdcdc"));
            	}
        		holder.avatar_drop_down_list_item.setBackgroundResource(R.drawable.prf_cell_icon_placeholder);
    	    }
	        return convertView;
	    		    	
	    }
	    
	    public Bitmap resizeWhiteBitmap(Bitmap initial, int position){
	    	initial = BitmapFactory.decodeResource(mContext.getResources(),
                    R.drawable.dnt_cellmask);
        	return Bitmap.createScaledBitmap(initial, value_width_height, value_width_height, true);
        	
	    }
	    
	    public Bitmap resizeGrayBitmap(Bitmap initial, int position){
	    	initial = BitmapFactory.decodeResource(mContext.getResources(),
                    R.drawable.dnt_cellmask_gray);
	    	return Bitmap.createScaledBitmap(initial, value_width_height, value_width_height, true);
	    }
  
}
