package ua.regionit.planvy.util;



import java.util.ArrayList;
import java.util.List;

import org.holoeverywhere.LayoutInflater;
import org.holoeverywhere.widget.ArrayAdapter;
import org.holoeverywhere.widget.LinearLayout;
import org.holoeverywhere.widget.TextView;

import ua.regionit.planvy.R;
import ua.regionit.planvy.ui.ProfileActivity;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;




	public class InvitesProfileAdapter extends ArrayAdapter<InvitesProfileModel> {
		int value_width_height;
	    TextView inv_nickname, item_message, tv_time_gone;
	    ImageView item_image;
	    Context mContext;
	    Bitmap resizedBitmap;
	    int sdk;
	    LinearLayout item_image_bg;
	    List<String> displayedImages;	    
	    public static final int SEND_BY_ME = 1;
	    public static final int SEND_TO_ME = 2;
	    int mResourse=0;
	    
	    public InvitesProfileAdapter(Context context) {
	        super(context, R.layout.notification_list_item_send_by_me, android.R.id.text1, new ArrayList<InvitesProfileModel>());
	        mContext = context;
	    }
	    public InvitesProfileAdapter(Context context, int resourse) {
	        super(context, R.layout.notification_list_item_send_to_me,  android.R.id.text1, new ArrayList<InvitesProfileModel>());
	        mResourse = SEND_TO_ME;
	        mContext = context;
	    }
	    private class ViewHolder {
			public TextView inv_nickname, item_message, tv_time_gone;
			public ImageView item_image, img_arrow_profile;
			LinearLayout item_image_bg, lay_cloud;
		}
	    
	   
	    
	    
	    @SuppressWarnings("deprecation")
		@SuppressLint("NewApi")
		@Override
	    public View getView(int position, View convertView, ViewGroup parent) {
	    	sdk = android.os.Build.VERSION.SDK_INT;
	    	final ViewHolder holder;
	        if (convertView == null) {
	        	 LayoutInflater li = (LayoutInflater) getContext().getSystemService(
	                     Context.LAYOUT_INFLATER_SERVICE);
	        	 if (mResourse == SEND_TO_ME){
	        	 convertView = li.inflate(R.layout.notification_list_item_send_to_me, parent, false);}
	        	 else{
	        		 convertView = li.inflate(R.layout.notification_list_item_send_by_me, parent, false);
	        	 }
	        	//convertView = super.getView(position, convertView, parent);
	        	holder = new ViewHolder();
				holder.inv_nickname = (TextView) convertView.findViewById(R.id.nickname_profile_item);
				holder.item_message = (TextView) convertView.findViewById(R.id.item_message);
				holder.tv_time_gone = (TextView) convertView.findViewById(R.id.tv_time_gone);
				holder.item_image = (ImageView) convertView.findViewById(R.id.item_image);
				holder.item_image_bg = (LinearLayout) convertView.findViewById(R.id.item_image_bg);
				holder.img_arrow_profile = (ImageView) convertView.findViewById(R.id.img_arrow_profile);
				holder.lay_cloud = (LinearLayout) convertView.findViewById(R.id.lay_cloud);
				convertView.setTag(holder);
	        }else {
				holder = (ViewHolder) convertView.getTag();
			}
	        
	        InvitesProfileModel p = getItem(position);
	        if (p.getToPhone().equals("")){
        	holder.inv_nickname.setText(p.getName());}
	        else{
	        	holder.inv_nickname.setText(p.getToPhone());
	        }
	        	holder.item_message.setText(p.getDescription());
	       
        	holder.tv_time_gone.setText(p.getDate());
        	if (p.getIsPaid()){
        		if (mResourse ==SEND_TO_ME){
        			holder.img_arrow_profile.setImageResource(R.drawable.prf_red_arrow);
        			if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
        				holder.lay_cloud.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.red_cloud));
				            } else {
				            	holder.lay_cloud.setBackground(mContext.getResources().getDrawable(R.drawable.red_cloud));
				            }
        			
        		}
        		else{
        			holder.img_arrow_profile.setImageResource(R.drawable.prf_arrow_blue);
        			if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
        				holder.lay_cloud.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.blue_cloud));
				            } else {
				            	holder.lay_cloud.setBackground(mContext.getResources().getDrawable(R.drawable.blue_cloud));
				            }
        		}
        	}
        	else{
        		holder.img_arrow_profile.setImageResource(R.drawable.prf_arrow_gray);
        		if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
    				holder.lay_cloud.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.gray_cloud));
			            } else {
			            	holder.lay_cloud.setBackground(mContext.getResources().getDrawable(R.drawable.gray_cloud));
			            }
        	}
        	ImageLoader imageLoader = ImageLoader.getInstance();
        	DisplayImageOptions options = new DisplayImageOptions.Builder()
        	                                       .showImageOnLoading(R.drawable.prf_cell_icon_placeholder)
        	                                       .showImageForEmptyUri(R.drawable.prf_cell_icon_placeholder)
        	                                       .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
        	                                       .cacheInMemory(true)
        	                                       .cacheOnDisc(true)
        	                                       .displayer(new FadeInBitmapDisplayer(300))
        	                                       .build();
        	
        	value_width_height = (int) ProfileActivity.dipToPixels(mContext, 50);
        	if (!p.getUrlToPhoto().equals("")){
        		
            	Bitmap dnt_cellmask = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.dnt_cellmask);
            	Bitmap dnt_cellmask_resized = Bitmap.createScaledBitmap(dnt_cellmask, value_width_height, value_width_height, true);
            	holder.item_image.setImageBitmap(dnt_cellmask_resized);
        	imageLoader.loadImage(p.getUrlToPhoto(), options, new SimpleImageLoadingListener() {
        		
        	    @Override
        	    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
        	    	Bitmap bitmap = Support.optimizeBitmap(loadedImage);
        	    	resizedBitmap=Bitmap.createScaledBitmap(bitmap, value_width_height, value_width_height, true);
        	    	Drawable drawable = new BitmapDrawable( mContext.getResources(),resizedBitmap);
        	    	if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
        	    		holder.item_image_bg.setBackgroundDrawable(drawable);
				            } else {
				            	holder.item_image_bg.setBackground(drawable);
				            }
        	    }
        	});
        	}
        	else{
        		holder.item_image_bg.setBackgroundColor(Color.WHITE);
        		holder.item_image.setImageResource(R.drawable.prf_cell_icon_placeholder);
        	}
        	
        	/*convertView.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					ProfileItemDialog profileItemDialog = ProfileItemDialog.newInstance(convertView.get, "Sarah was late today for meeting with a new customer", "She donated 10$ animal sheltes for being late", ProfileItemDialog.ButtonsLayout.ONE_BUTTON);
					profileItemDialog.show((ProfileActivity) mContext);
					
				}
			});*/
        	
	        return convertView;
	    		    	
	    }
	   
	    /*private class AnimateFirstDisplayListener extends SimpleImageLoadingListener {

			final List<String> displayedImages = Collections.synchronizedList(new LinkedList<String>());

			@SuppressLint("NewApi")
			@SuppressWarnings("deprecation")
			@Override
			public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
				if (loadedImage != null) {
					resizedBitmap=Bitmap.createScaledBitmap(loadedImage, value_width_height, value_width_height, true);
        	    	Drawable drawable = new BitmapDrawable( mContext.getResources(),resizedBitmap);
        	    	if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
        	    		
				    		item_image_bg.setBackgroundDrawable(drawable);
				    		//item_image.setImageBitmap(loadedImage);
				            } else {
				            	item_image_bg.setBackground(drawable);
				            	//item_image.setImageBitmap(loadedImage);
				            }
					
					boolean firstDisplay = !displayedImages.contains(imageUri);
					if (firstDisplay) {
						FadeInBitmapDisplayer.animate(item_image_bg, 500);
						displayedImages.add(imageUri);
					}
				}
			}
		}*/
	   		
	    
	    	    
}
