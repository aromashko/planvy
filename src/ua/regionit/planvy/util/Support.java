package ua.regionit.planvy.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.DisplayMetrics;
import android.util.TypedValue;

public class Support {
	public static boolean isNetworkConnected(Context context) {
		  ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		  NetworkInfo ni = cm.getActiveNetworkInfo();
		  if (ni == null) {
		   // There are no active networks.
		   return false;
		  } else
		   return true;
		 }
	
	public static Bitmap optimizeBitmap(Bitmap bmp){
		Bitmap bitmap;
		if (bmp.getWidth() >= bmp.getHeight()){

			bitmap = Bitmap.createBitmap(
    				bmp, 
    				bmp.getWidth()/2 - bmp.getHeight()/2,
    		     0,
    		     bmp.getHeight(), 
    		     bmp.getHeight()
    		     );

    		}else{

    			bitmap = Bitmap.createBitmap(
    					bmp,
    		     0, 
    		     bmp.getHeight()/2 - bmp.getWidth()/2,
    		     bmp.getWidth(),
    		     bmp.getWidth() 
    		     );
    		}
		return bitmap;
	}
	
	public static float dipToPixels(Context context, float dipValue) {
	    DisplayMetrics metrics = context.getResources().getDisplayMetrics();
	    return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dipValue, metrics);
	}
}
