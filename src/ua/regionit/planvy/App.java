
package ua.regionit.planvy;

import java.util.ArrayList;

import org.acra.ACRA;
import org.acra.annotation.ReportsCrashes;
import org.holoeverywhere.FontLoader;
import org.holoeverywhere.FontLoader.Font;
import org.holoeverywhere.FontLoader.FontCollector;
import org.holoeverywhere.FontLoader.RawFont;
import org.holoeverywhere.app.Application;

import ua.regionit.planvy.ui.ContactModel;
import ua.regionit.planvy.ui.LoginActivity;
import ua.regionit.planvy.ui.SplashActivity;
import android.content.Context;
import android.graphics.Bitmap.CompressFormat;
import android.os.AsyncTask;
import android.util.Log;

import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseFacebookUtils;
import com.parse.ParseUser;
import com.parse.PushService;
@ReportsCrashes(formKey = "dHNYNzM4RVNaRHMwNVdCdFVYbEFnbkE6MA")
public class App extends Application {

    
    private static App sInstance;
    private static final Font sJosefinSansLight;
    private static final Font sMuseoSans300;
    private static final FontCollector sMuseoSans;
    private static final Font sMuseoSans900;
    private static final Font sMuseoSans500;
    private static final FontCollector sDefaultFont;
    
    private static ArrayList<ContactModel> mContacts;
    //PushCallback pushCallback;
    
    private ArrayList<Object> mObjects = new ArrayList<Object>();

    static {
        sMuseoSans300 = new RawFont(R.raw.museo_sans_300).setFontStyle(FontLoader.registerTextStyle("300"));
        sMuseoSans500 = new RawFont(R.raw.museo_sans_500).setFontStyle(FontLoader.registerTextStyle("500"));
        sMuseoSans900 = new RawFont(R.raw.museo_sans_900).setFontStyle(FontLoader.registerTextStyle("900"));
        sJosefinSansLight = new RawFont(R.raw.josefin_sans_std_regular).setFontStyle(FontLoader.TEXT_STYLE_NORMAL)
                .setFontFamily("josefin");

        sMuseoSans = new FontCollector();
        sMuseoSans.setFontFamily("museosans");
        sMuseoSans.register(sMuseoSans900).asDefaultFont();
        sMuseoSans.register(sMuseoSans300);
        sMuseoSans.register(sMuseoSans500);

        sDefaultFont = new FontCollector();
        sDefaultFont.register(sMuseoSans);
        sDefaultFont.register(sJosefinSansLight).asDefaultFont();
        //sDefaultFont.register(FontLoader.ROBOTO);
       // FontLoader.setDefaultFont(sDefaultFont);
    }

    public static App get() {
        return sInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        ACRA.init(this);
        sInstance = this;
        mContacts = new ArrayList<ContactModel>();
        /*CookieSyncManager.createInstance(this);
        CookieManager.getInstance().removeAllCookie();
        mQueue = Volley.newRequestQueue(this, new HurlStack());*/
        
        Parse.initialize(this, "AkRIyTFqZGBbIlLApN4bcdiSZEPFE6PwW6ypYkud", "LY90dEJwwvAcmrRYJVehMIUEHQQutgEpsAMZVXYE");
        Parse.setLogLevel(Parse.LOG_LEVEL_VERBOSE);
        //PushService.setDefaultPushCallback(this, SplashActivity.class);
        //pushCallback = new PushCallback();
        //pushCallback.execute();
        PushService.setDefaultPushCallback(App.this, SplashActivity.class);
       ParseFacebookUtils.initialize(getString(R.string.app_id)); // Looks like this line sometimes costs too much CPU time. Moved to splash activity.
        // Moving to splash add some problems
        //ParseUser.enableAutomaticUser();
		ParseACL defaultACL = new ParseACL();
		// If you would like all objects to be private by default, remove this line.
		defaultACL.setPublicReadAccess(true);
		//defaultACL.setPublicWriteAccess(true);
		
		ParseACL.setDefaultACL(defaultACL, true);
		
		
		
		initImageLoader(getApplicationContext());
		

    }

    public static void initImageLoader(Context context) {
		// This configuration tuning is custom. You can tune every option, you may tune some of them,
		// or you can create default configuration by
		//  ImageLoaderConfiguration.createDefault(this);
		// method.
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
		.threadPriority(Thread.NORM_PRIORITY - 2)
		.memoryCacheExtraOptions(1280, 720)
		.discCacheExtraOptions(1280, 720, CompressFormat.JPEG, 100, null)
		.discCacheFileNameGenerator(new Md5FileNameGenerator())
		.tasksProcessingOrder(QueueProcessingType.LIFO)
				.build();
		// Initialize ImageLoader with configuration.
		ImageLoader.getInstance().init(config);
		
	}
    
    public void setArrayModel(ArrayList<ContactModel> mArray){
    	//mContacts.clear();
    	mContacts = mArray;
    }
    
    public ArrayList<ContactModel> getArrayModel(){
    	return mContacts;
    }
    
    /*class PushCallback extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
          super.onPreExecute();
          
        }

        @Override
        protected Void doInBackground(Void... params) {
        	PushService.setDefaultPushCallback(App.this, SplashActivity.class);
          return null;
        }

        @Override
        protected void onPostExecute(Void result) {
          super.onPostExecute(result);
          Log.e("PushCallBack", "done");
        }
      }*/
    
       
}
