package ua.regionit.planvy.ui;

import org.holoeverywhere.LayoutInflater;
import org.holoeverywhere.app.Activity;
import org.holoeverywhere.app.Fragment;
import org.holoeverywhere.widget.LinearLayout;
import org.holoeverywhere.widget.TextView;

import ua.regionit.planvy.AppLog;
import ua.regionit.planvy.R;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.facebook.Session;
import com.flurry.android.FlurryAgent;
import com.parse.DeleteCallback;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.PushService;
import com.parse.RefreshCallback;
import com.parse.SaveCallback;



public class SettingsFragment extends Fragment implements OnClickListener {
	
	TextView userNameSetting, emailSettings, phone_settings, tv_status_settings;
	String user_name, email;
	LinearLayout userNameLay ;
	Activity settActivity;
	RelativeLayout emailLay, phone_lay_settings, passwordLay, logOutLay, helpCenterLay, account_status_lay;
	
	ImageView img_back_button_settings;
	ParseUser currentUser;
	Boolean status;
	String phoneNumber;
	public static final String NICKNAME= "nickname";
	public static final String CHANGE_EMAIL_FRAGMENT_TAG= "change_email_fragment_tag";
	public static final String CHANGE_PHONE_FRAGMENT_TAG= "change_phone_fragment_tag";
	public static final String CHANGE_PASSWORD_FRAGMENT_TAG= "change_password_fragment_tag";
	public static final String HELP_CENTER_FRAGMENT_TAG= "help_center_fragment_tag";
	public static final int RESULT_LOG_OUT = 5;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		 View view = inflater.inflate(R.layout.fragment_settings, container, false);
		initUi(view);
		initListener();
		
		currentUser = ParseUser.getCurrentUser();
		user_name = currentUser.getString(NICKNAME);
		try{
		AppLog.e("User Name",currentUser.getString(NICKNAME));
		}
		catch (NullPointerException e){
			ParseUser.logOut();
			Session session = ParseFacebookUtils.getSession();
			if (session != null){
     		session.closeAndClearTokenInformation();
			}
		}
		email = currentUser.getEmail();
		phoneNumber = currentUser.getString("phone");
		userNameSetting.setText(user_name);
		emailSettings.setText(email);
		
		status = currentUser.getBoolean("deactivated");
		if (status){
			tv_status_settings.setText("Active");
		}
		else{
			tv_status_settings.setText("Deactive");
		}
		
		if (phoneNumber!= null && !phoneNumber.equals("")){
			phone_settings.setText(phoneNumber);
			
		}
		
		return view;
	}
	
	



	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		/*int settings_row_height = (view.findViewById(R.id.userNameLay)).getHeight();
		Log.e("Height", settings_row_height+"");
		passwordLay.getLayoutParams().height = settings_row_height;*/
	}



	

	public void initUi(View view){
		userNameSetting = (TextView) view.findViewById(R.id.userNameSetting);
		emailSettings = (TextView) view.findViewById(R.id.emailSettings);
		userNameLay = (LinearLayout) view.findViewById(R.id.userNameLay);
		emailLay = (RelativeLayout) view.findViewById(R.id.emailLay);
		phone_lay_settings = (RelativeLayout) view.findViewById(R.id.phone_lay_settings);
		passwordLay = (RelativeLayout) view.findViewById(R.id.passwordLay);
		helpCenterLay = (RelativeLayout) view.findViewById(R.id.helpCenterLay);
		logOutLay = (RelativeLayout) view.findViewById(R.id.logOutLay);
		img_back_button_settings =  (ImageView) view.findViewById(R.id.img_back_button_settings);
		account_status_lay = (RelativeLayout) view.findViewById(R.id.account_status_lay);
		phone_settings = (TextView) view.findViewById(R.id.phone_settings);
		tv_status_settings = (TextView) view.findViewById(R.id.tv_status_settings);
	}
	
	public void initListener(){
		userNameLay.setOnClickListener(this);
		emailLay.setOnClickListener(this);
		passwordLay.setOnClickListener(this);
		phone_lay_settings.setOnClickListener(this);
		helpCenterLay.setOnClickListener(this);
		logOutLay.setOnClickListener(this);
		img_back_button_settings.setOnClickListener(this);
		account_status_lay.setOnClickListener(this);
	}
	
	@Override
	public void onAttach(Activity activity) {
		settActivity = (SettingsActivity) activity;
		super.onAttach(activity);
	}
	
	public static void callFacebookLogout(Context context) {
	    Session session = ParseFacebookUtils.getSession();
	    if (session != null) {

	        if (!session.isClosed()) {
	        	Log.e("Facebook", "closed");
	            session.closeAndClearTokenInformation();
	            //clear your preferences if saved
	        }
	    } else {
	    	Log.e("Facebook", "open and close");
	        session = new Session(context);
	        Session.setActiveSession(session);

	        session.closeAndClearTokenInformation();
	            //clear your preferences if saved

	    }

	}
	

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.userNameLay:
				
			
			break;
			case R.id.phone_lay_settings:
				Fragment changePhoneFragment = new ChangePhoneFragment();     
		        FragmentTransaction ft_phone = settActivity.getSupportFragmentManager().beginTransaction();
		        ft_phone.replace(R.id.fragment_container,changePhoneFragment, CHANGE_PHONE_FRAGMENT_TAG);
		        ft_phone.addToBackStack(CHANGE_PHONE_FRAGMENT_TAG);
		        ft_phone.commit();
		        FlurryAgent.logEvent("Change Phone", true);
			
			break;
		case R.id.emailLay:
			Fragment changeEmailFragment = new ChangeEmailFragment();     
	        FragmentTransaction ft_email = settActivity.getSupportFragmentManager().beginTransaction();
	        ft_email.replace(R.id.fragment_container,changeEmailFragment, CHANGE_EMAIL_FRAGMENT_TAG);
	        ft_email.addToBackStack(CHANGE_EMAIL_FRAGMENT_TAG);
	        ft_email.commit();
	        FlurryAgent.logEvent("Change Email", true);
			break;
		case R.id.passwordLay:
			Fragment changePasswordFragment = new ChangePasswordFragment();
	        FragmentTransaction ft_password = settActivity.getSupportFragmentManager().beginTransaction();
	        ft_password.replace(R.id.fragment_container,changePasswordFragment, CHANGE_PASSWORD_FRAGMENT_TAG);
	        ft_password.addToBackStack(CHANGE_PASSWORD_FRAGMENT_TAG);
	        ft_password.commit(); 
	        FlurryAgent.logEvent("Change Password", true);
			break;
		case R.id.helpCenterLay:
			Fragment helpCenterFragment = new HelpCenterFragment();
	        FragmentTransaction ft_help_center = settActivity.getSupportFragmentManager().beginTransaction();
	        ft_help_center.replace(R.id.fragment_container,helpCenterFragment, HELP_CENTER_FRAGMENT_TAG);
	        ft_help_center.addToBackStack(HELP_CENTER_FRAGMENT_TAG);
	        ft_help_center.commit();
	        FlurryAgent.logEvent("Help Center", true);
			break;
		case R.id.logOutLay:
			ParseInstallation installation = ParseInstallation.getCurrentInstallation();
            installation.remove("owner");
           
            installation.saveInBackground(new SaveCallback() {
				
				@Override
				public void done(ParseException e) {
					if (e!=null){
						
						e.printStackTrace();
					}
					
				}
			});
			Session session = ParseFacebookUtils.getSession();
			if (session != null){
				Log.e("Facebook Parse","Log out");
				
     		session.closeAndClearTokenInformation();
			}
			Session session2 = Session.getActiveSession();
			if (session2 != null){
				Log.e("Facebook Session","Log out");
				
				session2.closeAndClearTokenInformation();
			}
			//callFacebookLogout(getActivity());
			ParseUser.logOut();
			Log.e("Parse","Log out");
			Intent intent = new Intent(getActivity(), LoginActivity.class);
			//getActivity().setResult(RESULT_LOG_OUT, intent);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); 
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			//settActivity.setResult(RESULT_LOG_OUT, intent);
			startActivity(intent);
			//getActivity().finishFromChild(getActivity());
			settActivity.finish();
			
			
			break;
		case R.id.img_back_button_settings:
			Intent intentProfile = new Intent(settActivity.getBaseContext(), ProfileActivity.class);
			getActivity().setResult(RESULT_LOG_OUT, intentProfile);
			intentProfile.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); 
			intentProfile.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intentProfile);
			settActivity.finish();
			break;
		case R.id.account_status_lay:
			status = !status;
			currentUser.put("deactivated", status);
			currentUser.saveInBackground();
			if (status){
				tv_status_settings.setText("Active");
			}
			else{
				tv_status_settings.setText("Deactive");
			}
			break;
		}
		
	}
	
}
