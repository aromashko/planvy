package ua.regionit.planvy.ui;

import org.holoeverywhere.LayoutInflater;
import org.holoeverywhere.app.Activity;
import org.holoeverywhere.app.Fragment;
import org.holoeverywhere.widget.Button;
import org.holoeverywhere.widget.EditText;
import org.holoeverywhere.widget.ProgressBar;
import org.holoeverywhere.widget.TextView;
import org.holoeverywhere.widget.Toast;

import ua.regionit.planvy.ErrorCode;
import ua.regionit.planvy.R;
import ua.regionit.planvy.ui.dialog.NotifyDialog;
import ua.regionit.planvy.util.Support;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SaveCallback;



public class ChangePhoneFragment extends Fragment implements OnClickListener {
	EditText edt_changed_phone;
	TextView tv_warning_change_phone;
	ProgressBar changephone_progress;
	Button btn_save_new_phone, btn_cancel_new_phone;
	ImageView img_back_button_change_phone;
	public static final String NICKNAME= "nickname";
	Activity settActivity;
	NotifyDialog notifyDialogInternet, notifyDialogError;
	public Dialog progressDialog;
	 String old_phone;
	 ParseUser parseUser;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_change_phone, null);
	    initUi(v);
	    initListener();
	    ParseUser currentUser = ParseUser.getCurrentUser();
	    old_phone = currentUser.getString("phone");
	    if (old_phone!= null && !old_phone.equals("")){
	    edt_changed_phone.setText(old_phone);}
	    edt_changed_phone.addTextChangedListener(new TextWatcher() {
			   public void afterTextChanged(Editable s) {
				   Log.d("after", "called");
				   
				 
				  if (edt_changed_phone.getText().length()<2){
					 
					  edt_changed_phone.setText("+1");
					  edt_changed_phone.setSelection(edt_changed_phone.getText().length());
					  
				   }
				  else{
					   if (edt_changed_phone.getText().charAt(0) != '+'){
						   edt_changed_phone.setText("+1");
						   edt_changed_phone.setSelection(edt_changed_phone.getText().length());
						   }
					   if (edt_changed_phone.getText().charAt(1) !='1'){
						   edt_changed_phone.setText("+1");
						   edt_changed_phone.setSelection(edt_changed_phone.getText().length());
					   }
				   }
			   }

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
			}
			});
		 return v;
	}

	public void initUi(View view){
		edt_changed_phone = (EditText) view.findViewById(R.id.edt_changed_phone);
		btn_save_new_phone = (Button) view.findViewById(R.id.btn_save_new_phone);
		btn_cancel_new_phone = (Button) view.findViewById(R.id.btn_cancel_new_phone);
		tv_warning_change_phone = (TextView) view.findViewById(R.id.tv_warning_change_phone);
		changephone_progress =  (ProgressBar) view.findViewById(R.id.changephone_progress);
		img_back_button_change_phone = (ImageView) view.findViewById(R.id.img_back_button_change_phone);
	}
	
	
	public void initListener(){
		btn_save_new_phone.setOnClickListener(this);
		btn_cancel_new_phone.setOnClickListener(this);
		img_back_button_change_phone.setOnClickListener(this);
	}
	
	@Override
	public void onAttach(Activity activity) {
		settActivity = (SettingsActivity) activity;
		super.onAttach(activity);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()){
		case R.id.btn_save_new_phone:
			if (!Support.isNetworkConnected(settActivity)){
				notifyDialogInternet = NotifyDialog.newInstance(getString(R.string.no_internet_title), getString(R.string.no_internet_text), NotifyDialog.ButtonsLayout.ONE_BUTTON);
		    	notifyDialogInternet.show(settActivity);
				break;
			}
			tv_warning_change_phone.setVisibility(View.GONE);
			//tv_warning_change_phone.setVisibility(View.VISIBLE);
			String phone = edt_changed_phone.getText().toString();
			if (phone.equals("")){
				tv_warning_change_phone.setText("Phone number cannot be blank");
				tv_warning_change_phone.setVisibility(View.VISIBLE);
				break;
			}
			
			if (phone.length()<=11){
				tv_warning_change_phone.setText("Phone number is too short");
				tv_warning_change_phone.setVisibility(View.VISIBLE);
				break;	
			}	
			if (phone.length()>=16){
				tv_warning_change_phone.setText("Phone number is too long");
				tv_warning_change_phone.setVisibility(View.VISIBLE);
				break;
				
			}
			progressDialog = ProgressDialog.show(
		             settActivity, "", "Change Phone...", true);
			//settActivity.onBackPressed();
			parseUser =ParseUser.getCurrentUser();
        	parseUser.put("phone", phone);
        	parseUser.saveInBackground(new SaveCallback() {
				
				@Override
				public void done(ParseException e) {
					progressDialog.dismiss();
					if (e==null){
						settActivity.onBackPressed();
					}
					else{
						 if (old_phone!= null && !old_phone.equals("")){
						parseUser.put("phone", old_phone);
						 }
						 else{
							 parseUser.put("phone", "+1");
						 }
						int err = e.getCode();
            	    	
            	    	Log.d("Error code:", Integer.toString(err));
            	    	
            	    	switch(err){
            	    	case ErrorCode.CONNECTION_FAILED:
            	    		notifyDialogInternet = NotifyDialog.newInstance(getString(R.string.no_internet_title), getString(R.string.no_internet_text), NotifyDialog.ButtonsLayout.ONE_BUTTON);
            		    	notifyDialogInternet.show(settActivity);
            	    		break;
            	    	default:
            	    		edt_changed_phone.setText("+1");
            	    		edt_changed_phone.setSelection(2);
            	    		notifyDialogError =  NotifyDialog.newInstance("", e.getMessage(), NotifyDialog.ButtonsLayout.ONE_BUTTON);
            	    		notifyDialogError.show(settActivity);
            	    		
            	    		//tv_warning_change_phone.setText(e.getMessage());
            	    		//tv_warning_change_phone.setVisibility(View.VISIBLE);
            	    	}
						
						//Toast.makeText(getSupportApplication(), "Ploblem with saving new password", Toast.LENGTH_LONG).show();
						//change_password_progress.setVisibility(View.GONE);
						//Toast.makeText(getSupportApplication(), "Ploblem with saving new telephone", Toast.LENGTH_LONG).show();
						//tv_warning_change_phone.setText(e.getMessage());
						e.printStackTrace();
					}
					
				}
			});
			break;
		case R.id.btn_cancel_new_phone:
			settActivity.onBackPressed();
			break;
		case R.id.img_back_button_change_phone:
			settActivity.onBackPressed();
			break;
		}
	}

}