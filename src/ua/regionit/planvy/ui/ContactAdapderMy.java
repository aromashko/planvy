package ua.regionit.planvy.ui;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import ua.regionit.planvy.R;
import ua.regionit.planvy.ui.views.PinnedSectionListView.PinnedSectionListAdapter;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

public class ContactAdapderMy extends BaseAdapter implements Filterable, PinnedSectionListAdapter{
	
	private Context mContext;
	private LayoutInflater mInflater;
	private ArrayList<ContactModel> mListOfObjects;//���� � ���������� ������� ������������ � ��������
	private ArrayList<ContactModel> mFilteredListOfObjects;//���� � �������������� ����������
	private int HEADER = 0;
	private int CHAILD = 1;
	private Map<String, String> mHashMap, mHashMapNumber;
	private ArrayList<String> mString;
	private HashMap<String,String> iii;
	
	public ContactAdapderMy(Context context,ArrayList<ContactModel> list, Map<String, String> map, Map<String, String> map_number){
		this.mContext = context;
		this.mListOfObjects = list;
		this.mFilteredListOfObjects = list;
		this.mHashMap = map;
		this.mHashMapNumber = map_number;
		this.mString = new ArrayList<String>();
		this.mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	private HashMap<String,String> mMagic(){
		HashMap<String, String> mHashMapFunc = new HashMap<String, String>();
		for(int i=mHashMap.size()+2;i<mListOfObjects.size();i++){
			if(mListOfObjects.get(i).getEmail()!=null){
			if(checkEmail(mListOfObjects.get(i).getEmail())){
				mHashMapFunc.put(mListOfObjects.get(i).getEmail()
						, mListOfObjects.get(i).getName());
			}
			}else{
			if(checkPhone(mListOfObjects.get(i).getPhone())){
				mHashMapFunc.put(mListOfObjects.get(i).getPhone()
						, mListOfObjects.get(i).getName());
				Log.d("PHONE ==>", mListOfObjects.get(i).getPhone());
				Log.d("NAME ==>", mListOfObjects.get(i).getName());
			}
			}
		}
		return mHashMapFunc;
	}
	
	private boolean checkEmail(String email){
		String bool = mHashMap.get(email);
		if(bool != null){
			return true;
		}
		return false;
	}
	
	private boolean checkPhone(String phone){
		String bool = mHashMapNumber.get(phone);
		if(bool != null){
			return true;
		}
		return false;
	}

	@Override
	public int getCount() {
		if (mListOfObjects != null){
		return mListOfObjects.size();
		}
		return 0;
	}

	@Override
	public ContactModel getItem(int position) {
		return mListOfObjects.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View rootView, ViewGroup parent) {
		int viewType =getItemViewType(position);
		if(viewType == HEADER){
			//�������������� � ������� � �������(Planvy Friend, TelephoneContact)
			rootView = mInflater.inflate(R.layout.ad_people_header_item,parent,false);
            ((TextView)rootView.findViewById(R.id.tv_add_people_header_text)).setText(mListOfObjects.get(position).getName());
        }
		if(viewType == CHAILD){
			//������ ������ ��������� ��� ���, View with checkBox's
			rootView = mInflater.inflate(R.layout.ad_people_item,parent,false);
            ((TextView)rootView.findViewById(R.id.tv_add_people_text)).setText(mListOfObjects.get(position).getName());
            TextView tv = (TextView)rootView.findViewById(R.id.tv_add_people_text_2);
            
            if(mListOfObjects.get(position).isPlanvy_friend()){
            	tv.setVisibility(View.VISIBLE);
            }else{
            tv.setVisibility(View.GONE);
            }
            
            if(iii==null){
            	iii = mMagic();
            }
            
            if(mListOfObjects.get(position).getEmail()!=null){
            	tv.setText(iii.get(mListOfObjects.get(position).getEmail()));
            }
            if(iii.get(mListOfObjects.get(position).getEmail())==null){
            	tv.setText(iii.get(mListOfObjects.get(position).getPhone()));
            }
            

            
            if(mListOfObjects.get(position).isSelected() && position!=0
            		&& mListOfObjects.get(position).getName()!="Invite Friends from Contacts"){
            ((CheckBox)rootView.findViewById(R.id.cb_item)).setChecked(true);
   		 	}else{
   		 	((CheckBox)rootView.findViewById(R.id.cb_item)).setChecked(false);
   		 	}
            
            ((CheckBox)rootView.findViewById(R.id.cb_item)).setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                ContactModel p = mListOfObjects.get(position);
                    if (!p.isSelected()) {
                        p.setSelected(true);
                    } else {
                        p.setSelected(false);
                    }
                                  }
            });
		}
		
		
		
		return rootView;
	}

	@Override
	public boolean isItemViewTypePinned(int viewType) {
		if(viewType == HEADER){
			return true;
		}else{
			return false;
		}
		
	}
	
	@Override
	public int getItemViewType(int position) {
		if(mListOfObjects.get(position).getName().equals("Friends on Planvy")
				||mListOfObjects.get(position).getName().equals("Invite Friends from Contacts")){
			return HEADER;
		}else{
			return CHAILD;
		}
	}
	
	@Override
	public int getViewTypeCount() {
		return 2;
	}
	
	@Override
    public Filter getFilter() {
    		
    	
        Filter filter = new Filter() {
            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
            	mListOfObjects =  (ArrayList<ContactModel>) results.values;
                notifyDataSetChanged();
            }
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                FilterResults results = new FilterResults();
               
                if(constraint == null || constraint.length() == 0){
                    results.count = mFilteredListOfObjects.size();
                    results.values = mFilteredListOfObjects;
                    return results;
                }
                else{
                	//constraint = constraint.toString().toLowerCase();
                    ArrayList<ContactModel> FilteredArrayNames = new ArrayList<ContactModel>();
                    Log.e("constraint", constraint.toString()+ " fh "+ constraint.length());
                    for(ContactModel cModel:mFilteredListOfObjects){
                    	String filter_string = constraint.toString().toLowerCase();
                    	String rightArg = cModel.getName().toLowerCase();
                    	if(constraint.toString().equals("")){
                    		FilteredArrayNames.addAll(mFilteredListOfObjects);
     	                   
                    	} else if(rightArg.startsWith(filter_string)){
                    		FilteredArrayNames.add(cModel);
                        }
                    }
                    results.count = FilteredArrayNames.size();
                    results.values = FilteredArrayNames;
                }
                return results;
            }
        };
        return filter;
    	
    }
	
	
}
