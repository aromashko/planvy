package ua.regionit.planvy.ui;

import org.holoeverywhere.LayoutInflater;
import org.holoeverywhere.app.Activity;
import org.holoeverywhere.app.Fragment;
import org.holoeverywhere.widget.Button;
import org.holoeverywhere.widget.EditText;
import org.holoeverywhere.widget.ProgressBar;
import org.holoeverywhere.widget.TextView;
import org.holoeverywhere.widget.Toast;

import ua.regionit.planvy.ErrorCode;
import ua.regionit.planvy.R;
import ua.regionit.planvy.ui.dialog.NotifyDialog;
import ua.regionit.planvy.util.EmailFormatValidator;
import ua.regionit.planvy.util.Support;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SaveCallback;



public class ChangeEmailFragment extends Fragment implements OnClickListener{
	EditText edt_change_email, edt_changed_email;
	Button btn_save_new_email, btn_cancel_new_email;
	ProgressBar changeEmailProgress;
	TextView tv_warning_change_email;
	String old_email, new_email;
	ParseUser currentUser;
	ImageView img_back_button_change_email;
	Activity settActivity;
	EmailFormatValidator emailFormatValidator;
	NotifyDialog notifyDialogInternet, notifyDialogError;
	public Dialog progressDialog;
	String old_email_not_changed;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_change_email, null);
		initUi(v);
		initListener();
		currentUser = ParseUser.getCurrentUser();
		emailFormatValidator = new EmailFormatValidator();
		old_email_not_changed = currentUser.getEmail();
		//if (currentUser.getEmail() != null){
		//	edt_change_email.setText(currentUser.getEmail());
	//}
		/*edt_changed_email.addTextChangedListener(new TextWatcher() {
			   public void afterTextChanged(Editable s) {
				   getTextFromChangeEmailView();
			      if (old_email.equals(new_email)) {
			    	  tv_warning_change_email.setVisibility(View.GONE);
			      } else {
			    	  tv_warning_change_email.setText("Email not equal");
			    	  tv_warning_change_email.setVisibility(View.VISIBLE);
			      }
			   }
			   public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
			   public void onTextChanged(CharSequence s, int start, int before, int count) {}
			 });*/
		    
		    return v;
	}
	
	public void initUi(View view){
		edt_change_email = (EditText) view.findViewById(R.id.edt_change_email);
		edt_changed_email = (EditText) view.findViewById(R.id.edt_changed_email);
		btn_save_new_email =(Button) view.findViewById(R.id.btn_save_new_email);
		btn_cancel_new_email =(Button) view.findViewById(R.id.btn_cancel_new_email);
		tv_warning_change_email = (TextView) view.findViewById(R.id.tv_warning_change_email);
		changeEmailProgress= (ProgressBar) view.findViewById(R.id.changeEmailProgress);
		img_back_button_change_email = (ImageView) view.findViewById(R.id.img_back_button_change_email);
	}
	
	public void initListener(){
		btn_save_new_email.setOnClickListener(this);
		btn_cancel_new_email.setOnClickListener(this);
		img_back_button_change_email.setOnClickListener(this);
		
	}
	
	public void getTextFromChangeEmailView(){
		old_email = edt_change_email.getText().toString();
		new_email = edt_changed_email.getText().toString();
	}
	
	@Override
	public void onAttach(Activity activity) {
		settActivity = (SettingsActivity) activity;
		super.onAttach(activity);
	}
	
	@Override
	public void onClick(View v) {
		switch (v.getId()){
		case R.id.btn_save_new_email:
			if (!Support.isNetworkConnected(settActivity)){
				notifyDialogInternet = NotifyDialog.newInstance(getString(R.string.no_internet_title), getString(R.string.no_internet_text), NotifyDialog.ButtonsLayout.ONE_BUTTON);
		    	notifyDialogInternet.show(settActivity);
				break;
			}
			tv_warning_change_email.setVisibility(View.GONE);
			getTextFromChangeEmailView();
			if (old_email.equals("")){
				tv_warning_change_email.setText("Email cannot be blank");
				tv_warning_change_email.setVisibility(View.VISIBLE);
				break;
				
			}
			if (new_email.equals("")){
				tv_warning_change_email.setText("Emails do not match");
				tv_warning_change_email.setVisibility(View.VISIBLE);
				break;
			}
			
			if(old_email.equals("")&&new_email.equals("")){
				tv_warning_change_email.setText("Emails cannot be blank");
				tv_warning_change_email.setVisibility(View.VISIBLE);
				break;
			}
			
			if (!emailFormatValidator.validate(old_email) || !emailFormatValidator.validate(new_email)){
				tv_warning_change_email.setText("Invalid email");
				tv_warning_change_email.setVisibility(View.VISIBLE);
				break;
			}
			
			
			
			//getTextFromChangeEmailView();
				      if (old_email.equals(new_email)) {
				    	  currentUser.setEmail(new_email);
				    	 
				    	  progressDialog = ProgressDialog.show(
						             settActivity, "", "Change Email...", true);
				    	  //changeEmailProgress.setVisibility(View.VISIBLE);
				    	  currentUser.saveInBackground(new SaveCallback() {
							
							@Override
							public void done(ParseException e) {
								progressDialog.dismiss();
								if(e==null){
								//changeEmailProgress.setVisibility(View.GONE);
								Log.d("New email", "save");
								 settActivity.onBackPressed();
								}
								else{
									 currentUser.setEmail(old_email_not_changed);
									
									Log.d("New email","not save");
								//changeEmailProgress.setVisibility(View.GONE);
	                	    	int err = e.getCode();
	                	    	e.printStackTrace();
	                	    	Log.d("Error code:", Integer.toString(err));
	                	    	
	                	    	switch(err){
	                	    	case ErrorCode.CONNECTION_FAILED:
	                	    		notifyDialogInternet = NotifyDialog.newInstance(getString(R.string.no_internet_title), getString(R.string.no_internet_text), NotifyDialog.ButtonsLayout.ONE_BUTTON);
	                		    	notifyDialogInternet.show(settActivity);
	                	    		break;
							/*case ErrorCode.INVALID_EMAIL_ADDRESS:
								Toast.makeText(getSupportApplication(), "Ploblem with saving new email: Invalid email", Toast.LENGTH_LONG).show();
								tv_warning_change_email.setText("Invalid email");
								tv_warning_change_email.setVisibility(View.VISIBLE);
                				break;
                	    	case ErrorCode.EMAIL_TAKEN:
                	    		Toast.makeText(getSupportApplication(), "Ploblem with saving new email: The email " +new_email + " is already taken", Toast.LENGTH_LONG).show();
                	    		tv_warning_change_email.setText("The email " +new_email + " is already taken");
                	    		tv_warning_change_email.setVisibility(View.VISIBLE);
                	    		break;
                	    	case ErrorCode.USERNAME_TAKEN:
                	    		Toast.makeText(getSupportApplication(), "Ploblem with saving new email: The email " +new_email + " is already taken", Toast.LENGTH_LONG).show();
                	    		tv_warning_change_email.setText("The email " +new_email + " is already taken");
                	    		tv_warning_change_email.setVisibility(View.VISIBLE);
                				break;*/
                	    	default:
                	    		//Toast.makeText(getSupportApplication(), "Ploblem with saving new email", Toast.LENGTH_LONG).show();
                	    		notifyDialogError =  NotifyDialog.newInstance("", e.getMessage(), NotifyDialog.ButtonsLayout.ONE_BUTTON);
                	    		notifyDialogError.show(settActivity);
	                	    	}
								}
							}
						});
				   
				      } else {
				    	  tv_warning_change_email.setText("Email not equal");
				    	  tv_warning_change_email.setVisibility(View.VISIBLE);
				      }
				  
			break;
		case R.id.btn_cancel_new_email:
			settActivity.onBackPressed();
			break;
		case R.id.img_back_button_change_email:
			settActivity.onBackPressed();
			break;
		}
		
	}
}
