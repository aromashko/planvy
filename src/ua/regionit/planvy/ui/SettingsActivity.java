package ua.regionit.planvy.ui;

import org.holoeverywhere.app.Activity;
import org.holoeverywhere.app.Fragment;
import org.holoeverywhere.widget.LinearLayout;
import org.holoeverywhere.widget.TextView;

import com.flurry.android.FlurryAgent;

import ua.regionit.planvy.R;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentManager.OnBackStackChangedListener;
import android.support.v4.app.FragmentTransaction;



public class SettingsActivity extends Activity{
	TextView userNameSetting, emailSettings;
	String user_name, email;
	LinearLayout userNameLay, emailLay, passwordLay, helpCenterLay, logOutLay;
	Fragment frag2;
	public final String SETTINGS_FRAGMENT_TAG= "settings_fragment_tag";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings);
		FlurryAgent.logEvent("Settings", true);
		 if (findViewById(R.id.fragment_container) != null) {
	            if (savedInstanceState != null)
	                return;
	            frag2 = new SettingsFragment();
	           
	            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
	            ft.replace(R.id.fragment_container, frag2, SETTINGS_FRAGMENT_TAG);
	            ft.addToBackStack(null);
	            ft.commit();
	            FragmentManager fm = getSupportFragmentManager();
	            fm.addOnBackStackChangedListener(new OnBackStackChangedListener() {
	                @Override
	                public void onBackStackChanged() {
	                    if(getSupportFragmentManager().getBackStackEntryCount() == 0){
	                    	startActivity(new Intent(SettingsActivity.this, ProfileActivity.class));
	                    	finish();
	                    	}
	                    else{
	                    	
	                    }
	                }
	            });
	           
	        }
		
	}
	
	@Override
	protected void onStart()
	{
		super.onStart();
		FlurryAgent.onStartSession(this, "XWHZX63GBXQKSR9KJGQ9");
	}
	
	@Override
	protected void onStop()
	{
		super.onStop();		
		FlurryAgent.onEndSession(this);
	}
	
	

}
