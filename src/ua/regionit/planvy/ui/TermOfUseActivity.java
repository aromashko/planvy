package ua.regionit.planvy.ui;

import org.holoeverywhere.app.Activity;
import org.holoeverywhere.widget.Button;
import org.holoeverywhere.widget.EditText;
import org.holoeverywhere.widget.ProgressBar;
import org.holoeverywhere.widget.TextView;

import com.flurry.android.FlurryAgent;

import ua.regionit.planvy.R;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

public class TermOfUseActivity extends Activity implements OnClickListener{
	
	ImageView back;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_term_of_service);
		FlurryAgent.logEvent("Term of Use", true);
		initUi();
		initListener();
	}
	
	@Override
	protected void onStart()
	{
		super.onStart();
		FlurryAgent.onStartSession(this, "XWHZX63GBXQKSR9KJGQ9");
	}
	
	@Override
	protected void onStop()
	{
		super.onStop();		
		FlurryAgent.onEndSession(this);
	}

	public void initUi(){
		back = (ImageView)findViewById(R.id.btn_term_of_use_back);
	}
	
	public void initListener(){
		back.setOnClickListener(this);
	}
	
	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.btn_term_of_use_back:
			finish();
		break;
		
	}
	}
}
