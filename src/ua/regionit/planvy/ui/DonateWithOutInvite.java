package ua.regionit.planvy.ui;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.holoeverywhere.LayoutInflater;
import org.holoeverywhere.app.Activity;
import org.holoeverywhere.widget.Button;
import org.holoeverywhere.widget.EditText;
import org.holoeverywhere.widget.LinearLayout;
import org.holoeverywhere.widget.ProgressBar;
import org.holoeverywhere.widget.TextView;
import org.holoeverywhere.widget.Toast;
import org.holoeverywhere.widget.ViewPager;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import ua.regionit.planvy.AppLog;
import ua.regionit.planvy.ErrorCode;
import ua.regionit.planvy.R;
import ua.regionit.planvy.ui.dialog.DonateFromInviteDialog;
import ua.regionit.planvy.ui.dialog.NotifyDialog;
import ua.regionit.planvy.util.Support;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ScrollView;

import com.flurry.android.FlurryAgent;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalItem;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalPaymentDetails;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;







public class DonateWithOutInvite extends Activity implements OnClickListener {
	TextView  tv_title_donate_from_invite;
	EditText edit_donate_summ_donate_without_invite;
	Button btn_give_donate_without_invite, btn_cancel_donate_without_invite;
	ImageView imgbackButton;
	static EditText mEditText;
	ParseUser currentUser;
	int sdk, mPosition;
	LinearLayout lay_user_picture_donate_from_invite;
	Bitmap resizedBitmap;
	int number_charity_list=0, mPositionClicked=-1, currentItem;
	DisplayImageOptions options;
	ViewPager pager_donate_without_donate;
	String charityDescription, title, email, photoThumbnail, photoFull, objectIdInvite, objectIdCharity;
	protected ImageLoader imageLoader = ImageLoader.getInstance();
	String[] imageUrls;
	List<String> arrImageUrls, arrTitle, arrCharityDescription, arrPhotoFull, arrObjectIdCharity;
	Bitmap thumbnail, resizedThumbnail;
	ImageView imageView;
	ProgressBar spinner;
	View imageLayout;
	ScrollView scrollview_donate_from_invite;
	Bitmap dnt_cellmask_resized, donate_cellmask_red_resized;
	Boolean isCharityChoosen = false;
	NotifyDialog notifyDialog;
	String amountPaid, short_description;
	int amount;
	public static final String RESPONSE = "response";
	public static final String SHORT_DESCRIPTION = "short_description";
	public static final String AMOUNT = "amount";
	
	public static final String PROOF_OF_PAYMENT = "proof_of_payment";
	public static final String ADAPTIVE_PAYMENT = "adaptive_payment";
	public static final String PAYMENT_EXEC_STATUS = "payment_exec_status";
	public static final String ID = "id";
	public static final String STATE="state";
	 NotifyDialog notifyDialogInternet;
	
	Typeface helveticaNeue;
	
	// set to PaymentActivity.ENVIRONMENT_PRODUCTION to move real money.
    // set to PaymentActivity.ENVIRONMENT_SANDBOX to use your test credentials from https://developer.paypal.com
    // set to PaymentActivity.ENVIRONMENT_NO_NETWORK to kick the tires without communicating to PayPal's servers.
    private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_PRODUCTION;
    
    // note that these credentials will differ between live & sandbox environments.
    private static final String CONFIG_CLIENT_ID = "AR_wzBAAqbal-WI7Qd1rmtuM6VATjAHWKpnWImY-WFWwCJDNg8dADbRYVFGO";
    // when testing in sandbox, this is likely the -facilitator email address. 
    private static final String CONFIG_RECEIVER_EMAIL = "admin@planvy.com";
    
    private static PayPalConfiguration config = new PayPalConfiguration()
    .environment(CONFIG_ENVIRONMENT)
    .clientId(CONFIG_CLIENT_ID);
    
    
	 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.donate_without_invite_activity);
		FlurryAgent.logEvent("Donate", true);
		sdk = android.os.Build.VERSION.SDK_INT;
		helveticaNeue = Typeface.createFromAsset(getAssets(), "HelveticaNeue.ttf");
		initUi();
		initListener();
		initArray();
		
		
		
		edit_donate_summ_donate_without_invite.setSelection(edit_donate_summ_donate_without_invite.getText().length());
		addOnTextChangeListener(edit_donate_summ_donate_without_invite);
		currentUser = ParseUser.getCurrentUser();
		
		if (currentUser == null){
			btn_cancel_donate_without_invite.setText("Back to Main");
		}
		
		getCharityList(0,20);
		//setViewPagerOnTouchListener();
		
		options = new DisplayImageOptions.Builder()
		.showImageForEmptyUri(R.drawable.ic_empty)
		.showImageOnFail(R.drawable.ic_error)
		.resetViewBeforeLoading(true)
		.cacheInMemory(true)
		.cacheOnDisc(true)
		.imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
		.bitmapConfig(Bitmap.Config.RGB_565)
		.considerExifParams(true)
		.displayer(new FadeInBitmapDisplayer(300))
		.build();
		
		/* Intent intent_pay = new Intent(this, PayPalService.class);
       
		intent_pay.putExtra(PaymentActivity.EXTRA_PAYPAL_ENVIRONMENT, CONFIG_ENVIRONMENT);
		intent_pay.putExtra(PaymentActivity.EXTRA_CLIENT_ID, CONFIG_CLIENT_ID);
		intent_pay.putExtra(PaymentActivity.EXTRA_RECEIVER_EMAIL, CONFIG_RECEIVER_EMAIL);
        
        startService(intent_pay);*/
        
        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        startService(intent);
		
      
	}
	
	@Override
	protected void onStart()
	{
		super.onStart();
		FlurryAgent.onStartSession(this, "XWHZX63GBXQKSR9KJGQ9");
	}
	
	@Override
	protected void onStop()
	{
		super.onStop();		
		FlurryAgent.onEndSession(this);
	}
	
	
	public void initUi(){
		
		edit_donate_summ_donate_without_invite = (EditText) findViewById(R.id.edit_donate_summ_donate_without_invite);
		edit_donate_summ_donate_without_invite.setTypeface(helveticaNeue);
		btn_give_donate_without_invite = (Button) findViewById(R.id.btn_give_donate_without_invite);
		btn_cancel_donate_without_invite = (Button) findViewById(R.id.btn_cancel_donate_without_invite);
		imgbackButton = (ImageView) findViewById(R.id.imgbackButton);
		pager_donate_without_donate = (ViewPager) findViewById(R.id.pager_donate_without_donate);
		scrollview_donate_from_invite = (ScrollView) findViewById(R.id.scrollview_donate_from_invite);
		tv_title_donate_from_invite = (TextView) findViewById(R.id.tv_title_donate_from_invite);
	}
	public void initListener(){
		btn_give_donate_without_invite.setOnClickListener(this);
		btn_cancel_donate_without_invite.setOnClickListener(this);
		imgbackButton.setOnClickListener(this);
	}
	
	public void initArray(){
		arrImageUrls = new ArrayList<String>();
		arrTitle = new ArrayList<String>();
		arrCharityDescription = new ArrayList<String>();
		arrPhotoFull = new ArrayList<String>();
		arrObjectIdCharity = new ArrayList<String>();
	}
	

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.btn_give_donate_without_invite:
			if (!Support.isNetworkConnected(DonateWithOutInvite.this)){
				notifyDialogInternet = NotifyDialog.newInstance(getString(R.string.no_internet_title), getString(R.string.no_internet_text), NotifyDialog.ButtonsLayout.ONE_BUTTON);
		    	notifyDialogInternet.show(DonateWithOutInvite.this);
				break;
			}
			String donate_sum = edit_donate_summ_donate_without_invite.getText().toString();
			if (donate_sum.length() <=1){
				NotifyDialog notifyDialog = NotifyDialog.newInstance("Warning", "Enter donate sum", NotifyDialog.ButtonsLayout.ONE_BUTTON);
				notifyDialog.show(this);
				break;
			}
			if (!isCharityChoosen){
				NotifyDialog notifyDialog = NotifyDialog.newInstance("Warning", "Choose charity cause", NotifyDialog.ButtonsLayout.ONE_BUTTON);
				notifyDialog.show(this);
				break;
			}
			String donate_sum_cut= donate_sum.substring(1, donate_sum.length());
			
			/* DecimalFormat df = (DecimalFormat) NumberFormat.getInstance(Locale.ENGLISH);
	            df.setParseBigDecimal(true);
			try {
				bigDecimal = (BigDecimal) df.parseObject(donate_sum_cut);
			} catch (java.text.ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
			amount= Integer.valueOf(donate_sum_cut);
			short_description = arrTitle.get(mPositionClicked);
			 PayPalPayment thingToBuy = new PayPalPayment(new BigDecimal(donate_sum_cut), "USD", arrTitle.get(mPositionClicked),PayPalPayment.PAYMENT_INTENT_SALE);
		        //new PaymentMethodActivity().
			 
		        Intent intent = new Intent(this, PaymentActivity.class);
		        
		      /*  intent.putExtra(PaymentActivity.EXTRA_PAYPAL_ENVIRONMENT, CONFIG_ENVIRONMENT);
		        intent.putExtra(PaymentActivity.EXTRA_CLIENT_ID, CONFIG_CLIENT_ID);
		        intent.putExtra(PaymentActivity.EXTRA_RECEIVER_EMAIL, CONFIG_RECEIVER_EMAIL);
		        
		        // It's important to repeat the clientId here so that the SDK has it if Android restarts your 
		        // app midway through the payment UI flow.
		        intent.putExtra(PaymentActivity.EXTRA_CLIENT_ID, CONFIG_CLIENT_ID);*/
		        //intent.putExtra(PaymentActivity.EXTRA_PAYER_ID, "your-customer-id-in-your-system");
		        //intent.putExtra(PaymentActivity.EXTRA_RECEIVER_EMAIL, "admin@planvy.com");
		       
		       intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);
		     
		       //intent.putExtra("AMOUNT_PAID", amountPaid);
		       
		        startActivityForResult(intent, 0);
		        
			break;
		case R.id.btn_cancel_donate_without_invite:
			if  (currentUser == null){
				startActivity(new Intent(this, InspireActivity.class));
			}
			else{
			startActivity(new Intent(this, ProfileActivity.class));}
			break;
		case R.id.imgbackButton:
			if  (currentUser == null){
				startActivity(new Intent(this, InspireActivity.class));
			}
			else{
			finish();
			}
			break;
		}
	
	}
	
	
	
	public static void addOnTextChangeListener(EditText editText){
		mEditText = editText;
		mEditText.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
								
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				if (s.length() == 0){
					mEditText.setText("$");
					mEditText.setSelection(mEditText.getText().length());
				}
				if (mEditText.getText().length()>0 && mEditText.getText().charAt(0) != '$'){
					mEditText.setText("$"+ s.subSequence(0, mEditText.getText().length()));
					mEditText.setSelection(mEditText.getText().length());
				}
				if (mEditText.getText().length()>2 && mEditText.getText().charAt(2) == '$'){
					mEditText.setText(mEditText.getText().subSequence(2, mEditText.length()));
				}
			}
		});
	}
	
	public void setViewPagerOnTouchListener(){
		pager_donate_without_donate.setOnTouchListener(new View.OnTouchListener() {
			   /* public boolean onTouch(View v, MotionEvent e) {
			        // How far the user has to scroll before it locks the parent vertical scrolling.
			        final int margin = 10;
			        final int fragmentOffset = v.getScrollX() % v.getWidth();

			        if (fragmentOffset > margin && fragmentOffset < v.getWidth() - margin) {
			        	pager.getParent().requestDisallowInterceptTouchEvent(true);
			        }
			        return false;
			    }*/
				int dragthreshold = 30;
		        int downX;
		        int downY;

		        @Override
		        public boolean onTouch(View v, MotionEvent event) {

		            switch (event.getAction()) {
		            case MotionEvent.ACTION_DOWN:
		                downX = (int) event.getRawX();
		                downY = (int) event.getRawY();
		                break;
		            case MotionEvent.ACTION_MOVE:
		                int distanceX = Math.abs((int) event.getRawX() - downX);
		                int distanceY = Math.abs((int) event.getRawY() - downY);

		                if (distanceY > distanceX && distanceY > dragthreshold) {
		                    pager_donate_without_donate.getParent().requestDisallowInterceptTouchEvent(false);
		                    scrollview_donate_from_invite.getParent().requestDisallowInterceptTouchEvent(true);
		                } else if (distanceX > distanceY && distanceX > dragthreshold) {
		                    pager_donate_without_donate.getParent().requestDisallowInterceptTouchEvent(true);
		                    scrollview_donate_from_invite.getParent().requestDisallowInterceptTouchEvent(false);
		                }
		                break;
		            case MotionEvent.ACTION_UP:
		            	scrollview_donate_from_invite.getParent().requestDisallowInterceptTouchEvent(false);
		                pager_donate_without_donate.getParent().requestDisallowInterceptTouchEvent(false);
		                break;
		            }
		            return false;
		        }
			});
	}
	
	
	
	public void getCharityList(int offset, int limit) {
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("offset", offset);
		params.put("limit", limit);
		
		//progress_bar_profile.setVisibility(View.VISIBLE);
		//tv_warning_profile_activity.setVisibility(View.GONE);
		 ParseCloud.callFunctionInBackground("getCharityList", params, new FunctionCallback<JSONArray>() {
			    public void done(JSONArray charity , ParseException e) {
			       if (e == null) {
			    	 
			    	   AppLog.e("Charity", charity.toString());
			    	   parseCharityList(charity);
			    	   imageUrls = new String[arrImageUrls.size()];
			    	   imageUrls = arrImageUrls.toArray(imageUrls);
			    	   pager_donate_without_donate.setAdapter(new ImagePagerAdapter(imageUrls));
			   			pager_donate_without_donate.setCurrentItem(0);
			   			
			        
			       }
			       else{
			    	   int err = e.getCode();
			    	   Log.d("Error code:", Integer.toString(err));
			    	   switch(err){
					    case ErrorCode.CONNECTION_FAILED:
					    	notifyDialogInternet = NotifyDialog.newInstance(getString(R.string.no_internet_title), getString(R.string.no_internet_text), NotifyDialog.ButtonsLayout.ONE_BUTTON);
					    	notifyDialogInternet.show(DonateWithOutInvite.this);
					    	//tv_warning_profile_activity.setText("Problem with Internet. Please try again.");
					    	//tv_warning_profile_activity.setVisibility(View.VISIBLE);
           				break;
					    case ErrorCode.SCRIPT_ERROR:
					    	/*if (e.getMessage().equals("User are not authorized") ){
					    		startActivity(new Intent(ProfileActivity.this, LoginActivity.class));
					    	}*/
					    break;
					    default:
            	    		/*tv_warning_profile_activity.setText(e.getMessage());
            	    		tv_warning_profile_activity.setVisibility(View.VISIBLE);*/
	            	    	}
			    	  
			    	  // progress_bar_profile.setVisibility(View.GONE);
			    	   e.printStackTrace();
			       }
			   }
			});
	}
	
	@Override
    protected void onActivityResult (int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
            if (confirm != null) {
            	
            	try {
            		JSONObject response = confirm.toJSONObject().getJSONObject(RESPONSE);
            		String state = response.getString(STATE);
    		    	
    		    	if (!state.equals("approved")){
    		    		Toast.makeText(DonateWithOutInvite.this, "State: not approved", Toast.LENGTH_LONG).show();
    		    		return;
    		    	}
            		amountPaid = Integer.toString(amount);
					/*amount = payment.getInt(AMOUNT);
					short_description = payment.getString(SHORT_DESCRIPTION);*/
					Map<String, String> charityTitleParams = new HashMap<String, String>();
			    	   charityTitleParams.put("Pay for", short_description);
			    	   FlurryAgent.logEvent("User Paid ", charityTitleParams);
				} catch (JSONException e) {
					e.printStackTrace();
				}
                    Log.i("paymentExample", confirm.toJSONObject().toString());
                    notifyDialog = NotifyDialog.newInstance("", "Your gift has been sent to the charity successfully",NotifyDialog.ButtonsLayout.ONE_BUTTON);
                    notifyDialog.setListener(new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							if (which == Dialog.BUTTON_POSITIVE) {
								notifyDialog.dismiss();
								
								 Intent intent = new Intent(DonateWithOutInvite.this, ShareActivity.class);
							        intent.putExtra("AMOUNT_PAID", amountPaid);
							        intent.putExtra("DESCRIPTION", short_description);
							        startActivity(intent);
							}
							
						}
					});
                    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                    ft.add(notifyDialog, null);
                    ft.commitAllowingStateLoss();
                   // notifyDialog.show(DonateWithOutInvite.this);
            }
        }
        else if (resultCode == Activity.RESULT_CANCELED) {
            Log.i("paymentExample", "The user canceled.");
        }
        /*else if (resultCode == PaymentActivity.RESULT_PAYMENT_INVALID) {
            Log.i("paymentExample", "An invalid payment was submitted. Please see the docs.");
        }*/
    }
	
	@Override
    public void onDestroy() {
        stopService(new Intent(this, PayPalService.class));
        super.onDestroy();
    }
	
	public void parseCharityList(JSONArray jsonArray){
		try{
			if (jsonArray.length()>19){
				number_charity_list=number_charity_list+20;
				getCharityList(number_charity_list,number_charity_list+20);
			}
			for(int i = 0; i < jsonArray.length(); i++){
				
		    	JSONObject jsonObject = jsonArray.getJSONObject(i);
		    	Log.d("Json Object " + Integer.toString(i), jsonObject.toString());
		    	charityDescription = jsonObject.getString("charityDescription");
		    	title = jsonObject.getString("title");
		    	email = jsonObject.getString("email");
		    	photoThumbnail = jsonObject.getString("photoThumbnail");
		    	photoFull = jsonObject.getString("photoFull");
		    	objectIdCharity= jsonObject.getString("objectId");
		    			
		    	arrImageUrls.add(photoThumbnail);
		    	arrCharityDescription.add(charityDescription);
		    	arrTitle.add(title);
		    	arrPhotoFull.add(photoFull);
		    	arrObjectIdCharity.add(objectIdCharity);
				
		      
				    
		       }
		    
		    
		} catch (JSONException e) {
		    e.printStackTrace();
		}
	}
	
	
			
	private class ImagePagerAdapter extends PagerAdapter {

		private String[] images;
		private LayoutInflater inflater;
		

		ImagePagerAdapter(String[] images ) {
			this.images = images;
			inflater = getLayoutInflater();
		}
		
		

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			container.removeView((View) object);
		}

		@Override
		public int getCount() {
			return images.length;
		}
		
		 private class ViewHolder {
				
			 	
				ImageView img_charity;
				LinearLayout lay_view_pager_item;
				ProgressBar spinner;
				TextView tv_title_donate_from_invite;
				
			}
		 
		 

		@Override
		public Object instantiateItem(ViewGroup view, final int position) {
			final ViewHolder holder;
			 
			//if (view==null){
			imageLayout = inflater.inflate(R.layout.item_pager_image, view, false);
			//assert imageLayout != null;
			holder = new ViewHolder();
			holder.img_charity = (ImageView) imageLayout.findViewById(R.id.image_item_view_pager);
			holder.img_charity.setTag("img"+position);
			Log.d("Tag init","img"+position);
			holder.lay_view_pager_item = (LinearLayout) imageLayout.findViewById(R.id.lay_view_pager_item);
			holder.spinner = (ProgressBar) imageLayout.findViewById(R.id.loading);
			holder.tv_title_donate_from_invite = (TextView) imageLayout.findViewById(R.id.tv_title_donate_from_invite);
			
			String title = arrTitle.get(position);
			holder.tv_title_donate_from_invite.setText(title);
			
			holder.tv_title_donate_from_invite.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					DonateFromInviteDialog donateFromInviteDialog = DonateFromInviteDialog.newInstance(arrTitle.get(position), arrCharityDescription.get(position), arrPhotoFull.get(position));
					donateFromInviteDialog.show(DonateWithOutInvite.this);
				}
			});
			imageLoader.loadImage(images[position],  options, new SimpleImageLoadingListener(){
				@Override
				public void onLoadingStarted(String imageUri, View view) {
					holder.spinner.setVisibility(View.VISIBLE);
				}

				@Override
				public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
					String message = null;
					switch (failReason.getType()) {
						case IO_ERROR:
							message = "Input/Output error";
							break;
						case DECODING_ERROR:
							message = "Image can't be decoded";
							break;
						case NETWORK_DENIED:
							message = "Downloads are denied";
							break;
						case OUT_OF_MEMORY:
							message = "Out Of Memory error";
							break;
						case UNKNOWN:
							message = "Unknown error";
							break;
					}
					Toast.makeText(DonateWithOutInvite.this, message, Toast.LENGTH_SHORT).show();

					holder.spinner.setVisibility(View.GONE);
				}
				
				@SuppressWarnings("deprecation")
				@SuppressLint("NewApi")
				@Override
				public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
					
		        	int value_height_width = (int) ProfileActivity.dipToPixels(DonateWithOutInvite.this, 100);
		        	
		        	if (loadedImage.getWidth() >= loadedImage.getHeight()){

		        		thumbnail = Bitmap.createBitmap(
		        				loadedImage, 
		        				loadedImage.getWidth()/2 - loadedImage.getHeight()/2,
		        		     0,
		        		     loadedImage.getHeight(), 
		        		     loadedImage.getHeight()
		        		     );

		        		}else{

		        			thumbnail = Bitmap.createBitmap(
		        					loadedImage,
		        		     0, 
		        		     loadedImage.getHeight()/2 - loadedImage.getWidth()/2,
		        		     loadedImage.getWidth(),
		        		     loadedImage.getWidth() 
		        		     );
		        		}
		        	resizedThumbnail=Bitmap.createScaledBitmap(thumbnail, value_height_width, value_height_width, true);
		        	Bitmap dnt_cellmask = BitmapFactory.decodeResource(getResources(),
	                        R.drawable.dnt_cellmask);
	            	dnt_cellmask_resized = Bitmap.createScaledBitmap(dnt_cellmask, value_height_width, value_height_width, true);
	            	
	            	 Bitmap donate_cellmask_red = BitmapFactory.decodeResource(getResources(),
	                         R.drawable.nvt_photo_mask_red);
	             	donate_cellmask_red_resized = Bitmap.createScaledBitmap(donate_cellmask_red, value_height_width, value_height_width, true);
	            	//Drawable drawable_cellmask = new BitmapDrawable(getResources(),dnt_cellmask_resized);
	            	holder.img_charity.setImageBitmap(dnt_cellmask_resized);
		        	 Drawable drawable = new BitmapDrawable(getResources(),resizedThumbnail);
		        	 LayoutParams linLayoutParam = new LayoutParams(value_height_width, value_height_width); 
		        	 holder.lay_view_pager_item.setLayoutParams(linLayoutParam);
		        	 
		             
		             if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
		            	 holder.lay_view_pager_item.setBackgroundDrawable(drawable);
		            	
		             } else {
		            	 holder.lay_view_pager_item.setBackground(drawable);
		             }
		            
		             holder.lay_view_pager_item.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
						
							isCharityChoosen = true;
							if (pager_donate_without_donate.getCurrentItem() == 0){
								currentItem= 0;
							}
							else{
								currentItem= pager_donate_without_donate.getCurrentItem()+1;
							}
							
							if (mPositionClicked == position){
								DonateFromInviteDialog donateFromInviteDialog = DonateFromInviteDialog.newInstance(arrTitle.get(position), arrCharityDescription.get(position), arrPhotoFull.get(position));
								donateFromInviteDialog.show(DonateWithOutInvite.this);
								
							}
							mPositionClicked = position;
							
							if (position-1<0){
								pager_donate_without_donate.setCurrentItem(0);
								String current = pager_donate_without_donate.getCurrentItem()+ "";
								Log.d("CurrentItem", current);
							}
							else{
							pager_donate_without_donate.setCurrentItem(position-1);
							Log.d("CurrentItem",Integer.toString(position-1));}
							//mPositionClicked = pager.getCurrentItem()+1;
							//Log.d("mPositionClicked",Integer.toString(mPositionClicked));
							//Log.d("mPosition-1", Integer.toString(mPosition-1));
							/*int length = getCount();
							Log.d("Length",Integer.toString(length));*/
							
								/*if (i==mPositionClicked){
									continue;
								}*/	
							for (int i=0;i<getCount();i++){
								String pos = Integer.toString(i);
								String tag= "img"+pos;
								Log.d("Tag",tag);
								ImageView img=(ImageView) pager_donate_without_donate.findViewWithTag(tag);
								//ImageView view1 = (ImageView) pager.getChildAt(0).findViewById(R.id.image_item_view_pager);
								//view1.setImageBitmap(dnt_cellmask_resized);\
								try{
								img.setImageBitmap(dnt_cellmask_resized);
								}
								catch (NullPointerException e){
									Log.e("Null Pointer exception", "true");
									
								}
								//pager.setAdapter(new ImagePagerAdapter(imageUrls));
								
							}
							holder.img_charity.setImageBitmap(donate_cellmask_red_resized);
							
						}
					});
		        	holder.spinner.setVisibility(View.GONE);
		        	if (position == 0){
		        		holder.lay_view_pager_item.performClick();
		        	}
		        	
				}
		        	
				
			});
			
			
			view.addView(imageLayout,0);
			return imageLayout;
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return view.equals(object);
		}

		@Override
		public void restoreState(Parcelable state, ClassLoader loader) {
		}

		@Override
		public Parcelable saveState() {
			return null;
		}
		@Override
		public float getPageWidth(int position) 
		{ return((float)1/3); } 
	}
	
	
}
