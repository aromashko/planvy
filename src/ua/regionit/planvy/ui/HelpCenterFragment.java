package ua.regionit.planvy.ui;

import org.holoeverywhere.LayoutInflater;
import org.holoeverywhere.app.Activity;
import org.holoeverywhere.app.Fragment;

import ua.regionit.planvy.R;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;




public class HelpCenterFragment extends Fragment implements OnClickListener {
	Activity settActivity;
	ImageView img_back_button_help_center;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.help_center_fragment, null);
	    initUi(v);
	    initListener();
		 return v;
	}
	
	public void initUi(View view){
		img_back_button_help_center = (ImageView) view.findViewById(R.id.img_back_button_help_center);
	}
	
	
	public void initListener(){
		img_back_button_help_center.setOnClickListener(this);
	}
	
	@Override
	public void onAttach(Activity activity) {
		settActivity = (SettingsActivity) activity;
		super.onAttach(activity);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()){
		case R.id.img_back_button_help_center:	
			settActivity.onBackPressed();
			break;
		}
	}

}
