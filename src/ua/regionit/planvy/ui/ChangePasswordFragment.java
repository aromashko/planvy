package ua.regionit.planvy.ui;

import org.holoeverywhere.LayoutInflater;
import org.holoeverywhere.app.Activity;
import org.holoeverywhere.app.Fragment;
import org.holoeverywhere.widget.Button;
import org.holoeverywhere.widget.EditText;
import org.holoeverywhere.widget.ProgressBar;
import org.holoeverywhere.widget.TextView;
import org.holoeverywhere.widget.Toast;

import ua.regionit.planvy.ErrorCode;
import ua.regionit.planvy.R;
import ua.regionit.planvy.ui.dialog.NotifyDialog;
import ua.regionit.planvy.util.Support;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SaveCallback;




public class ChangePasswordFragment extends Fragment implements OnClickListener {
	EditText edt_change_password, edt_changed_password;
	Button btn_save_new_password, btn_cancel_new_password;
	TextView tv_warning_change_password;
	ProgressBar change_password_progress;
	String password, confirm_password;
	ParseUser currentUser;
	ImageView img_back_button_change_password;
	Activity settActivity;
	NotifyDialog notifyDialogInternet, notifyDialogError;
	public Dialog progressDialog;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_change_password, null);
	    initUi(v);
	    initListener();
	    currentUser = ParseUser.getCurrentUser();
	    edt_changed_password.addTextChangedListener(new TextWatcher() {
			   public void afterTextChanged(Editable s) {
				   getTextFromChangePasswordView();
				   if (password.equals(confirm_password)) {
					   
				   }
				   else {
				    	  tv_warning_change_password.setText("Passwords do not match");
				      }
			   }
			   public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
			   public void onTextChanged(CharSequence s, int start, int before, int count) {}
			 });
		 return v;
	}
	
	public void initUi(View view){
		edt_change_password = (EditText) view.findViewById(R.id.edt_change_password);
		edt_changed_password = (EditText) view.findViewById(R.id.edt_changed_password);
		btn_save_new_password = (Button) view.findViewById(R.id.btn_save_new_password);
		btn_cancel_new_password = (Button) view.findViewById(R.id.btn_cancel_new_password);
		tv_warning_change_password = (TextView) view.findViewById(R.id.tv_warning_change_password);
		change_password_progress =  (ProgressBar) view.findViewById(R.id.change_password_progress);
		img_back_button_change_password = (ImageView) view.findViewById(R.id.img_back_button_change_password);
	}
	
	
	public void initListener(){
		btn_save_new_password.setOnClickListener(this);
		btn_cancel_new_password.setOnClickListener(this);
		img_back_button_change_password.setOnClickListener(this);
	}
	
	public void getTextFromChangePasswordView(){
		password = edt_change_password.getText().toString();
		confirm_password = edt_changed_password.getText().toString();
	}
	
	@Override
	public void onAttach(Activity activity) {
		settActivity = (SettingsActivity) activity;
		super.onAttach(activity);
	}
	
	
	@Override
	public void onClick(View v) {
		switch (v.getId()){
		case R.id.btn_save_new_password:
			if (!Support.isNetworkConnected(settActivity)){
				notifyDialogInternet = NotifyDialog.newInstance(getString(R.string.no_internet_title), getString(R.string.no_internet_text), NotifyDialog.ButtonsLayout.ONE_BUTTON);
		    	notifyDialogInternet.show(settActivity);
				break;
			}
			tv_warning_change_password.setVisibility(View.GONE);
			getTextFromChangePasswordView();
			if (password.equals("")){
				tv_warning_change_password.setText("Password cannot be blank");
				tv_warning_change_password.setVisibility(View.VISIBLE);
				break;
				
			}
			if (confirm_password.equals("")){
				tv_warning_change_password.setText("Passwords do not match");
				tv_warning_change_password.setVisibility(View.VISIBLE);
				break;
			}
			
				      if (password.equals(confirm_password)) {
				    	  currentUser.setPassword(confirm_password);
				    	  progressDialog = ProgressDialog.show(
						             settActivity, "", "Change Password...", true);
				    	  //change_password_progress.setVisibility(View.VISIBLE);
				    	  currentUser.saveInBackground(new SaveCallback() {
							
							@Override
							public void done(ParseException e) {
								progressDialog.dismiss();
								if(e==null){
									startActivity(new Intent(getActivity(), LoginActivity.class));
									//change_password_progress.setVisibility(View.GONE);
									//Toast.makeText(getSupportApplication(), "Ploblem with saving new password", Toast.LENGTH_LONG).show();
									
								}
								else{
									int err = e.getCode();
		                	    	e.printStackTrace();
		                	    	Log.d("Error code:", Integer.toString(err));
		                	    	
		                	    	switch(err){
		                	    	case ErrorCode.CONNECTION_FAILED:
		                	    		notifyDialogInternet = NotifyDialog.newInstance(getString(R.string.no_internet_title), getString(R.string.no_internet_text), NotifyDialog.ButtonsLayout.ONE_BUTTON);
		                		    	notifyDialogInternet.show(settActivity);
		                	    		break;
		                	    	default:
		                	    		notifyDialogError =  NotifyDialog.newInstance("", e.getMessage(), NotifyDialog.ButtonsLayout.ONE_BUTTON);
		                	    		notifyDialogError.show(settActivity);
		                	    		//tv_warning_change_password.setText(e.getMessage());
		                	    		//tv_warning_change_password.setVisibility(View.VISIBLE);
		                	    	}
									
									//Toast.makeText(getSupportApplication(), "Ploblem with saving new password", Toast.LENGTH_LONG).show();
									//change_password_progress.setVisibility(View.GONE);
									e.printStackTrace();
	                	    	/*int err = e.getCode();
	                	    	Log.d("Error code:", Integer.toString(err));
	                	    	switch(err){
	                	    	default:
	                	    		tv_warning_change_password.setText(e.getMessage());
	                	    		tv_warning_change_password.setVisibility(View.VISIBLE);
	                	    	}*/
								}
							}
						});
				   
				      } else {
				    	  tv_warning_change_password.setText("Passwords do not match");
				    	  tv_warning_change_password.setVisibility(View.VISIBLE);
				      }
				  
			break;
		case R.id.btn_cancel_new_password:	
			settActivity.onBackPressed();
			break;
		case R.id.img_back_button_change_password:
			settActivity.onBackPressed();
		
			break;
		}
	}

}
