
package ua.regionit.planvy.ui.dialog;


import org.holoeverywhere.app.Dialog;
import org.holoeverywhere.app.DialogFragment;
import org.holoeverywhere.drawable.ColorDrawable;
import org.holoeverywhere.widget.Button;

import ua.regionit.planvy.R;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;




public class ChangePictureDialog extends DialogFragment implements OnClickListener {

    
    
    private android.content.DialogInterface.OnClickListener mListener;
    private Button btn_take_picture, btn_choose_picture, btn_cancel_change_picture;
    public static final int TAKE_PICTURE= 2;
    public static final int CHOOSE_PICTURE= 3;
    

    public static ChangePictureDialog newInstance() {
        ChangePictureDialog d = new ChangePictureDialog();
       
        return d;
    }
    
    
    

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_FRAME, R.style.DialogStyle);
    }

    @Override
    public View onCreateView(org.holoeverywhere.LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_change_picture, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        btn_take_picture = (Button) view.findViewById(R.id.btn_take_picture);
        btn_choose_picture = (Button) view.findViewById(R.id.btn_choose_picture);
        btn_cancel_change_picture= (Button) view.findViewById(R.id.btn_cancel_change_picture);
        
        btn_take_picture.setOnClickListener(this);
        btn_choose_picture.setOnClickListener(this);
        btn_cancel_change_picture.setOnClickListener(this);
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(0));	
    }

    public ChangePictureDialog setListener(DialogInterface.OnClickListener listener) {
        mListener = listener;
        return this;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
    	Dialog dialog = super.onCreateDialog(savedInstanceState);
        return dialog;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
        case R.id.btn_take_picture:
        	if (mListener != null) {
                mListener.onClick(null, TAKE_PICTURE);
            }
        	break;
        case R.id.btn_choose_picture:
        	if (mListener != null) {
                mListener.onClick(null, CHOOSE_PICTURE);
            }
        	break;
        case R.id.btn_cancel_change_picture:
        	 dismiss();
        	break;
        }
        dismiss();
    }


   

}
