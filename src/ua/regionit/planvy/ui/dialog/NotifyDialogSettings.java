
package ua.regionit.planvy.ui.dialog;



import org.holoeverywhere.app.Dialog;
import org.holoeverywhere.app.DialogFragment;
import org.holoeverywhere.drawable.ColorDrawable;

import ua.regionit.planvy.R;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;


public class NotifyDialogSettings extends DialogFragment implements OnClickListener {

    private static final String EXTRA_BUTTON_NO = "extra_button_no";

    private static final String EXTRA_BUTTON_YES = "extra_button_yes";

    private static final String TAG = "HelpDialogFragment";

    public static final String EXTRA_MESSAGE = "extra_message";
    public static final String EXTRA_TITLE = "extra_title";
    public static final String EXTRA_BUTTONS_LAYOUT = "extra_buttons";

    private TextView mTitleView;
    private TextView mMessageView;
    Typeface helveticaNeue;
    Button btn_ok;
    
    public static final String STARTSPANNEBLE="startSpanneble";
    public static final String ENDSPANNEBLE="endSpanneble";

    SpannableString send_pass_rec_text;
    private android.content.DialogInterface.OnClickListener mListener;

    private Button mYesButton;

    private Button mNoButton;
    
    Integer spanStart;
    
    public static NotifyDialogSettings newInstance(String title, String message) {
        return newInstance(title, message, ButtonsLayout.TWO_BUTTONS);
    }

    public static NotifyDialogSettings newInstance(String title, String message, int layout) {
        NotifyDialogSettings d = new NotifyDialogSettings();
        Bundle args = new Bundle();
        args.putString(EXTRA_TITLE, title);
        args.putString(EXTRA_MESSAGE, message);
        args.putInt(EXTRA_BUTTONS_LAYOUT, layout);
        d.setArguments(args);
        return d;
    }
    
    public static NotifyDialogSettings newInstance(String title, String message, int startSpanneble, int layout) {
        NotifyDialogSettings d = new NotifyDialogSettings();
        Bundle args = new Bundle();
        args.putString(EXTRA_TITLE, title);
        args.putString(EXTRA_MESSAGE, message);
        args.putInt(STARTSPANNEBLE, startSpanneble);
        args.putInt(EXTRA_BUTTONS_LAYOUT, layout);
        d.setArguments(args);
        return d;
    }
    

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_FRAME, R.style.DialogStyle);
        helveticaNeue = Typeface.createFromAsset(getActivity().getAssets(), "HelveticaNeue.ttf");
    }

    @Override
    public View onCreateView(org.holoeverywhere.LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_two_buttons_dialog, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mTitleView = (TextView)view.findViewById(R.id.notify_title);
        mMessageView = (TextView)view.findViewById(R.id.description_profile_item_dialog);

        String title = null;
        CharSequence message = null;
        int buttonsLayout = ButtonsLayout.ONE_BUTTON;
        String yesButtonText = null;
        String noButtonText = null;

        Bundle arguments = getArguments();
        if (arguments != null) {
            title = arguments.getString(EXTRA_TITLE);
            if (TextUtils.isEmpty(title)) {
                title = "";
            }
            message = arguments.getCharSequence(EXTRA_MESSAGE);
            if (TextUtils.isEmpty(message)) {
                message = arguments.getString(EXTRA_MESSAGE);
            }
            buttonsLayout = arguments.getInt(EXTRA_BUTTONS_LAYOUT);
            yesButtonText = arguments.getString(EXTRA_BUTTON_YES);
            noButtonText = arguments.getString(EXTRA_BUTTON_NO);
            spanStart =Integer.valueOf(arguments.getInt(STARTSPANNEBLE,-1));
            if (spanStart != -1){
            	send_pass_rec_text = new SpannableString("An email has been sent to your account's email address. Please check you email to continue. If you are still having problems, please visit \nPlanvy Hep Center");
        		send_pass_rec_text.setSpan(new UnderlineSpan(), arguments.getInt(STARTSPANNEBLE), send_pass_rec_text.length(), 0);
        		
        		Log.d("Length send pass",Integer.toString(send_pass_rec_text.length()));
            }
                        
        }
        if (title.equals("")){
        	mTitleView.setVisibility(View.GONE);
        }
        else{
        mTitleView.setText(title);}
        mMessageView.setText(message);
        if (send_pass_rec_text != null){
        mMessageView.setText(send_pass_rec_text);}
        
        switch (buttonsLayout) {
            case ButtonsLayout.ONE_BUTTON:
                view.findViewById(R.id.button_bar_ok).setVisibility(View.VISIBLE);
                view.findViewById(R.id.buttons_bar_yes_no).setVisibility(View.GONE);
                break;
            case ButtonsLayout.TWO_BUTTONS:
                view.findViewById(R.id.button_bar_ok).setVisibility(View.GONE);
                view.findViewById(R.id.buttons_bar_yes_no).setVisibility(View.VISIBLE);
                break;
            case ButtonsLayout.CUSTOM:
                view.findViewById(R.id.button_bar_ok).setVisibility(View.VISIBLE);
                view.findViewById(R.id.buttons_bar_yes_no).setVisibility(View.GONE);
                break;
        }
        
        if (title.equals("") && message.equals("")){
        	mTitleView.setVisibility(View.GONE);
        	mMessageView.setVisibility(View.GONE);
        	view.findViewById(R.id.button_bar_ok).setVisibility(View.GONE);
            view.findViewById(R.id.buttons_bar_yes_no).setVisibility(View.GONE);
            view.findViewById(R.id.dialog_pb).setVisibility(View.VISIBLE);
        }
        
        mYesButton = (Button)view.findViewById(R.id.button_no);
        mYesButton.setOnClickListener(this);
        if (!TextUtils.isEmpty(yesButtonText)) {
            mYesButton.setText(yesButtonText);
        }

        mNoButton = (Button)view.findViewById(R.id.button_yes);
        mNoButton.setOnClickListener(this);
        if (!TextUtils.isEmpty(noButtonText)) {
            mNoButton.setText(noButtonText);
        }
        btn_ok = (Button)view.findViewById(R.id.button_ok);
        btn_ok.setTypeface(helveticaNeue);
        view.findViewById(R.id.button_ok).setOnClickListener(this);
        
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(0));	
    }

    public NotifyDialogSettings setListener(DialogInterface.OnClickListener listener) {
        mListener = listener;
        return this;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
    	Dialog dialog = super.onCreateDialog(savedInstanceState);
        return dialog;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_no:
                if (mListener != null) {
                    mListener.onClick(null, Dialog.BUTTON_NEGATIVE);
                }
                break;
            case R.id.button_yes:
                if (mListener != null) {
                    mListener.onClick(null, Dialog.BUTTON_POSITIVE);
                }
                break;
            case R.id.button_ok:
                if (mListener != null) {
                    mListener.onClick(null, Dialog.BUTTON_POSITIVE);
                }
                break;
        }
        dismiss();
    }

    public interface ButtonsLayout {
        int CUSTOM = 0;
        int TWO_BUTTONS = 1;
        int ONE_BUTTON = 2;
    }

    public interface Buttons {
        int OK = 0;
        int YES = 1;
        int NO = 2;
    }

    public void setButtonText(int button, String text) {
        switch (button) {
            case Buttons.OK:

                break;
            case Buttons.YES:
                getArguments().putString(EXTRA_BUTTON_YES, text);
                break;
            case Buttons.NO:
                getArguments().putString(EXTRA_BUTTON_NO, text);
                break;
        }
    }

}
