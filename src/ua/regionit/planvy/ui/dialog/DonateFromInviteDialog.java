
package ua.regionit.planvy.ui.dialog;


import org.holoeverywhere.app.Dialog;
import org.holoeverywhere.app.DialogFragment;
import org.holoeverywhere.drawable.ColorDrawable;
import org.holoeverywhere.widget.Button;
import org.holoeverywhere.widget.TextView;

import ua.regionit.planvy.R;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;




public class DonateFromInviteDialog extends DialogFragment implements OnClickListener {

    
    
    private android.content.DialogInterface.OnClickListener mListener;
    private Button ok_button;
    private ImageView img_donate_from_invite_full;
    private TextView donate_from_invite_title, tv_description_donate_from_invite_dialog;
    public static final int TAKE_PICTURE= 2;
    public static final int CHOOSE_PICTURE= 3;
    DisplayImageOptions options;
    ImageLoader imageLoader;

    public static DonateFromInviteDialog newInstance(String title, String description, String urlFullPhoto) {
        DonateFromInviteDialog d = new DonateFromInviteDialog();
        Bundle args = new Bundle();
        args.putString("TITLE", title);
        args.putString("DESCRIPTION", description);
        args.putString("URL_PHOTO_FULL", urlFullPhoto);
        d.setArguments(args);
        return d;
    }
    
    
    

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_FRAME, R.style.DialogStyle);
        options = new DisplayImageOptions.Builder()
		.showImageForEmptyUri(R.drawable.ic_empty)
		.showImageOnFail(R.drawable.ic_error)
		.resetViewBeforeLoading(true)
		.cacheInMemory(true)
		.cacheOnDisc(true)
		.imageScaleType(ImageScaleType.EXACTLY)
		.bitmapConfig(Bitmap.Config.RGB_565)
		.considerExifParams(true)
		.displayer(new FadeInBitmapDisplayer(300))
		//.displayer(new RoundedBitmapDisplayer(300))
		.build();
    }

    @Override
    public View onCreateView(org.holoeverywhere.LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_donate_from_invite_dialog, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        imageLoader = ImageLoader.getInstance();
        ok_button = (Button) view.findViewById(R.id.button_ok);
        img_donate_from_invite_full =  (ImageView) view.findViewById(R.id.img_donate_from_invite_full);
        donate_from_invite_title =  (TextView) view.findViewById(R.id.donate_from_invite_title);
        tv_description_donate_from_invite_dialog =  (TextView) view.findViewById(R.id.tv_description_donate_from_invite_dialog);
        String title, description, urlPhotoFull;
        
        Bundle arguments = getArguments();
        title = arguments.getString("TITLE");
        description = arguments.getString("DESCRIPTION");
        urlPhotoFull = arguments.getString("URL_PHOTO_FULL");
        
        
        tv_description_donate_from_invite_dialog.setText(description);
        donate_from_invite_title.setText(title);
       
        imageLoader.displayImage(urlPhotoFull, img_donate_from_invite_full, options);
        ok_button.setOnClickListener(this);
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(0));	
    }

    public DonateFromInviteDialog setListener(DialogInterface.OnClickListener listener) {
        mListener = listener;
        return this;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
    	Dialog dialog = super.onCreateDialog(savedInstanceState);
        return dialog;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
        case R.id.button_ok:
        	dismiss();
        }
        dismiss();
    }


   

}
