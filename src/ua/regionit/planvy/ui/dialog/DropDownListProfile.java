
package ua.regionit.planvy.ui.dialog;




import java.util.List;

import org.holoeverywhere.app.Dialog;
import org.holoeverywhere.app.DialogFragment;
import org.holoeverywhere.drawable.ColorDrawable;
import org.holoeverywhere.widget.ListView;

import ua.regionit.planvy.R;
import ua.regionit.planvy.ui.DonateFromInviteActivity;
import ua.regionit.planvy.util.InvitesProfileModel;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;




public class DropDownListProfile extends DialogFragment implements OnClickListener {

    
   
    private android.content.DialogInterface.OnClickListener mListener;
    View source;
    ListView lv_received_notification_drop_down;
    ListAdapter mAdapter;
    static Context mContext;
   
   

    public static DropDownListProfile newInstance(Context context) {
        DropDownListProfile d = new DropDownListProfile();
        Bundle args = new Bundle();
        d.setArguments(args);
        mContext = context;
        return d;
    }
    
    
    

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_FRAME, R.style.DialogStyle);
        
     }

    @Override
    public View onCreateView(org.holoeverywhere.LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	//setDialogPosition();
    	
    	Window window = getDialog().getWindow();

        // set "origin" to top left corner, so to speak
        window.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL);

        // after that, setting values for x and y works "naturally"
        WindowManager.LayoutParams params = window.getAttributes();
        //params.x = 300;
        params.y = 40;
        window.setAttributes(params);
        return inflater.inflate(R.layout.fragment_drop_down_list_profile, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        TextView mtv = (TextView)view.findViewById(R.id.change_picture_title);
    	Typeface light = Typeface.createFromAsset(mContext.getAssets(), "HelveticaNeueLight.ttf");    
    	mtv.setTypeface(light);
        lv_received_notification_drop_down = (ListView)view.findViewById(R.id.lv_received_notification_drop_down);
        lv_received_notification_drop_down.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapter, View view, int position,
					long id) {
				FlurryAgent.logEvent("View Invite", true);
				InvitesProfileModel invitesProfileModel = (InvitesProfileModel) mAdapter.getItem(position);
				//invitesProfileModel.getObjectId();
				Log.d("Object Id", invitesProfileModel.getObjectId() );
				ParseQuery<ParseObject> query = ParseQuery.getQuery("Invite");
	        	query.whereEqualTo("objectId", invitesProfileModel.getObjectId());
	        	query.findInBackground(new FindCallback<ParseObject>() {
	        	    public void done(List<ParseObject> object, ParseException e) {
	        	        if (e == null) {
	        	        	if (object.size()>0){
	        	        	ParseObject invites = object.get(0);
	        	        	invites.put("isRead", true);
	        	        	invites.saveInBackground();
	        	        	}
	        	        } else {
	        	            Log.d("Error", e.getMessage());
	        	        }
	        	    }
	        	});
	        	Intent intent = new Intent(mContext, DonateFromInviteActivity.class);
	        	intent.putExtra("nickname", invitesProfileModel.getName());
	        	intent.putExtra("url", invitesProfileModel.getUrlToPhoto());
	        	intent.putExtra("description", invitesProfileModel.getDescription());
	        	intent.putExtra("objectId", invitesProfileModel.getObjectId());
	        	startActivity(intent);
	        	getDialog().dismiss();
			}
        	
		});
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(0));	
    }

    public DropDownListProfile setListener(DialogInterface.OnClickListener listener) {
        mListener = listener;
        return this;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
    	Dialog dialog = super.onCreateDialog(savedInstanceState);
        return dialog;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
        /*case R.id.btn_take_picture:
        	if (mListener != null) {
                mListener.onClick(null, TAKE_PICTURE);
            }
        	break;*/
        
        }
        dismiss();
    }

    
    public void setAdapterDropDownList(ListAdapter adapter){
    	if (lv_received_notification_drop_down != null){
    		mAdapter=adapter;
    	lv_received_notification_drop_down.setAdapter(adapter);}
    }
    
   /* private void setDialogPosition() {
        if (source == null) {
            return; // Leave the dialog in default position
        }

        // Find out location of source component on screen
        // see http://stackoverflow.com/a/6798093/56285
        int[] location = new int[2];
        source.getLocationOnScreen(location);
        int sourceX = location[0];
        int sourceY = location[1];

        Window window = getDialog().getWindow();

        // set "origin" to top left corner
        window.setGravity(Gravity.TOP|Gravity.LEFT);

        WindowManager.LayoutParams params = window.getAttributes();

        // Just an example; edit to suit your needs.
        params.x = sourceX - dpToPx(110); // about half of confirm button size left of source view
        params.y = sourceY - dpToPx(80); // above source view

        window.setAttributes(params);
    }

    public int dpToPx(float valueInDp) {
        DisplayMetrics metrics = getActivity().getResources().getDisplayMetrics();
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, valueInDp, metrics);
    }
   */

}
