package ua.regionit.planvy.ui.dialog;

import org.holoeverywhere.app.Dialog;
import org.holoeverywhere.app.DialogFragment;
import org.holoeverywhere.drawable.ColorDrawable;
import org.holoeverywhere.widget.EditText;

import ua.regionit.planvy.AppLog;
import ua.regionit.planvy.R;
import ua.regionit.planvy.ui.LoginActivity;
import ua.regionit.planvy.ui.ShareActivity;
import ua.regionit.planvy.ui.dialog.ChangePhoneDialog.Buttons;
import ua.regionit.planvy.ui.dialog.ChangePhoneDialog.ButtonsLayout;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SaveCallback;

public class FacebookShareTextDialog extends DialogFragment implements OnClickListener {

    private static final String EXTRA_BUTTON_NO = "extra_button_no";

    private static final String EXTRA_BUTTON_YES = "extra_button_yes";

    private static final String TAG = "HelpDialogFragment";

    public static final String EXTRA_MESSAGE = "extra_message";
    public static final String EXTRA_TITLE = "extra_title";
    public static final String EXTRA_BUTTONS_LAYOUT = "extra_buttons";

    private TextView mTitleView;
    public static EditText edt_change_share_text;
    public static String shareText;
    
    String mPhoneNumber;
   
    private android.content.DialogInterface.OnClickListener mListener;

    private Button mYesButton;

    private Button mNoButton;
    ParseUser parseUser;
    LoginActivity activity;
    
    //public Dialog progressDialogChangePhone;
    
    public static FacebookShareTextDialog newInstance(String title) {
        return newInstance(title, ButtonsLayout.TWO_BUTTONS);
    }

    public static FacebookShareTextDialog newInstance(String title, int layout) {
    	FacebookShareTextDialog d = new FacebookShareTextDialog();
        Bundle args = new Bundle();
        args.putString(EXTRA_TITLE, title);
        args.putInt(EXTRA_BUTTONS_LAYOUT, layout);
        d.setArguments(args);
        return d;
    }
    
   
    

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_FRAME, R.style.DialogStyle);
    }

    @Override
    public View onCreateView(org.holoeverywhere.LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.facebook_sharing_dialog, null);
    }
    
    

    

	@Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        parseUser =ParseUser.getCurrentUser();
        mTitleView = (TextView)view.findViewById(R.id.fb_notify_title);
        edt_change_share_text = (EditText)view.findViewById(R.id.edt_fb_sharing);
        
        edt_change_share_text.setText(ShareActivity.shareMsg);
				
       

        String title = null;
        CharSequence message = null;
        int buttonsLayout = ButtonsLayout.ONE_BUTTON;
        String yesButtonText = null;
        String noButtonText = null;

        Bundle arguments = getArguments();
        if (arguments != null) {
            title = arguments.getString(EXTRA_TITLE);
            
            buttonsLayout = arguments.getInt(EXTRA_BUTTONS_LAYOUT);
            yesButtonText = arguments.getString(EXTRA_BUTTON_YES);
            noButtonText = arguments.getString(EXTRA_BUTTON_NO);
            
                        
        }
        mTitleView.setText(title);
        
        
        switch (buttonsLayout) {
            case ButtonsLayout.ONE_BUTTON:
                view.findViewById(R.id.fb_button_bar_ok).setVisibility(View.VISIBLE);
                view.findViewById(R.id.fb_buttons_bar_yes_no).setVisibility(View.GONE);
                break;
            case ButtonsLayout.TWO_BUTTONS:
                view.findViewById(R.id.fb_button_bar_ok).setVisibility(View.GONE);
                view.findViewById(R.id.fb_buttons_bar_yes_no).setVisibility(View.VISIBLE);
                break;
            case ButtonsLayout.CUSTOM:
                view.findViewById(R.id.fb_button_bar_ok).setVisibility(View.VISIBLE);
                view.findViewById(R.id.fb_buttons_bar_yes_no).setVisibility(View.GONE);
                break;
        }
        
        
        
        mYesButton = (Button)view.findViewById(R.id.fb_button_yes);
        mYesButton.setOnClickListener(this);
        if (!TextUtils.isEmpty(yesButtonText)) {
            mYesButton.setText(yesButtonText);
        }

        mNoButton = (Button)view.findViewById(R.id.fb_button_no);
        mNoButton.setOnClickListener(this);
        if (!TextUtils.isEmpty(noButtonText)) {
            mNoButton.setText(noButtonText);
        }
        view.findViewById(R.id.fb_button_ok).setOnClickListener(this);
        
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(0));	
    }

    public FacebookShareTextDialog setListener(DialogInterface.OnClickListener listener) {
        mListener = listener;
        return this;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
    	Dialog dialog = super.onCreateDialog(savedInstanceState);
        return dialog;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fb_button_no:
                if (mListener != null) {
                    mListener.onClick(null, Dialog.BUTTON_NEGATIVE);
                }
                break;
            case R.id.fb_button_yes:
                if (mListener != null) {
                    mListener.onClick(null, Dialog.BUTTON_POSITIVE);
                }
                shareText = edt_change_share_text.getText().toString();
                
//                getShareText();
            	
            	
            	break;
            case R.id.fb_button_ok:
                if (mListener != null) {
                    mListener.onClick(null, Dialog.BUTTON_POSITIVE);
                }
                break;
        }
        dismiss();
    }
    
//    public static String getShareText(){
//    	String shareText = edt_change_share_text.getText().toString();
//    	return shareText;
//    }

    public interface ButtonsLayout {
        int CUSTOM = 0;
        int TWO_BUTTONS = 1;
        int ONE_BUTTON = 2;
    }

    public interface Buttons {
        int OK = 0;
        int YES = 1;
        int NO = 2;
    }

    public void setButtonText(int button, String text) {
        switch (button) {
            case Buttons.OK:

                break;
            case Buttons.YES:
                getArguments().putString(EXTRA_BUTTON_YES, text);
                break;
            case Buttons.NO:
                getArguments().putString(EXTRA_BUTTON_NO, text);
                break;
        }
    }

}
