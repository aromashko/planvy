
package ua.regionit.planvy.ui.dialog;


import org.holoeverywhere.app.Activity;
import org.holoeverywhere.app.Dialog;
import org.holoeverywhere.app.DialogFragment;
import org.holoeverywhere.drawable.ColorDrawable;
import org.holoeverywhere.widget.EditText;

import ua.regionit.planvy.AppLog;
import ua.regionit.planvy.R;
import ua.regionit.planvy.ui.LoginActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SaveCallback;


public class ChangePhoneDialog extends DialogFragment implements OnClickListener {

    private static final String EXTRA_BUTTON_NO = "extra_button_no";

    private static final String EXTRA_BUTTON_YES = "extra_button_yes";

    private static final String TAG = "HelpDialogFragment";

    public static final String EXTRA_MESSAGE = "extra_message";
    public static final String EXTRA_TITLE = "extra_title";
    public static final String EXTRA_BUTTONS_LAYOUT = "extra_buttons";

    private TextView mTitleView;
    private EditText edt_phone_change_phone_dialog;
    
    String mPhoneNumber;
   
    private android.content.DialogInterface.OnClickListener mListener;

    private Button mYesButton;

    private Button mNoButton;
    ParseUser parseUser;
    LoginActivity activity;
    
    //public Dialog progressDialogChangePhone;
    
    public static ChangePhoneDialog newInstance(String title) {
        return newInstance(title, ButtonsLayout.TWO_BUTTONS);
    }

    public static ChangePhoneDialog newInstance(String title, int layout) {
        ChangePhoneDialog d = new ChangePhoneDialog();
        Bundle args = new Bundle();
        args.putString(EXTRA_TITLE, title);
        args.putInt(EXTRA_BUTTONS_LAYOUT, layout);
        d.setArguments(args);
        return d;
    }
    
   
    

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_FRAME, R.style.DialogStyle);
    }

    @Override
    public View onCreateView(org.holoeverywhere.LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.change_phone_dialog, null);
        
    }
    
    

    

	/*@Override
	public void dismiss() {
		 Log.e("Dismiss", "click");
		 if (mListener != null) {
    		 Log.e("Dismiss", "click");
             mListener.onClick(null, Dialog.BUTTON_NEGATIVE);
         }
		super.dismiss();
	}*/
	
	

	@Override
	public void onCancel(DialogInterface dialog) {
		Log.e("Cancel", "click");
		if (mListener != null) {
            mListener.onClick(null, Dialog.BUTTON_NEGATIVE);
        }
		super.onCancel(dialog);
	}

	@Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        parseUser =ParseUser.getCurrentUser();
        
        mTitleView = (TextView)view.findViewById(R.id.notify_title);
        edt_phone_change_phone_dialog = (EditText)view.findViewById(R.id.edt_phone_change_phone_dialog);
        
        TelephonyManager tMgr =(TelephonyManager) getDialog().getContext().getSystemService(Context.TELEPHONY_SERVICE);
		 mPhoneNumber = tMgr.getLine1Number();
		 mPhoneNumber ="1";
		 if (mPhoneNumber != null && mPhoneNumber.length() >=2 && mPhoneNumber.substring(0, 2).equals("+1")){
		 AppLog.e("My phone number", mPhoneNumber);
		 edt_phone_change_phone_dialog.setText(mPhoneNumber);}
		 edt_phone_change_phone_dialog.setSelection(edt_phone_change_phone_dialog.getText().length());
		 edt_phone_change_phone_dialog.addTextChangedListener(new TextWatcher() {
			   public void afterTextChanged(Editable s) {
				  
				   
				 
				  if (edt_phone_change_phone_dialog.getText().length()<2){
					 
					  edt_phone_change_phone_dialog.setText("+1");
					  edt_phone_change_phone_dialog.setSelection(edt_phone_change_phone_dialog.getText().length());
					  
				   }
				  else{
					   if (edt_phone_change_phone_dialog.getText().charAt(0) != '+'){
						   edt_phone_change_phone_dialog.setText("+1");
						   edt_phone_change_phone_dialog.setSelection(edt_phone_change_phone_dialog.getText().length());
						   }
					   if (edt_phone_change_phone_dialog.getText().charAt(1) !='1'){
						   edt_phone_change_phone_dialog.setText("+1");
						   edt_phone_change_phone_dialog.setSelection(edt_phone_change_phone_dialog.getText().length());
					   }
				   }
				  
				  if (s.length() >= 0 && s.length() <=11){
					   mYesButton.setEnabled(false);
					   mYesButton.setTextColor(getResources().getColor(R.color.Black));
				   }
				   else if(s.length()>11){
					   mYesButton.setEnabled(true);
					   mYesButton.setTextColor(getResources().getColor(R.color.Blue));
				   }
				   if(s.length()>15){
					   s.delete(15, 16);
				   }
				  
			   }
			   public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				  
			   }
			   public void onTextChanged(CharSequence s, int start, int before, int count) {
			   }
			 });
		
       

        String title = null;
        CharSequence message = null;
        int buttonsLayout = ButtonsLayout.ONE_BUTTON;
        String yesButtonText = null;
        String noButtonText = null;

        Bundle arguments = getArguments();
        if (arguments != null) {
            title = arguments.getString(EXTRA_TITLE);
            
            buttonsLayout = arguments.getInt(EXTRA_BUTTONS_LAYOUT);
            yesButtonText = arguments.getString(EXTRA_BUTTON_YES);
            noButtonText = arguments.getString(EXTRA_BUTTON_NO);
            
                        
        }
        mTitleView.setText(title);
        
        
        switch (buttonsLayout) {
            case ButtonsLayout.ONE_BUTTON:
                view.findViewById(R.id.button_bar_ok).setVisibility(View.VISIBLE);
                view.findViewById(R.id.buttons_bar_yes_no).setVisibility(View.GONE);
                break;
            case ButtonsLayout.TWO_BUTTONS:
                view.findViewById(R.id.button_bar_ok).setVisibility(View.GONE);
                view.findViewById(R.id.buttons_bar_yes_no).setVisibility(View.VISIBLE);
                break;
            case ButtonsLayout.CUSTOM:
                view.findViewById(R.id.button_bar_ok).setVisibility(View.VISIBLE);
                view.findViewById(R.id.buttons_bar_yes_no).setVisibility(View.GONE);
                break;
        }
        
        
        
        mYesButton = (Button)view.findViewById(R.id.button_yes);
        mYesButton.setOnClickListener(this);
        if (!TextUtils.isEmpty(yesButtonText)) {
            mYesButton.setText(yesButtonText);
        }

        mNoButton = (Button)view.findViewById(R.id.button_no);
        mNoButton.setOnClickListener(this);
        if (!TextUtils.isEmpty(noButtonText)) {
            mNoButton.setText(noButtonText);
        }
        view.findViewById(R.id.button_ok).setOnClickListener(this);
        
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(0));	
    }

    public ChangePhoneDialog setListener(DialogInterface.OnClickListener listener) {
        mListener = listener;
        return this;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
    	Dialog dialog = super.onCreateDialog(savedInstanceState);
    	
        return dialog;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_no:
                if (mListener != null) {
                	Log.e("Negative", "click");
                    mListener.onClick(null, Dialog.BUTTON_NEGATIVE);
                }
                break;
            case R.id.button_yes:
            	 if (mListener != null) {
            		 Log.e("Positive", "click");
                     mListener.onClick(null, Dialog.BUTTON_POSITIVE);
                 }
            	String phoneNumber = edt_phone_change_phone_dialog.getText().toString();
            	
            	parseUser.put("phone", phoneNumber);
            	parseUser.saveInBackground(new SaveCallback() {
					
					@Override
					public void done(ParseException e) {
						if (e==null){
							Log.e("Phone", "saved");
						}
						else{
							e.printStackTrace();
						}
						
					}
				});
            	
            	
                break;
            case R.id.button_ok:
                if (mListener != null) {
                	Log.e("Positive", "click");
                    mListener.onClick(null, Dialog.BUTTON_POSITIVE);
                }
                break;
                default:
                	 if (mListener != null) {
                		 Log.e("Default", "click");
                         mListener.onClick(null, Dialog.BUTTON_NEGATIVE);
                     }
                	//mListener.onClick(null, Dialog.BUTTON_NEGATIVE);
        }
        /*if (mListener != null) {
        	Log.e("Outside Default", "click");
            mListener.onClick(null, Dialog.BUTTON_NEGATIVE);
        }*/
    }

    public interface ButtonsLayout {
        int CUSTOM = 0;
        int TWO_BUTTONS = 1;
        int ONE_BUTTON = 2;
    }

    public interface Buttons {
        int OK = 0;
        int YES = 1;
        int NO = 2;
    }

    public void setButtonText(int button, String text) {
        switch (button) {
            case Buttons.OK:

                break;
            case Buttons.YES:
                getArguments().putString(EXTRA_BUTTON_YES, text);
                break;
            case Buttons.NO:
                getArguments().putString(EXTRA_BUTTON_NO, text);
                break;
        }
    }

}
