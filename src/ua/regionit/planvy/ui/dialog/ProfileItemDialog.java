
package ua.regionit.planvy.ui.dialog;


import org.holoeverywhere.app.Dialog;
import org.holoeverywhere.app.DialogFragment;
import org.holoeverywhere.drawable.ColorDrawable;
import org.holoeverywhere.widget.LinearLayout;

import ua.regionit.planvy.R;
import ua.regionit.planvy.ui.ProfileActivity;
import ua.regionit.planvy.util.MD5Hash;
import ua.regionit.planvy.util.Support;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;


public class ProfileItemDialog extends DialogFragment implements OnClickListener {

    private static final String EXTRA_BUTTON_NO = "extra_button_no";

    private static final String EXTRA_BUTTON_YES = "extra_button_yes";

    private View source;
    public static final String EXTRA_DESCRIPTION = "extra_description";
    public static final String EXTRA_NICKNAME = "extra_nickame";
    public static final String EXTRA_DONATE_MESSAGE = "extra_donate_message";
    public static final String EXTRA_CHARITY_TITLE = "extra_charity_title";
    public static final String EXTRA_AMOUNT_PAID = "extra_amount_paid";
    public static final String EXTRA_BUTTONS_LAYOUT = "extra_buttons";
    public static final String EXTRA_DATE_OF_MESSAGE = "extra_date_of_message";
    private TextView nickname_profile_item_dialog;
    private TextView description_profile_item_dialog;
    private TextView donated_message_profile_item_dialog;
    Typeface light;
    public static Bitmap mAvatar= null;
    Bitmap resized_bitmap;
    private android.content.DialogInterface.OnClickListener mListener;

    private LinearLayout pic_here_profile_item_dialog;
    private Button mYesButton;

    private Button mNoButton;
    private ImageView img_user_picture_profile_item;
    int value_width_height;
    static Context mContext;
    int sdk ;
    static int mResourse;
    public static final int SEND_TO_ME=1;
    public static final int SEND_BY_ME=2;
    String donatedMessage = "";
    
    
    public static ProfileItemDialog newInstance(Context context, String nickname, String description, String urlToPhoto,  Bitmap avatar, String charityTitle, int amountPaid, String dateOfMessage, int resourse,  int layout) {
        ProfileItemDialog d = new ProfileItemDialog();
        Bundle args = new Bundle();
        args.putString(EXTRA_NICKNAME, nickname);
        args.putString(EXTRA_DESCRIPTION, description);
        
        args.putString(EXTRA_CHARITY_TITLE, charityTitle);
        args.putInt(EXTRA_AMOUNT_PAID, amountPaid);
        args.putString(EXTRA_DATE_OF_MESSAGE, dateOfMessage);
        args.putInt(EXTRA_BUTTONS_LAYOUT, layout);
        d.setArguments(args);
        mResourse = resourse;
        mContext = context;
        mAvatar = avatar;
        return d;
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_FRAME, R.style.DialogStyle);
        light = Typeface.createFromAsset(getActivity().getAssets(), "HelveticaNeueLight.ttf");
    }

    @Override
    public View onCreateView(org.holoeverywhere.LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	// getDialog().getWindow().setGravity(Gravity.CENTER_HORIZONTAL | Gravity.TOP);
    	 
        /*View view = inflater.inflate(R.layout.profile_list_item_dialog, null);
        ViewGroup.LayoutParams lp = view.getLayoutParams();

        lp.width=50;

        view.setLayoutParams(lp);

        view.invalidate();*/
        return inflater.inflate(R.layout.profile_list_item_dialog, null);
    }

    @SuppressWarnings("deprecation")
	@SuppressLint("NewApi")
	@Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        nickname_profile_item_dialog = (TextView)view.findViewById(R.id.nickname_profile_item_dialog);
        description_profile_item_dialog = (TextView)view.findViewById(R.id.description_profile_item_dialog);
        donated_message_profile_item_dialog = (TextView)view.findViewById(R.id.donated_message_profile_item_dialog);
        donated_message_profile_item_dialog.setTypeface(light);
        img_user_picture_profile_item = (ImageView) view.findViewById(R.id.img_user_picture_profile_item);
        pic_here_profile_item_dialog = (LinearLayout) view.findViewById(R.id.pic_here_profile_item_dialog);
        String nickname = null;
        String description = null;
        
        int buttonsLayout = ButtonsLayout.ONE_BUTTON;
        String yesButtonText = null;
        String noButtonText = null;
        String charityTitle = null;
        String dateOfMessage = null;
        int amountPaid = 0;

        Bundle arguments = getArguments();
        if (arguments != null) {
        	nickname = arguments.getString(EXTRA_NICKNAME); 
        	description = arguments.getString(EXTRA_DESCRIPTION);
            buttonsLayout = arguments.getInt(EXTRA_BUTTONS_LAYOUT);
            yesButtonText = arguments.getString(EXTRA_BUTTON_YES);
            noButtonText = arguments.getString(EXTRA_BUTTON_NO);
            charityTitle = arguments.getString(EXTRA_CHARITY_TITLE);
            amountPaid = arguments.getInt(EXTRA_AMOUNT_PAID);  
            dateOfMessage =arguments.getString(EXTRA_DATE_OF_MESSAGE);
        }
        if (nickname.equals("")){
        	nickname_profile_item_dialog.setVisibility(View.GONE);
        }
        value_width_height = (int) ProfileActivity.dipToPixels(mContext, 80);
        
        Bitmap dnt_cellmask = BitmapFactory.decodeResource(getResources(),
                R.drawable.nvt_photo_mask);
    	Bitmap dnt_cellmask_resized = Bitmap.createScaledBitmap(dnt_cellmask, value_width_height, value_width_height, true);
    	img_user_picture_profile_item.setImageBitmap(dnt_cellmask_resized);
    	if (mAvatar!= null){
    		Bitmap bitmap = Support.optimizeBitmap(mAvatar);
    	resized_bitmap=Bitmap.createScaledBitmap(bitmap, value_width_height, value_width_height, true);
    	Drawable drawable = new BitmapDrawable( mContext.getResources(),resized_bitmap);
    	if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
    		pic_here_profile_item_dialog.setBackgroundDrawable(drawable);
	            } else {
	            	pic_here_profile_item_dialog.setBackground(drawable);
	            }
    	}
    	else{
    		pic_here_profile_item_dialog.setVisibility(View.GONE);
    	}
        nickname_profile_item_dialog.setText(nickname);
        description_profile_item_dialog.setText(description +"\non "+ dateOfMessage);
        Log.e("Charity", charityTitle);
        if (charityTitle.equals("")){
        	donated_message_profile_item_dialog.setVisibility(View.GONE);
        }
        else{
        	if (mResourse== SEND_TO_ME){
        		donatedMessage= "You donated " + "$" +  amountPaid+ "\nto " + charityTitle;}
        	
        	else{
        	donatedMessage= nickname  + " donated $"+ amountPaid + "\nto " + charityTitle ;}
        donated_message_profile_item_dialog.setText(donatedMessage);
        donated_message_profile_item_dialog.setVisibility(View.VISIBLE);
        }
        switch (buttonsLayout) {
            case ButtonsLayout.ONE_BUTTON:
                view.findViewById(R.id.button_ok).setVisibility(View.VISIBLE);
                
                break;
            case ButtonsLayout.TWO_BUTTONS:
                view.findViewById(R.id.button_ok).setVisibility(View.GONE);
               
                break;
           
        }
        
        view.findViewById(R.id.button_ok).setOnClickListener(this);
        
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
       
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(0));	
       
    }

    
    
    
    
    public ProfileItemDialog setListener(DialogInterface.OnClickListener listener) {
        mListener = listener;
        return this;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
    	Dialog dialog = super.onCreateDialog(savedInstanceState);
        return dialog;
    }
    
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            
            case R.id.button_ok:
                if (mListener != null) {
                    mListener.onClick(null, Dialog.BUTTON_POSITIVE);
                }
                break;
        }
        dismiss();
    }

    public interface ButtonsLayout {
        int CUSTOM = 0;
        int TWO_BUTTONS = 1;
        int ONE_BUTTON = 2;
    }

    public interface Buttons {
        int OK = 0;
        int YES = 1;
        int NO = 2;
    }

    public void setButtonText(int button, String text) {
        switch (button) {
            case Buttons.OK:

                break;
            case Buttons.YES:
                getArguments().putString(EXTRA_BUTTON_YES, text);
                break;
            case Buttons.NO:
                getArguments().putString(EXTRA_BUTTON_NO, text);
                break;
        }
    }

}
