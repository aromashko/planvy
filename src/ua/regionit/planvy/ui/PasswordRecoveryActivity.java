package ua.regionit.planvy.ui;


import java.util.List;

import org.holoeverywhere.app.Activity;
import org.holoeverywhere.widget.Button;
import org.holoeverywhere.widget.LinearLayout;
import org.holoeverywhere.widget.ProgressBar;
import org.holoeverywhere.widget.TextView;

import ua.regionit.planvy.ErrorCode;
import ua.regionit.planvy.R;
import ua.regionit.planvy.ui.dialog.NotifyDialog;
import ua.regionit.planvy.util.Support;
import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

import com.flurry.android.FlurryAgent;
import com.parse.FindCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.RequestPasswordResetCallback;


public class PasswordRecoveryActivity extends Activity implements OnClickListener {
	String email,nickname;
	TextView tv_greetingText, tv_warning_find_account_activity;
	ImageView imgUserPicture, imgbackButton;
	Button btn_send_password;
	ProgressBar progressBar_find_account;
	LinearLayout pic_here;
	String send_pass_rec_text = "An email has been sent to your account's email address. Please check you email to continue. If you are still having problems, please visit \nPlanvy Hep Center";
	ParseQuery<ParseUser> query;
	NotifyDialog dialogFragment;
	int sdk;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.find_the_account);
		FlurryAgent.logEvent("PasswordRecovery", true);
		initUi();
		initListener();
		sdk= android.os.Build.VERSION.SDK_INT;
		
		Intent intent = getIntent();
		email = intent.getStringExtra(FindAccountActivity.EMAIL);
		nickname = intent.getStringExtra(FindAccountActivity.NICKNAME);
		tv_greetingText.setText("Hi " + nickname+ "!\nWould you like to reset\n you password?");
		tv_warning_find_account_activity.setVisibility(View.GONE);
		progressBar_find_account.setVisibility(View.VISIBLE);
		query = ParseUser.getQuery();
		query.whereEqualTo("email", email);
		query.findInBackground(new FindCallback<ParseUser>() {
		  public void done(List<ParseUser> objects, ParseException e) {
			  ParseFile parseFile = objects.get(0).getParseFile("photo");
			  if(parseFile != null){
				parseFile.getDataInBackground(new GetDataCallback() {
					
					@SuppressLint("NewApi")
					@SuppressWarnings("deprecation")
					public void done(byte[] data, ParseException e) {
					    if (e == null) {
					    	int value_width_height = (int) Support.dipToPixels(PasswordRecoveryActivity.this, 100);
					    progressBar_find_account.setVisibility(View.GONE);
					    Log.d("ParseByte",data.toString());
					    Bitmap bmp = BitmapFactory.decodeByteArray(data, 0, data.length);
					    Bitmap bitmap = Support.optimizeBitmap(bmp);
					    Bitmap bitmap_resized = Bitmap.createScaledBitmap(bitmap, value_width_height, value_width_height, true);
				    	Drawable d = new BitmapDrawable(getResources(),bitmap_resized);
				    	 if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
				    		 pic_here.setBackgroundDrawable(d);
				            } else {
				            	pic_here.setBackground(d);
				            }
					    } else {
					    	int err = e.getCode();
	        		    	Log.d("Error code:", Integer.toString(err));
	            	    	switch(err){
					    case ErrorCode.CONNECTION_FAILED:
					    	tv_warning_find_account_activity.setText("Problem with Internet. Please try again.");
					    	tv_warning_find_account_activity.setVisibility(View.VISIBLE);
            				break;
	            	    	}
					    	progressBar_find_account.setVisibility(View.GONE);
					      e.printStackTrace();
					    }
					  }
				});
			  }
			  else{
				  progressBar_find_account.setVisibility(View.GONE);
			  }
		  }
		});
	}
	
	@Override
	protected void onStart()
	{
		super.onStart();
		FlurryAgent.onStartSession(this, "XWHZX63GBXQKSR9KJGQ9");
	}
	
	@Override
	protected void onStop()
	{
		super.onStop();		
		FlurryAgent.onEndSession(this);
	}
	
	public void initUi(){
		tv_greetingText = (TextView) findViewById(R.id.tv_greeting_text);
		imgUserPicture = (ImageView) findViewById(R.id.img_user_picture);
		btn_send_password = (Button) findViewById(R.id.btn_send_password);
		progressBar_find_account = (ProgressBar) findViewById(R.id.progressBar_find_account);
		pic_here = (LinearLayout) findViewById(R.id.pic_here);
		tv_warning_find_account_activity = (TextView) findViewById(R.id.tv_warning_find_account_activity);
		imgbackButton= (ImageView) findViewById(R.id.imgbackButton);
	}
	public void initListener(){
		btn_send_password.setOnClickListener(this);	
		imgbackButton.setOnClickListener(this);	
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.btn_send_password:
			progressBar_find_account.setVisibility(View.VISIBLE);
			ParseUser.requestPasswordResetInBackground(email,
	                new RequestPasswordResetCallback() {
				public void done(ParseException e) {
					if (e == null) {
						progressBar_find_account.setVisibility(View.GONE);
						dialogFragment = NotifyDialog.newInstance("",
				                send_pass_rec_text,140, NotifyDialog.ButtonsLayout.ONE_BUTTON);
				        dialogFragment.setListener(new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {
								dialogFragment.dismiss();
							startActivity(new Intent(PasswordRecoveryActivity.this, LoginActivity.class));	
								finish();
							}
				        	
				        });
				        dialogFragment.show(PasswordRecoveryActivity.this);
						
					} else {
						progressBar_find_account.setVisibility(View.GONE);
						int err = e.getCode();
						switch(err){
						default:
							//NotifyDialog notifyDialog = NotifyDialog.newInstance("", e.getMessage());
							tv_warning_find_account_activity.setText(e.getMessage());
							tv_warning_find_account_activity.setVisibility(View.VISIBLE);
						}
						Log.d("Error code:", Integer.toString(err));
	        	    	e.printStackTrace();
	        	    	String message = e.getMessage();
	        	    	Log.d("Error Message:",message);
	    		    	
					}
				}
			});
			break;
		case R.id.imgbackButton:
			onBackPressed();
			break;
		}
		
	}
	
}
