package ua.regionit.planvy.ui;

import java.util.ArrayList;
import java.util.List;

import ua.regionit.planvy.R;
import ua.regionit.planvy.ui.views.PinnedSectionListView.PinnedSectionListAdapter;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

	public class ContactAdapter extends BaseAdapter implements Filterable,PinnedSectionListAdapter {
		 
		 ArrayList<ContactModel> FilterResults=null;
		 Context mContext;
		 ArrayList<ContactModel> itemsArrayList, filteredItems;
		 ArrayList<ContactModel> mArr;
		 LayoutInflater mInf;
		 ContactModel med;
		 ArrayList<ContactModel> filteredData;
		 Filter filter;
		 ContactAdapter adapter;
		 int pos;
		 private LayoutInflater mInflater;
		 public static final int FIRST = 0;
	     public static final int SECOND = 1;
		 
		 public ContactAdapter(Context context, ArrayList<ContactModel> mContacts){
		  this.mContext = context;
		  this.itemsArrayList = mContacts;
		  this.mArr=mContacts;
		  mInf = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		  this.adapter = this;
		 }
		 @Override
		 public int getCount() {
		  // TODO Auto-generated method stub
		  return itemsArrayList.size();
		 }

		 @Override
		 public Object getItem(int position) {
		  // TODO Auto-generated method stub
		  return itemsArrayList.get(position);
		 }

		 @Override
		 public long getItemId(int position) {
		  // TODO Auto-generated method stub
		  return position;
		 }

		 /*@Override
		 public View getView(int position, View convertView, ViewGroup parent) {
		  // TODO Auto-generated method stub
		  final ViewHolder vh1;
		  if(convertView != null){
		   vh1 = (ViewHolder)convertView.getTag();
		  }else{
		   convertView = mInf.inflate(R.layout.ad_people_item, parent,false);
		   
		   vh1 = new ViewHolder();
		   vh1.tvName = (TextView)convertView.findViewById(R.id.tv_greeting_text);
		   vh1.cb = (CheckBox)convertView.findViewById(R.id.cb_item);
		   vh1.llTag = (LinearLayout)convertView.findViewById(R.id.lltag);
		   convertView.setTag(vh1);
		  }
		 pos=position;
		 vh1.llTag.setTag(position);
		 vh1.tvName.setText(itemsArrayList.get(position).getName());
		 
		 if(itemsArrayList.get(position).isSelected()){
			 vh1.cb.setChecked(true);
		 }else{
			 vh1.cb.setChecked(false);
		 }
		 
		  vh1.cb.setOnClickListener(new OnClickListener() {

              @Override
              public void onClick(View v) {

                
                  ContactModel p = itemsArrayList.get((Integer)vh1.llTag.getTag());
                  if (!p.isSelected()) {
                      p.setSelected(true);
                  } else {
                      p.setSelected(false);
                  }
                                }
          });
		 
		  return convertView;
		 }*/
		 
		 @Override
	        public View getView(int position, View convertView, ViewGroup parent) {
	            int viewType = getItemViewType(position);
	            if(viewType == FIRST){
	                convertView = mInflater.inflate(R.layout.ad_people_header_item,parent,false);
	                ((TextView)convertView.findViewById(R.id.tv_add_people_header_text)).setText(itemsArrayList.get(position).getName());

	            }
	            if(viewType == SECOND){
	                convertView = mInflater.inflate(R.layout.ad_people_item,parent,false);
	                ((TextView)convertView.findViewById(R.id.tv_add_people_text)).setText(itemsArrayList.get(position).getName());
	            }
	            return convertView;
	        }

		 static class ViewHolder{
		  TextView tvName;
		  CheckBox cb;
		  LinearLayout llTag;
		 }
	    
	   		public List<ContactModel> getSelectedItems() {
	        List<ContactModel> selectedItems = new ArrayList<ContactModel>();
	        for (int i = 0; i < getCount(); i++) {
	        	ContactModel item = itemsArrayList.get(i);
	            if (item.isSelected()) {
	                selectedItems.add(item);
	            }
	        }
	        return selectedItems;
	    }
	   			 
	   	    
	    
	    public void Remove(int pos){
	    	ContactModel p = itemsArrayList.get(pos);
	    	Log.d("Udalit ===>", Integer.toString(pos));
            itemsArrayList.remove(p);
            notifyDataSetChanged();       
	    }
	    
	    @Override
	    public Filter getFilter() {
	    		
	    	
	        Filter filter = new Filter() {
	            @SuppressWarnings("unchecked")
	            @Override
	            protected void publishResults(CharSequence constraint, FilterResults results) {
	                itemsArrayList =  (ArrayList<ContactModel>) results.values;
	                notifyDataSetChanged();
	            }
	            @Override
	            protected FilterResults performFiltering(CharSequence constraint) {

	                FilterResults results = new FilterResults();
	               
	                if(constraint == null || constraint.length() == 0){
	                    results.count = mArr.size();
	                    results.values = mArr;
	                    return results;
	                }
	                else{
	                	//constraint = constraint.toString().toLowerCase();
	                    ArrayList<ContactModel> FilteredArrayNames = new ArrayList<ContactModel>();
	                    Log.e("constraint", constraint.toString()+ " fh "+ constraint.length());
	                    for(ContactModel cModel:mArr){
	                    	String filter_string = constraint.toString().toLowerCase();
	                    	String rightArg = cModel.getName().toLowerCase();
	                    	if(constraint.toString().equals("")){
	                    		FilteredArrayNames.addAll(mArr);
	     	                   
	                    	} else if(rightArg.startsWith(filter_string)){
	                    		FilteredArrayNames.add(cModel);
	                        }
	                    }
	                    results.count = FilteredArrayNames.size();
	                    results.values = FilteredArrayNames;
	                }
	                return results;
	            }
	        };
	        return filter;
	    	
	    }
		public void add(ContactModel cm) {
			// TODO Auto-generated method stub
			itemsArrayList.add(cm);
			notifyDataSetChanged();
		}
		
		@Override
        public boolean isItemViewTypePinned(int viewType) {
            if(viewType == FIRST){
                return true;
            }else{
                return false;
            }

        }

		 @Override
	        public int getItemViewType(int position) {
	            if (itemsArrayList.get(position).equals("1")){
	                return SECOND;
	            }else{
	                return FIRST;
	            }

	        }
		
		 @Override
	        public int getViewTypeCount() {
	            return 2;
	        }

}
