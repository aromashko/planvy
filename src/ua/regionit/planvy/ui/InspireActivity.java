package ua.regionit.planvy.ui;

import org.holoeverywhere.app.Activity;
import org.holoeverywhere.widget.Button;
import org.holoeverywhere.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.parse.ParseUser;

import ua.regionit.planvy.R;
import ua.regionit.planvy.ui.dialog.NotifyDialog;
import ua.regionit.planvy.util.CustomTypefaceSpan;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class InspireActivity extends Activity implements OnClickListener{
	Button send_invite, btn_donate, btn_cancel;
	NotifyDialog dialogFragment;
	TextView greating, send, or;
	Typeface helveticaNeue, helveticaLight, helveticaUltraLight, helveticaLigthItalic;
	ImageView back_btn;
	ParseUser currentUser;
	RelativeLayout lay_action_bar_inspire;
	int height_lay_action_bar;
	Boolean userLoggedIn =false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.inspire);
		FlurryAgent.logEvent("Inspire", true);
		initUi();
		initListener();
		lay_action_bar_inspire.measure(MeasureSpec.UNSPECIFIED,MeasureSpec.UNSPECIFIED);
		height_lay_action_bar = lay_action_bar_inspire.getMeasuredHeight();
		currentUser = ParseUser.getCurrentUser();
		helveticaNeue = Typeface.createFromAsset(getAssets(), "HelveticaNeue.ttf");
		helveticaLight = Typeface.createFromAsset(getAssets(), "HelveticaNeueLight.ttf");
		helveticaUltraLight = Typeface.createFromAsset(getAssets(), "HelveticaNeueUltraLight.ttf");
		helveticaLigthItalic = Typeface.createFromAsset(getAssets(), "HelveticaNeueLightItalic.ttf");
		
		greating.setTypeface(helveticaNeue);
		final SpannableStringBuilder sb = new SpannableStringBuilder("Send invitation to  make donation:");
        sb.setSpan(new CustomTypefaceSpan("", helveticaUltraLight), 0, 5, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        sb.setSpan(new CustomTypefaceSpan("", helveticaLight), 5, 16, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        sb.setSpan(new CustomTypefaceSpan("", helveticaUltraLight), 16, 24, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        sb.setSpan(new CustomTypefaceSpan("", helveticaLight), 25, 34, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        send.setText(sb);
		or.setTypeface(helveticaLigthItalic);
		
		if (currentUser == null){
			btn_cancel.setText("Back to Login");
		}
		else{
			userLoggedIn = true;
			back_btn.setVisibility(View.GONE);
			lay_action_bar_inspire.getLayoutParams().height = height_lay_action_bar;
		}
		//lay_action_bar_inspire.setO
	}
	
	@Override
	protected void onStart()
	{
		super.onStart();
		FlurryAgent.onStartSession(this, "XWHZX63GBXQKSR9KJGQ9");
	}
	
	@Override
	protected void onStop()
	{
		super.onStop();		
		FlurryAgent.onEndSession(this);
	}
	
	public void initUi(){
		lay_action_bar_inspire = (RelativeLayout) findViewById(R.id.lay_action_bar_inspire);
		send_invite = (Button) findViewById(R.id.btn_send_inspire_invite);
		btn_donate = (Button) findViewById(R.id.donate);
		btn_cancel = (Button) findViewById(R.id.cancel);
		greating = (TextView) findViewById(R.id.tv_greeting_text);
		send = (TextView) findViewById(R.id.tv_send_invitation);
		or = (TextView) findViewById(R.id.tv_inspire_or);
		back_btn = (ImageView) findViewById(R.id.img_back_button_inspire);
	}
	public void initListener(){
		send_invite.setOnClickListener(this);
		btn_donate.setOnClickListener(this);
		btn_cancel.setOnClickListener(this);
		back_btn.setOnClickListener(this);
	}
	
	@Override
	public void onBackPressed() {
		//super.onBackPressed();
		if (userLoggedIn){
			startActivity(new Intent(InspireActivity.this, ProfileActivity.class));
		}
		else{
			startActivity(new Intent(InspireActivity.this, LoginActivity.class));
			
		}
		
	}
	
	
	@Override
	public void onClick(View v) {
		switch (v.getId()){
		case R.id.btn_send_inspire_invite:
			/*dialogFragment = NotifyDialog.newInstance("","Please allow Planvy  to access your Contacts", NotifyDialog.ButtonsLayout.TWO_BUTTONS);
			        dialogFragment.setListener(new DialogInterface.OnClickListener() {

			            @Override
			            public void onClick(DialogInterface dialog, int which) {
			                if (which == Dialog.BUTTON_POSITIVE) {
			                	dialogFragment.dismiss();*/
			                	startActivity(new Intent(InspireActivity.this, AddPeopleActivity.class));
			        			overridePendingTransition(R.anim.slide_left_in, R.anim.slide_left_out);
			             /*   }
			            }
			        });
			        dialogFragment.show(this);*/
			
		break;
		case R.id.donate:
			startActivity(new Intent(InspireActivity.this, DonateWithOutInvite.class));
			overridePendingTransition(R.anim.slide_left_in, R.anim.slide_left_out);
		break;
		case R.id.cancel:
			onBackPressed();
			
		break;
		case R.id.img_back_button_inspire:
			onBackPressed();
		break;
		}
	}
	
}
