package ua.regionit.planvy.ui;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.holoeverywhere.LayoutInflater;
import org.holoeverywhere.app.Activity;
import org.holoeverywhere.widget.Button;
import org.holoeverywhere.widget.EditText;
import org.holoeverywhere.widget.LinearLayout;
import org.holoeverywhere.widget.ProgressBar;
import org.holoeverywhere.widget.TextView;
import org.holoeverywhere.widget.Toast;
import org.holoeverywhere.widget.ViewPager;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import ua.regionit.planvy.AppLog;
import ua.regionit.planvy.ErrorCode;
import ua.regionit.planvy.R;
import ua.regionit.planvy.ui.dialog.DonateFromInviteDialog;
import ua.regionit.planvy.ui.dialog.NotifyDialog;
import ua.regionit.planvy.ui.dialog.NotifyDialogDonate;
import ua.regionit.planvy.util.Support;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ScrollView;

import com.flurry.android.FlurryAgent;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;







public class DonateFromInviteActivity extends Activity implements OnClickListener {
	TextView tv_nickname_donate_from_invite, tv_description_donate_from_invite, tv_title_donate_from_invite, tv_from;
	EditText edit_donate_summ_donate_from_invite;
	Button btn_give_donate_from_invite, btn_cancel_donate_from_invite;
	ImageView imgbackButton, img_user_picture_donate_from_invite;
	static EditText mEditText;
	ParseUser currentUser;
	int value_width_height, sdk, mPosition;
	LinearLayout lay_user_picture_donate_from_invite;
	Bitmap resizedBitmap;
	int number_charity_list=0, mPositionClicked=-1, currentItem, amount;
	DisplayImageOptions options;
	ViewPager pager;
	String charityDescription, title, email, photoThumbnail, photoFull, objectIdInvite, objectIdCharity, pay_key;
	protected ImageLoader imageLoader = ImageLoader.getInstance();
	String[] imageUrls;
	List<String> arrImageUrls, arrTitle, arrCharityDescription, arrPhotoFull, arrObjectIdCharity;
	Bitmap thumbnail, resizedThumbnail;
	ImageView imageView;
	ProgressBar spinner;
	View imageLayout;
	ScrollView scrollview_donate_from_invite;
	Bitmap dnt_cellmask_resized, donate_cellmask_red_resized;
	Boolean isCharityChoosen = false, donationConfirmed = false;
	Typeface helveticaNeue, helveticaLight, helveticaUltraLight, helveticaLigthItalic;
	
	public static final String RESPONSE = "response";
	public static final String SHORT_DESCRIPTION = "short_description";
	public static final String AMOUNT = "amount";
	
	public static final String PROOF_OF_PAYMENT = "proof_of_payment";
	public static final String ADAPTIVE_PAYMENT = "adaptive_payment";
	public static final String PAYMENT_EXEC_STATUS = "payment_exec_status";
	public static final String ID = "id";
	public static final String STATE="state";
	
	
	// set to PaymentActivity.ENVIRONMENT_PRODUCTION to move real money.
    // set to PaymentActivity.ENVIRONMENT_SANDBOX to use your test credentials from https://developer.paypal.com
    // set to PaymentActivity.ENVIRONMENT_NO_NETWORK to kick the tires without communicating to PayPal's servers.
	private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_PRODUCTION;
    
    // note that these credentials will differ between live & sandbox environments.
    private static final String CONFIG_CLIENT_ID = "AR_wzBAAqbal-WI7Qd1rmtuM6VATjAHWKpnWImY-WFWwCJDNg8dADbRYVFGO";
    // when testing in sandbox, this is likely the -facilitator email address. 
    //private static final String CONFIG_RECEIVER_EMAIL = "admin@planvy.com";
    
    private static PayPalConfiguration config = new PayPalConfiguration()
    .environment(CONFIG_ENVIRONMENT)
    .clientId(CONFIG_CLIENT_ID);
    
    NotifyDialogDonate notifyDialogDonate;
    String short_description; 
    NotifyDialog notifyDialogInternet;
    
    public String mDonationInvoiceToken;
    
    MyAsyncTask myAsyncTask;
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.donate_activity);
		FlurryAgent.logEvent("PayForInvite", true);
		
		sdk = android.os.Build.VERSION.SDK_INT;
		initUi();
		initListener();
		initArray();
		
		helveticaNeue = Typeface.createFromAsset(getAssets(), "HelveticaNeue.ttf");
		helveticaLight = Typeface.createFromAsset(getAssets(), "HelveticaNeueLight.ttf");
		helveticaUltraLight = Typeface.createFromAsset(getAssets(), "HelveticaNeueUltraLight.ttf");
		helveticaLigthItalic = Typeface.createFromAsset(getAssets(), "HelveticaNeueLightItalic.ttf");
		
		edit_donate_summ_donate_from_invite.setSelection(edit_donate_summ_donate_from_invite.getText().length());
		addOnTextChangeListener(edit_donate_summ_donate_from_invite);
		currentUser = ParseUser.getCurrentUser();
		Intent intent = getIntent();
		String nickname = intent.getStringExtra("nickname");
		String description = intent.getStringExtra("description");
		String url = intent.getStringExtra("url");
		Log.d("url",url);
		objectIdInvite = intent.getStringExtra("objectId");
		Log.d("objectId",objectIdInvite);
		tv_nickname_donate_from_invite.setText(nickname);
		tv_description_donate_from_invite.setMovementMethod(new ScrollingMovementMethod());
		tv_description_donate_from_invite.setText(description);
		tv_description_donate_from_invite.setTypeface(helveticaNeue);
		tv_nickname_donate_from_invite.setTypeface(helveticaNeue);
		tv_from.setTypeface(helveticaUltraLight);
		if (!url.equals("")){
		setUserPicture(url);
		}
		getCharityList(0,20);
		setViewPagerOnTouchListener();
		
		options = new DisplayImageOptions.Builder()
		.showImageForEmptyUri(R.drawable.ic_empty)
		.showImageOnFail(R.drawable.ic_error)
		.resetViewBeforeLoading(true)
		.cacheInMemory(true)
		.cacheOnDisc(true)
		.imageScaleType(ImageScaleType.EXACTLY)
		.bitmapConfig(Bitmap.Config.RGB_565)
		.considerExifParams(true)
		.displayer(new FadeInBitmapDisplayer(300))
		.build();
		
		/*Intent intent_pay = new Intent(this, PayPalService.class);
        
		intent_pay.putExtra(PaymentActivity.EXTRA_PAYPAL_ENVIRONMENT, CONFIG_ENVIRONMENT);
		intent_pay.putExtra(PaymentActivity.EXTRA_CLIENT_ID, CONFIG_CLIENT_ID);
		intent_pay.putExtra(PaymentActivity.EXTRA_RECEIVER_EMAIL, CONFIG_RECEIVER_EMAIL);
        
        startService(intent_pay);*/
        
        Intent intentPayPal = new Intent(this, PayPalService.class);
        intentPayPal.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        startService(intentPayPal);
		
		
	}
	
	@Override
	protected void onStart()
	{
		super.onStart();
		FlurryAgent.onStartSession(this, "XWHZX63GBXQKSR9KJGQ9");
	}
	
	@Override
	protected void onStop()
	{
		super.onStop();		
		FlurryAgent.onEndSession(this);
	}
	
	public void initUi(){
		tv_nickname_donate_from_invite = (TextView) findViewById(R.id.tv_nickname_donate_from_invite);
		tv_description_donate_from_invite = (TextView) findViewById(R.id.tv_description_donate_from_invite);
		edit_donate_summ_donate_from_invite = (EditText) findViewById(R.id.edit_donate_summ_donate_from_invite);
		btn_give_donate_from_invite = (Button) findViewById(R.id.btn_give_donate_from_invite);
		btn_cancel_donate_from_invite = (Button) findViewById(R.id.btn_cancel_donate_from_invite);
		imgbackButton = (ImageView) findViewById(R.id.imgbackButton);
		img_user_picture_donate_from_invite = (ImageView) findViewById(R.id.img_user_picture_donate_from_invite);
		lay_user_picture_donate_from_invite  = (LinearLayout) findViewById(R.id.lay_user_picture_donate_from_invite);
		pager = (ViewPager) findViewById(R.id.pager);
		scrollview_donate_from_invite = (ScrollView) findViewById(R.id.scrollview_donate_from_invite);
		tv_title_donate_from_invite = (TextView) findViewById(R.id.tv_title_donate_from_invite);
		tv_from = (TextView) findViewById(R.id.donate_invite_from);
	}
	public void initListener(){
		btn_give_donate_from_invite.setOnClickListener(this);
		btn_cancel_donate_from_invite.setOnClickListener(this);
		imgbackButton.setOnClickListener(this);
	}
	
	public void initArray(){
		arrImageUrls = new ArrayList<String>();
		arrTitle = new ArrayList<String>();
		arrCharityDescription = new ArrayList<String>();
		arrPhotoFull = new ArrayList<String>();
		arrObjectIdCharity = new ArrayList<String>();
	}
	

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.btn_give_donate_from_invite:
			if (!Support.isNetworkConnected(DonateFromInviteActivity.this)){
				notifyDialogInternet = NotifyDialog.newInstance(getString(R.string.no_internet_title), getString(R.string.no_internet_text), NotifyDialog.ButtonsLayout.ONE_BUTTON);
		    	notifyDialogInternet.show(DonateFromInviteActivity.this);
				break;
			}
			String donate_sum = edit_donate_summ_donate_from_invite.getText().toString();
			if (donate_sum.length() <=1){
				NotifyDialog notifyDialog = NotifyDialog.newInstance("Warning", "Enter donate sum", NotifyDialog.ButtonsLayout.ONE_BUTTON);
				notifyDialog.show(this);
				break;
			}
			if (!isCharityChoosen){
				NotifyDialog notifyDialog = NotifyDialog.newInstance("Warning", "Choose charity cause", NotifyDialog.ButtonsLayout.ONE_BUTTON);
				notifyDialog.show(this);
				break;
			}
			String donate_sum_cut= donate_sum.substring(1, donate_sum.length());
			
			/* DecimalFormat df = (DecimalFormat) NumberFormat.getInstance(Locale.ENGLISH);
	            df.setParseBigDecimal(true);
			try {
				bigDecimal = (BigDecimal) df.parseObject(donate_sum_cut);
			} catch (java.text.ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
			amount= Integer.valueOf(donate_sum_cut);
			short_description = arrTitle.get(mPositionClicked);
			 PayPalPayment thingToBuy = new PayPalPayment(new BigDecimal(donate_sum_cut), "USD", 
					 arrTitle.get(mPositionClicked),PayPalPayment.PAYMENT_INTENT_SALE);
			 
			 	getDonationInvoiceToken();
		        Intent intent = new Intent(this, PaymentActivity.class);
		        
		        /*intent.putExtra(PaymentActivity.EXTRA_PAYPAL_ENVIRONMENT, CONFIG_ENVIRONMENT);
		        intent.putExtra(PaymentActivity.EXTRA_CLIENT_ID, CONFIG_CLIENT_ID);
		        intent.putExtra(PaymentActivity.EXTRA_RECEIVER_EMAIL, CONFIG_RECEIVER_EMAIL);
		        
		        // It's important to repeat the clientId here so that the SDK has it if Android restarts your 
		        // app midway through the payment UI flow.
		        intent.putExtra(PaymentActivity.EXTRA_CLIENT_ID, CONFIG_CLIENT_ID);*/
		        //intent.putExtra(PaymentActivity.EXTRA_PAYER_ID, "your-customer-id-in-your-system");
		        //intent.putExtra(PaymentActivity.EXTRA_RECEIVER_EMAIL, "admin@planvy.com");
		       
		        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);
		        
		        startActivityForResult(intent, 0);
			break;
		case R.id.btn_cancel_donate_from_invite:
			finish();
			break;
		case R.id.imgbackButton:
			finish();
			break;
		}
	
	}
	
	public void getDonationInvoiceToken(){
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("charityId", arrObjectIdCharity.get(mPositionClicked));
		params.put("inviteId", objectIdInvite);
		
		//progress_bar_profile.setVisibility(View.VISIBLE);
		//tv_warning_profile_activity.setVisibility(View.GONE);
		 ParseCloud.callFunctionInBackground("createCharityInvoice", params, new FunctionCallback<String>() {
			    public void done(String donationInvoiceToken , ParseException e) {
			       if (e == null) {
			    	   //Log.e("Donation invoice",donationInvoiceToken);
			    	   mDonationInvoiceToken = donationInvoiceToken;
			       }
			    
			    }
		 });
	}
	
	public static void addOnTextChangeListener(EditText editText){
		mEditText = editText;
		mEditText.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
								
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				if (s.length() == 0){
					mEditText.setText("$");
					mEditText.setSelection(mEditText.getText().length());
				}
				if (mEditText.getText().length()>0 && mEditText.getText().charAt(0) != '$'){
					mEditText.setText("$"+ s.subSequence(0, mEditText.getText().length()));
					mEditText.setSelection(mEditText.getText().length());
				}
				if (mEditText.getText().length()>2 && mEditText.getText().charAt(2) == '$'){
					mEditText.setText(mEditText.getText().subSequence(2, mEditText.length()));
				}
			}
		});
	}
	
	public void setViewPagerOnTouchListener(){
		pager.setOnTouchListener(new View.OnTouchListener() {
				int dragthreshold = 30;
		        int downX;
		        int downY;

		        @Override
		        public boolean onTouch(View v, MotionEvent event) {

		            switch (event.getAction()) {
		            case MotionEvent.ACTION_DOWN:
		                downX = (int) event.getRawX();
		                downY = (int) event.getRawY();
		                break;
		            case MotionEvent.ACTION_MOVE:
		                int distanceX = Math.abs((int) event.getRawX() - downX);
		                int distanceY = Math.abs((int) event.getRawY() - downY);

		                if (distanceY > distanceX && distanceY > dragthreshold) {
		                    pager.getParent().requestDisallowInterceptTouchEvent(false);
		                    scrollview_donate_from_invite.getParent().requestDisallowInterceptTouchEvent(true);
		                } else if (distanceX > distanceY && distanceX > dragthreshold) {
		                    pager.getParent().requestDisallowInterceptTouchEvent(true);
		                    scrollview_donate_from_invite.getParent().requestDisallowInterceptTouchEvent(false);
		                }
		                break;
		            case MotionEvent.ACTION_UP:
		            	scrollview_donate_from_invite.getParent().requestDisallowInterceptTouchEvent(false);
		                pager.getParent().requestDisallowInterceptTouchEvent(false);
		                break;
		            }
		            return false;
		        }
			});
	}
	//
	public void setUserPicture(String url){
	ImageLoader imageLoader = ImageLoader.getInstance();
	DisplayImageOptions options = new DisplayImageOptions.Builder()
	                                       .showImageOnLoading(R.drawable.prf_cell_icon_placeholder)
	                                       .showImageForEmptyUri(R.drawable.prf_cell_icon_placeholder)
	                                       .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
	                                       .cacheInMemory(true)
	                                       .cacheOnDisc(true)
	                                       .displayer(new FadeInBitmapDisplayer(300))
	                                       .build();
	
	value_width_height = (int) ProfileActivity.dipToPixels(DonateFromInviteActivity.this, 70);
	if (!url.equals("")){
		
    	Bitmap dnt_cellmask = BitmapFactory.decodeResource(getResources(),
                R.drawable.dnt_cellmask);
    	Bitmap dnt_cellmask_resized = Bitmap.createScaledBitmap(dnt_cellmask, value_width_height, value_width_height, true);
    	img_user_picture_donate_from_invite.setImageBitmap(dnt_cellmask_resized);
    	imageLoader.loadImage(url, options, new SimpleImageLoadingListener() {
		
	    @SuppressLint("NewApi")
		@SuppressWarnings("deprecation")
		@Override
	    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
	    	Bitmap bitmap = Support.optimizeBitmap(loadedImage);
	    	lay_user_picture_donate_from_invite.setVisibility(View.VISIBLE);
	    	resizedBitmap=Bitmap.createScaledBitmap(bitmap, value_width_height, value_width_height, true);
	    	Drawable drawable = new BitmapDrawable(getResources(),resizedBitmap);
	    	if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
	    		img_user_picture_donate_from_invite.setBackgroundDrawable(drawable);
		            } else {
		            	img_user_picture_donate_from_invite.setBackground(drawable);
		            }
	    }
	});
	}
	else{
		img_user_picture_donate_from_invite.setVisibility(View.GONE);
	}
	
	}
	
	public void getCharityList(int offset, int limit) {
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("offset", offset);
		params.put("limit", limit);
		
		//progress_bar_profile.setVisibility(View.VISIBLE);
		//tv_warning_profile_activity.setVisibility(View.GONE);
		 ParseCloud.callFunctionInBackground("getCharityList", params, new FunctionCallback<JSONArray>() {
			    public void done(JSONArray charity , ParseException e) {
			       if (e == null) {
			    	 
			    	   AppLog.e("Charity", charity.toString());
			    	   parseCharityList(charity);
			    	   imageUrls = new String[arrImageUrls.size()];
			    	   imageUrls = arrImageUrls.toArray(imageUrls);
			    	   pager.setAdapter(new ImagePagerAdapter(imageUrls));
			   			pager.setCurrentItem(0);
			   			
			        
			       }
			       else{
			    	   int err = e.getCode();
			    	   Log.d("Error code:", Integer.toString(err));
			    	   switch(err){
					    case ErrorCode.CONNECTION_FAILED:
					    	notifyDialogInternet = NotifyDialog.newInstance(getString(R.string.no_internet_title), getString(R.string.no_internet_text), NotifyDialog.ButtonsLayout.ONE_BUTTON);
					    	notifyDialogInternet.show(DonateFromInviteActivity.this);
					    	//tv_warning_profile_activity.setText("Problem with Internet. Please try again.");
					    	//tv_warning_profile_activity.setVisibility(View.VISIBLE);
           				break;
					    case ErrorCode.SCRIPT_ERROR:
					    	/*if (e.getMessage().equals("User are not authorized") ){
					    		startActivity(new Intent(ProfileActivity.this, LoginActivity.class));
					    	}*/
					    break;
					    default:
            	    		/*tv_warning_profile_activity.setText(e.getMessage());
            	    		tv_warning_profile_activity.setVisibility(View.VISIBLE);*/
	            	    	}
			    	  
			    	  // progress_bar_profile.setVisibility(View.GONE);
			    	   e.printStackTrace();
			       }
			   }
			});
	}
	
	@Override
    protected void onActivityResult (int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
            if (confirm != null) {
              //  try {
            		donationConfirmed=false;
                    Log.i("paymentExample", confirm.toJSONObject().toString());
                    parsePayPalFeedBack(confirm.toJSONObject());
                    // TODO: send 'confirm' to your server for verification.
                    // see https://developer.paypal.com/webapps/developer/docs/integration/mobile/verify-mobile-payment/
                    // for more details.

                    /* } catch (JSONException e) {
                    Log.e("paymentExample", "an extremely unlikely failure occurred: ", e);
                }*/
            }
        }
        else if (resultCode == Activity.RESULT_CANCELED) {
            Log.i("paymentExample", "The user canceled.");
        }
       /* else if (resultCode == PaymentActivity.RESULT_PAYMENT_INVALID) {
            Log.i("paymentExample", "An invalid payment was submitted. Please see the docs.");
        }*/
    }
	
	@Override
    public void onDestroy() {
        stopService(new Intent(this, PayPalService.class));
        super.onDestroy();
    }
	
	public void parseCharityList(JSONArray jsonArray){
		try{
			if (jsonArray.length()>19){
				number_charity_list=number_charity_list+20;
				getCharityList(number_charity_list,number_charity_list+20);
			}
			for(int i = 0; i < jsonArray.length(); i++){
				
		    	JSONObject jsonObject = jsonArray.getJSONObject(i);
		    	Log.d("Json Object " + Integer.toString(i), jsonObject.toString());
		    	charityDescription = jsonObject.getString("charityDescription");
		    	title = jsonObject.getString("title");
		    	email = jsonObject.getString("email");
		    	photoThumbnail = jsonObject.getString("photoThumbnail");
		    	photoFull = jsonObject.getString("photoFull");
		    	objectIdCharity= jsonObject.getString("objectId");
		    			
		    	arrImageUrls.add(photoThumbnail);
		    	arrCharityDescription.add(charityDescription);
		    	arrTitle.add(title);
		    	arrPhotoFull.add(photoFull);
		    	arrObjectIdCharity.add(objectIdCharity);
			
		       }
		    
		    
		} catch (JSONException e) {
		    e.printStackTrace();
		}
	}
	
	
	
	public void parsePayPalFeedBack(JSONObject jsonObj){
		try{
			
		    	JSONObject payment = jsonObj.getJSONObject(RESPONSE);
		    	String state = payment.getString(STATE);
		    	
		    	if (!state.equals("approved")){
		    		Toast.makeText(DonateFromInviteActivity.this, "State: not approved", Toast.LENGTH_LONG).show();
		    		return;
		    	}
		    	/*short_description = payment.getString(SHORT_DESCRIPTION);
		    	amount = payment.getInt(AMOUNT);
		    	
		    	
		    	
		    	JSONObject proof_of_payment = jsonObj.getJSONObject(PROOF_OF_PAYMENT);
		    	JSONObject adaptive_payment = proof_of_payment.getJSONObject(ADAPTIVE_PAYMENT);
		    	String payment_exec_status = adaptive_payment.getString(PAYMENT_EXEC_STATUS);*/
		    	pay_key = payment.getString(ID);
		    	Log.d("Pay", arrObjectIdCharity.get(mPositionClicked)+ " "+ objectIdInvite+ " " + short_description+" "+ amount+ " "+  pay_key);
		    	confirmDonation(arrObjectIdCharity.get(mPositionClicked), objectIdInvite, Integer.toString(amount), pay_key,short_description);
		    	paypalConfirmation(Integer.toString(amount), pay_key);
		    	notifyDialogDonate = NotifyDialogDonate.newInstance("", "Your gift has been sent to the charity successfully",NotifyDialogDonate.ButtonsLayout.ONE_BUTTON);
		    	notifyDialogDonate.setListener(new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if (which == Dialog.BUTTON_POSITIVE) {
							if (donationConfirmed == true){
								notifyDialogDonate.dismiss();
								
								 Intent intent = new Intent(DonateFromInviteActivity.this, ShareActivity.class);
							        intent.putExtra("AMOUNT_PAID", Integer.toString(amount));
							        intent.putExtra("DESCRIPTION", short_description);
							        startActivity(intent);
							}
							else{
							confirmDonation(arrObjectIdCharity.get(mPositionClicked), objectIdInvite, Integer.toString(amount), pay_key, short_description);
							}
							//startActivity(new Intent(DonateFromInviteActivity.this, ShareActivity.class));
						}
						
					}
				});
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.add(notifyDialogDonate, null);
                ft.commitAllowingStateLoss();
		    	
		    
		} catch (JSONException e) {
		    e.printStackTrace();
		}
	}
	
	public void confirmDonation(String charityId, String inviteId, String amountPaid, String transactionId, final String description) {
		final String mInviteId = inviteId;
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("charityId", charityId);
		params.put("inviteId", inviteId);
		params.put("amountPaid", amountPaid);
		params.put("transactionId", transactionId);
		 ParseCloud.callFunctionInBackground("confirmDonation", params, new FunctionCallback<String>() {
			    public void done(String status, ParseException e) {
			       if (e == null) {
			    	   Map<String, String> charityTitleParams = new HashMap<String, String>();
			    	   charityTitleParams.put("Pay for", description);
			    	   FlurryAgent.logEvent("User Paid ", charityTitleParams);
			    	donationConfirmed = true;
			        AppLog.e("Status", status.toString());
			        AppLog.e("InviteId", mInviteId);
			        
			        //notifyDialogDonate.dismiss();
			       
			       }
			       else{
			    	   notifyDialogDonate.setWaningText(e.getMessage());
			    	   e.printStackTrace();
			       }
			   }
			});
		 
	}
	
	
	public void paypalConfirmation(final String amount, final String transactionId){
		if (mDonationInvoiceToken != null){
			 myAsyncTask = new MyAsyncTask();
		   		myAsyncTask.execute(amount, transactionId);
		}
		else {
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put("charityId", arrObjectIdCharity.get(mPositionClicked));
			params.put("inviteId", objectIdInvite);
			
			//progress_bar_profile.setVisibility(View.VISIBLE);
			//tv_warning_profile_activity.setVisibility(View.GONE);
			 ParseCloud.callFunctionInBackground("createCharityInvoice", params, new FunctionCallback<String>() {
				    public void done(String donationInvoiceToken , ParseException e) {
				       if (e == null) {
				    	   //Log.e("Donation invoice",donationInvoiceToken);
				    	   mDonationInvoiceToken = donationInvoiceToken;
				    	   myAsyncTask = new MyAsyncTask();
				   		myAsyncTask.execute(amount, transactionId);
				       }
				    
				    }
			 });
		}
	}
	
	public String postData(String amount, String transactionId) {
	    // Create a new HttpClient and Post Header
		
	    HttpClient httpclient = new DefaultHttpClient();
	    HttpPost httppost = new HttpPost("http://adminplanvy.parseapp.com/makeDonation");

	    try {
	        // Add your data
	        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(4);
	        nameValuePairs.add(new BasicNameValuePair("payment_status", "Completed"));
	        nameValuePairs.add(new BasicNameValuePair("custom", mDonationInvoiceToken));
	        nameValuePairs.add(new BasicNameValuePair("mc_gross", amount));
	        nameValuePairs.add(new BasicNameValuePair("txn_id", transactionId));
	        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

	        // Execute HTTP Post Request
	        HttpResponse response = httpclient.execute(httppost);
	        HttpEntity entity = response.getEntity();
	            String responseBody = EntityUtils.toString(entity);
	            //Log.e("Response", responseBody);
	        
	        
	        return responseBody;

	    } catch (ClientProtocolException e) {
	        e.printStackTrace();
	    } catch (IOException e) {
	    	 e.printStackTrace();
	    }
		return "";
	    
	}
	
	private class MyAsyncTask extends AsyncTask<String, Integer, String>{
		  @Override
		  protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
		        
		        return postData(params[0], params[1]);
		  }
		 
		  protected void onPostExecute(String result){
			  if (result !=null && result.equals("")){
				  Log.e("Async Task", "Something going wrong");
			  }
			 /* else{
				 // Log.e("Async Task", result);
			  }*/
		
		  } 
		  
		 
		}
			
	private class ImagePagerAdapter extends PagerAdapter {

		private String[] images;
		private LayoutInflater inflater;
		

		ImagePagerAdapter(String[] images ) {
			this.images = images;
			inflater = getLayoutInflater();
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			container.removeView((View) object);
		}

		@Override
		public int getCount() {
			return images.length;
		}
		
		 private class ViewHolder {
				
			 	
				ImageView img_charity;
				LinearLayout lay_view_pager_item;
				ProgressBar spinner;
				TextView tv_title_donate_from_invite;
				
			}
		 
		 

		@Override
		public Object instantiateItem(ViewGroup view, final int position) {
			final ViewHolder holder;
			 
			//if (view==null){
			imageLayout = inflater.inflate(R.layout.item_pager_image, view, false);
			//assert imageLayout != null;
			holder = new ViewHolder();
			holder.img_charity = (ImageView) imageLayout.findViewById(R.id.image_item_view_pager);
			holder.img_charity.setTag("img"+position);
			Log.d("Tag init","img"+position);
			holder.lay_view_pager_item = (LinearLayout) imageLayout.findViewById(R.id.lay_view_pager_item);
			holder.spinner = (ProgressBar) imageLayout.findViewById(R.id.loading);
			holder.tv_title_donate_from_invite = (TextView) imageLayout.findViewById(R.id.tv_title_donate_from_invite);
			//imageLayout.setTag(holder);
			/*}
			else {
				holder = (ViewHolder) imageLayout.getTag();
			}*/
			String title = arrTitle.get(position);
			holder.tv_title_donate_from_invite.setText(title);
			
			holder.tv_title_donate_from_invite.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					DonateFromInviteDialog donateFromInviteDialog = DonateFromInviteDialog.newInstance(arrTitle.get(position), arrCharityDescription.get(position), arrPhotoFull.get(position));
					donateFromInviteDialog.show(DonateFromInviteActivity.this);
				}
			});
			
			imageLoader.loadImage(images[position],  options, new SimpleImageLoadingListener(){
				@Override
				public void onLoadingStarted(String imageUri, View view) {
					holder.spinner.setVisibility(View.VISIBLE);
				}

				@Override
				public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
					String message = null;
					switch (failReason.getType()) {
						case IO_ERROR:
							message = "Input/Output error";
							break;
						case DECODING_ERROR:
							message = "Image can't be decoded";
							break;
						case NETWORK_DENIED:
							message = "Downloads are denied";
							break;
						case OUT_OF_MEMORY:
							message = "Out Of Memory error";
							break;
						case UNKNOWN:
							message = "Unknown error";
							break;
					}
					Toast.makeText(DonateFromInviteActivity.this, message, Toast.LENGTH_SHORT).show();

					holder.spinner.setVisibility(View.GONE);
				}
				
				@SuppressWarnings("deprecation")
				@SuppressLint("NewApi")
				@Override
				public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
					
		        	int value_height_width = (int) ProfileActivity.dipToPixels(DonateFromInviteActivity.this, 100);
		        	//int value_height_weight_lay = (int) ProfileActivity.dipToPixels(DonateFromInviteActivity.this, 170);
		        	if (loadedImage.getWidth() >= loadedImage.getHeight()){

		        		thumbnail = Bitmap.createBitmap(
		        				loadedImage, 
		        				loadedImage.getWidth()/2 - loadedImage.getHeight()/2,
		        		     0,
		        		     loadedImage.getHeight(), 
		        		     loadedImage.getHeight()
		        		     );

		        		}else{

		        			thumbnail = Bitmap.createBitmap(
		        					loadedImage,
		        		     0, 
		        		     loadedImage.getHeight()/2 - loadedImage.getWidth()/2,
		        		     loadedImage.getWidth(),
		        		     loadedImage.getWidth() 
		        		     );
		        		}
		        	resizedThumbnail=Bitmap.createScaledBitmap(thumbnail, value_height_width, value_height_width, true);
		        	Bitmap dnt_cellmask = BitmapFactory.decodeResource(getResources(),
	                        R.drawable.dnt_cellmask);
	            	dnt_cellmask_resized = Bitmap.createScaledBitmap(dnt_cellmask, value_height_width, value_height_width, true);
	            	
	            	 Bitmap donate_cellmask_red = BitmapFactory.decodeResource(getResources(),
	                         R.drawable.nvt_photo_mask_red);
	             	donate_cellmask_red_resized = Bitmap.createScaledBitmap(donate_cellmask_red, value_height_width, value_height_width, true);
	            	//Drawable drawable_cellmask = new BitmapDrawable(getResources(),dnt_cellmask_resized);
	            	holder.img_charity.setImageBitmap(dnt_cellmask_resized);
		        	 Drawable drawable = new BitmapDrawable(getResources(),resizedThumbnail);
		        	 LayoutParams linLayoutParam = new LayoutParams(value_height_width, value_height_width); 
		        	 holder.lay_view_pager_item.setLayoutParams(linLayoutParam);
		        	 
		             
		             if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
		            	 holder.lay_view_pager_item.setBackgroundDrawable(drawable);
		            	
		             } else {
		            	 holder.lay_view_pager_item.setBackground(drawable);
		             }
		            
		             holder.lay_view_pager_item.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
						
							isCharityChoosen = true;
							if (pager.getCurrentItem() == 0){
								currentItem= 0;
							}
							else{
								currentItem= pager.getCurrentItem()+1;
							}
							
							if (mPositionClicked == position){
								DonateFromInviteDialog donateFromInviteDialog = DonateFromInviteDialog.newInstance(arrTitle.get(position), arrCharityDescription.get(position), arrPhotoFull.get(position));
								donateFromInviteDialog.show(DonateFromInviteActivity.this);
								
							}
							mPositionClicked = position;
							
							if (position-1<0){
								pager.setCurrentItem(0);
								String current = pager.getCurrentItem()+ "";
								Log.d("CurrentItem", current);
							}
							else{
							pager.setCurrentItem(position-1);
							Log.d("CurrentItem",Integer.toString(position-1));}
							//mPositionClicked = pager.getCurrentItem()+1;
							//Log.d("mPositionClicked",Integer.toString(mPositionClicked));
							//Log.d("mPosition-1", Integer.toString(mPosition-1));
							/*int length = getCount();
							Log.d("Length",Integer.toString(length));*/
							
								/*if (i==mPositionClicked){
									continue;
								}*/	
							for (int i=0;i<getCount();i++){
								String pos = Integer.toString(i);
								String tag= "img"+pos;
								Log.d("Tag",tag);
								ImageView img=(ImageView) pager.findViewWithTag(tag);
								//ImageView view1 = (ImageView) pager.getChildAt(0).findViewById(R.id.image_item_view_pager);
								//view1.setImageBitmap(dnt_cellmask_resized);\
								try{
								img.setImageBitmap(dnt_cellmask_resized);
								}
								catch (NullPointerException e){
									Log.e("Null Pointer exception", "true");
									
								}
								//pager.setAdapter(new ImagePagerAdapter(imageUrls));
								
							}
							holder.img_charity.setImageBitmap(donate_cellmask_red_resized);
							
						}
					});
		             if (position == 0){
			        		holder.lay_view_pager_item.performClick();
			        	}
		        	holder.spinner.setVisibility(View.GONE);
		        	
				}
		        	
				
			});
			
			
			view.addView(imageLayout,0);
			return imageLayout;
		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return view.equals(object);
		}

		@Override
		public void restoreState(Parcelable state, ClassLoader loader) {
		}

		@Override
		public Parcelable saveState() {
			return null;
		}
		@Override
		public float getPageWidth(int position) 
		{ return((float)1/3); } 
	}
	
	
}
