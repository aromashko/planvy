package ua.regionit.planvy.ui;

import java.util.ArrayList;
import org.holoeverywhere.widget.GridView;
import ua.regionit.planvy.R;
import ua.regionit.planvy.ui.ContactAdapter.ViewHolder;
import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

	public class GridViewAdapter extends BaseAdapter {
			    
		ArrayList<String> list = new ArrayList<String>();
		ArrayList<ContactModel> arraySelected, helpArray;
	    Context mContext;
	    LayoutInflater mInf;
	    GridViewAdapter adapter = this;
	    GridView grid;
	    Button sendButton;
	    
	    public GridViewAdapter(Context context, ArrayList<ContactModel> mContacts) {
	    	this.mContext = context;
			this.arraySelected = mContacts;
			this.helpArray = mContacts;
			mInf = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	    }
	    
	    static class ViewHolder{
			  TextView tvName;
			  Button remove;
			  RelativeLayout llTag;
			 }
	    
	    
	    @Override
	    public View getView(final int position, View convertView, ViewGroup parent) {

	    
	    	final ViewHolder vh1;
			  if(convertView != null){
			   vh1 = (ViewHolder)convertView.getTag();
			  }else{
			   convertView = mInf.inflate(R.layout.people_item, parent,false);
			   
			   vh1 = new ViewHolder();
			   vh1.tvName = (TextView)convertView.findViewById(R.id.text_people);
			   vh1.remove = (Button)convertView.findViewById(R.id.btn_delete_people);
			   vh1.llTag = (RelativeLayout)convertView.findViewById(R.id.grid_tag);
			   
			   convertView.setTag(vh1);
			  }
			 vh1.llTag.setTag(position);
			 vh1.tvName.setText(arraySelected.get(position).getName());
			 vh1.remove.setOnClickListener(new OnClickListener() {

	            @SuppressLint("NewApi")
				@Override
	            public void onClick(View v) {

	            	ContactModel p = arraySelected.get(position);
	                arraySelected.get(position).setSelected(false);
	                arraySelected.remove(position);
	                adapter.notifyDataSetChanged();
	                if(arraySelected.isEmpty()){
	                	sendButton.setEnabled(false);
	                }
	            }
	        });
			 
			  return convertView;
		 }
	    
	    void setSendButton(Button sendButton){
	    	this.sendButton = sendButton;
	    }
	    
	    @Override
		public int getCount() {
			// TODO Auto-generated method stub
			return arraySelected.size();
		}


		@Override
		public ContactModel getItem(int position) {
			// TODO Auto-generated method stub
			return arraySelected.get(position);
		}


		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}
}
