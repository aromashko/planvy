package ua.regionit.planvy.ui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.holoeverywhere.app.Activity;
import org.holoeverywhere.widget.Button;
import org.holoeverywhere.widget.ListView;

import com.flurry.android.FlurryAgent;
import com.parse.ParseException;

import ua.regionit.planvy.App;
import ua.regionit.planvy.AppLog;
import ua.regionit.planvy.R;
import ua.regionit.planvy.ui.dialog.ChangePhoneDialog;
import ua.regionit.planvy.ui.dialog.NotifyDialog;
import ua.regionit.planvy.ui.views.PinnedSectionListView;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseUser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AddPeopleActivity extends Activity implements OnClickListener {
	

	Button send_invite, btn_cancel;
	PinnedSectionListView lv_contacts;
	ProgressBar loader;
	GetContacts gc;
	ContactAdapderMy adapter;
	EditText search;
	ArrayList<String> name, phone, email, registred_emails, registred_nickname, selected_email, all_phone_list;
	ContactModel cm, cmFriendPlanvy;
	ParseUser currentUser;
	ImageView back_btn;
	ArrayList<ContactModel> filtred_contacts;
	ArrayList<ContactModel> AllModels = new ArrayList<ContactModel>();
	ArrayList<ContactModel> arrayFriendsPlanvy = new ArrayList<ContactModel>();
	ArrayList<ContactModel> helpFriendsPlanvy = new ArrayList<ContactModel>();
	ArrayList<ContactModel> arrayChecked = new ArrayList<ContactModel>();
	Context context;
	static Boolean goFromProfile=false;
	Map<String, String> hashmap_email = new HashMap<String, String>();
	Map<String, String> hashmap_number = new HashMap<String, String>();
	HashMap<String, ContactModel> helper = new HashMap<String, ContactModel>();
	
	ArrayList<Object> mObj = new ArrayList<Object>();
	 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.add_people);
		FlurryAgent.logEvent("AddPeople", true);
		context = this;
		initUi();
		initListener();
		
		currentUser = ParseUser.getCurrentUser();
		if(currentUser==null){
			btn_cancel.setText("Back to Main");
		}else{
			btn_cancel.setText("Back to Profile");
		}
		loader.setVisibility(View.VISIBLE);
		lv_contacts.setVisibility(View.GONE);
		name = new ArrayList<String>();
		phone = new ArrayList<String>();
		email = new ArrayList<String>();
		all_phone_list = new ArrayList<String>();
		selected_email = new ArrayList<String>();
		gc = new GetContacts();
		gc.execute();
			/*if(LoginActivity.skip==true){
			friends_on_planvy.setVisibility(View.GONE); 
			
		}*/
		search.setVisibility(View.GONE);
	    
		/**
	     * Enabling Search Filter
	     * */
		
		// Capture Text in EditText
        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Filter filter;
                if(adapter == null || s == null || (filter = adapter.getFilter()) == null){
                    Log.d(AddPeopleActivity.class.getSimpleName(), "Something goes really wrong with your views initialization");
                    return;
                }
                filter.filter(s);
            }
        });   
    
		lv_contacts.setOnItemClickListener(new OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View v, int position, long id)
            {
            if(adapter.getItem(position).getName()!="Friends on Planvy" &&
               adapter.getItem(position).getName()!="Invite Friends from Contacts"){
                CheckBox cb = (CheckBox) v.findViewById(R.id.cb_item);
                ContactModel p = (ContactModel) adapter.getItem(position);
                if (!cb.isChecked()) {
                    p.setSelected(true);
                    cb.setChecked(!cb.isChecked());
                } else {
                    p.setSelected(false);
                    cb.setChecked(!cb.isChecked());
                }
                
            }
            }
        });
		
	}
	
	@Override
	protected void onStart()
	{
		super.onStart();
		FlurryAgent.onStartSession(this, "XWHZX63GBXQKSR9KJGQ9");
	}
	
	@Override
	protected void onStop()
	{
		super.onStop();		
		FlurryAgent.onEndSession(this);
	}
		
	public void initUi(){
		send_invite = (Button)findViewById(R.id.btn_send_inspire_invite);
		btn_cancel = (Button)findViewById(R.id.btn_cancel);
		loader = (ProgressBar)findViewById(R.id.add_people_progress_bar);
		lv_contacts = (PinnedSectionListView)findViewById(R.id.lv_invite_contacts);
		search = (EditText)findViewById(R.id.et_search);
		back_btn = (ImageView)findViewById(R.id.img_back_button_add_people);
	}
	public void initListener(){
		send_invite.setOnClickListener(this);
		btn_cancel.setOnClickListener(this);
		back_btn.setOnClickListener(this);
	}
	
	public void readContacts(){
        ContentResolver cr = getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,null, null, null, null);

        Map<String, ContactModel> map = new HashMap<String, ContactModel>();
            while (cur.moveToNext()) {
                String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                String name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                ContactModel cm = new ContactModel(name);
                //     if (Integer.parseInt(cur.getString(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                cm.setName(name);
                cm.setSelected(false);

                System.out.println("name : " + name + ", ID : " + id);

                // get the phone number
                map.put(id, cm);

                AllModels.add(cm);

                // }
            }

            Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,
                    null, //ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = ?",
                    //new String[]{id}
                    null, null);
            while (pCur.moveToNext()) {
                String phone = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                String id = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));
                ContactModel cm = map.get(id);
                cm.setPhone(phone);
                all_phone_list.add(phone);
                System.out.println("phone" + phone);

            }
            pCur.close();


            // get email and type


            Cursor emailCur = cr.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                    null,
                    null, //ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
                    //new String[]{id}
                    null, null);
            while (emailCur.moveToNext()) {
                // This would allow you get several email addresses
                // if the email addresses were stored in an array
                String email = emailCur.getString(
                        emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                String emailType = emailCur.getString(emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.TYPE));

                int type = emailCur.getInt(emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.TYPE));
                String customLabel = emailCur.getString(emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.LABEL));
                CharSequence CustomemailType = ContactsContract.CommonDataKinds.Email.getTypeLabel(this.getResources(), type, customLabel);
                String id = emailCur.getString(emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.CONTACT_ID));
                ContactModel cm = map.get(id);
                cm.setEmail(email);
                System.out.println("E-mail" + email);
                System.out.println("Email " + email + " Email Type : " + emailType);

            }
            emailCur.close();

    }
	
	@Override
	public void onClick(View v) {
		
		switch (v.getId()){
		case R.id.btn_send_inspire_invite:
		/*	if (currentUser!=null && currentUser.getString("phone") == null){
				Log.d("phone","null");
				startChangePhoneDialog();
			}
			else if (currentUser!=null && currentUser.getString("phone").equals("")){
				Log.d("phone","not null");
				startChangePhoneDialog();
				}
			else{*/

				sendInspireInvite();
		//}


		break;
			
		case R.id.btn_cancel:
			if(currentUser==null){
				startActivity(new Intent(AddPeopleActivity.this, InspireActivity.class));
			}else{
				startActivity(new Intent(AddPeopleActivity.this, ProfileActivity.class));
			}
			
			
		break;
		case R.id.img_back_button_add_people :
			if (goFromProfile){
				goFromProfile=false;
				startActivity(new Intent(AddPeopleActivity.this, ProfileActivity.class));
			}
			else{
			startActivity(new Intent(AddPeopleActivity.this, InspireActivity.class));
			}
		break;
		}
	}
	
	public void sendInspireInvite(){
		int count=0;
		for(int i=0;i<arrayFriendsPlanvy.size();i++){
			if(arrayFriendsPlanvy.get(i).isSelected()){
			count++;
			}
			}
			for(int j=0;j<AllModels.size();j++){
			if(AllModels.get(j).isSelected()){
			count++;
			}
			}
			Log.d("CHECKED ===>", Integer.toString(count));
			if(count==0){
				/*NotifyDialog notifyDialog = NotifyDialog.newInstance("", "Please select at least one contact for invite", NotifyDialog.ButtonsLayout.ONE_BUTTON);
				notifyDialog.show(AddPeopleActivity.this);*/
				FragmentManager fm = getSupportFragmentManager();
		    	FragmentTransaction ft = fm.beginTransaction();
		    	NotifyDialog notifyDialog = NotifyDialog.newInstance("", "Please select at least one contact for invite", NotifyDialog.ButtonsLayout.ONE_BUTTON);
		    	notifyDialog.show(ft, "dlg1", true);
			}else{
			((App)getApplicationContext()).setArrayModel(AllModels);
			//sendInspireInvite();
			ArrayList<ContactModel> lll = new ArrayList<ContactModel>();
			for(int i=0;i<adapter.getCount();i++){
			//if(adapter.getItem(i).isSelected()){
				lll.add(adapter.getItem(i));
			//}
			}
			
			((App)getApplicationContext()).setArrayModel(lll);
			Intent intent = new Intent(AddPeopleActivity.this,InviteInfoActivity.class);
			startActivity(intent);
		}
		
	}
	
	public void startChangePhoneDialog(){
		ChangePhoneDialog changePhoneDialog = ChangePhoneDialog.newInstance("Please provide phone number\nin order to\nsend and receive invitations", ChangePhoneDialog.ButtonsLayout.TWO_BUTTONS);
		changePhoneDialog.setListener(new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				 if (which == Dialog.BUTTON_POSITIVE) {
					 sendInspireInvite();
				 }
				 				
			}
			
		});
		changePhoneDialog.show(AddPeopleActivity.this);
	}
	
	public ArrayList<String> getFriendsOnPlanvy(ArrayList<String> arrOfEmail , ArrayList<String> arrOfPhone){
		//progress_layoutAddPeopleFriend.setVisibility(View.VISIBLE);
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("emails", arrOfEmail);
		params.put("phones", arrOfPhone);
		
		ParseCloud.callFunctionInBackground("getFriendList", params, new FunctionCallback<JSONArray>() {
		    public void done(JSONArray users, ParseException e) {
		       if (e == null) {
		          Log.d("Add People",users.toString());
		          parseUserEmail(users);
		          for(int i=0;i<arrayFriendsPlanvy.size();i++){
			        	 Log.d("ITEM ===>", arrayFriendsPlanvy.get(i).getName());
			        	 arrayFriendsPlanvy.get(i).setPlanvy_friend(true);
			      }
		          ((App)getApplicationContext()).setArrayModel(AllModels);
		          
		          // totototot
		          loader.setVisibility(View.GONE);
				  lv_contacts.setVisibility(View.VISIBLE);
				  search.setVisibility(View.VISIBLE);
		          setUpAdaptor();
		       }else{
		    	  loader.setVisibility(View.GONE);
			  	  lv_contacts.setVisibility(View.VISIBLE);
		    	  e.printStackTrace();
		    	  AppLog.e("Something bag", "script probably");

		       }
		   }
		});
		return registred_emails;
	}
	
	public void parseUserEmail(JSONArray jsonArray){
	registred_emails = new ArrayList<String>();
	registred_nickname = new ArrayList<String>();
	//mAdapterPlanvyFriends.
		try{
			
			for(int i = 0; i < jsonArray.length(); i++){
		    	JSONObject jsonObject = jsonArray.getJSONObject(i);
		       
		        String email = jsonObject.getString("email");
		        String nickname = jsonObject.getString("nickname");
		        String number = jsonObject.getString("phone");
		        hashmap_email.put(email, nickname);
		        hashmap_number.put(number, nickname);
		        registred_emails.add(email);
		        registred_nickname.add(nickname);
		        cmFriendPlanvy = new ContactModel(nickname,email,true,number);
		        helper.put(email, cmFriendPlanvy);
		        
		        
		        
		        Log.d("Email",email);
		        Log.d("Nickname",nickname);
		    }
			
			arrayFriendsPlanvy.addAll(helper.values());
			
			
		} catch (JSONException e) {
		    e.printStackTrace();
		}
		
	}
	
	class GetContacts extends AsyncTask<Void, Void, Void> {

	    @Override
	    protected void onPreExecute() {
	    super.onPreExecute();
	    }

	    @Override
	    protected Void doInBackground(Void... params) {
	    	readContacts();
	    return null;
	    }

	    @Override
	    protected void onPostExecute(Void result) {
	      super.onPostExecute(result);
		
		  /*email.add("thekwand@mail.ru");
		  email.add("prewior@gmail.com");
		  email.add("y.melnyk@regionit.com.ua");
		  email.add("a.romashko@regionit.com.ua");
		  email.add("andrewlotar2@mail.ru");
		  email.add("andrewlotar3@mail.ru");
		  email.add("andrewlotar4@mail.ru");
		  email.add("andrewlotar9@mail.ru");
		  email.add("pronin.alx@gmail.com");
		  email.add("a.beylik@regionit.com.ua");
		  email.add("skadyrbekov@yahoo.com");
		  email.add("beilik.andry@mail.ru");
		  email.add("andrewlotar10@mail.ru");
		  email.add("andrewlotar11@mail.ru");*/
		  
	 
		  for(int i =0;i<AllModels.size();i++){
			  if(AllModels.get(i).getEmail()!=null){
				  email.add(AllModels.get(i).getEmail());
			  }
			  if(AllModels.get(i).getPhone()!=null){
				  all_phone_list.add(AllModels.get(i).getPhone());
			  }
		  }
		  getFriendsOnPlanvy(email, all_phone_list);
	      Log.d("Async ===>", "DONE!");
	    }
	}
	

	public ArrayList<ContactModel> getListOfObjects(ArrayList<ContactModel> pFriends, ArrayList<ContactModel> cList){
		ArrayList<ContactModel> mContactModel = new ArrayList<ContactModel>();
		if(!arrayFriendsPlanvy.isEmpty()){
		mContactModel.add(new ContactModel("Friends on Planvy"));
		}
		for(int i=0;i<pFriends.size();i++){
			mContactModel.add(pFriends.get(i));
		}
		mContactModel.add(new ContactModel("Invite Friends from Contacts"));
		for(int i=0;i<cList.size();i++){
			mContactModel.add(cList.get(i));
		}
		return mContactModel;
	}
	public void setUpAdaptor(){
		adapter = new ContactAdapderMy(context, getListOfObjects(arrayFriendsPlanvy, ((App)getApplicationContext()).getArrayModel()),hashmap_email, hashmap_number);
        lv_contacts.setAdapter(adapter);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		if(adapter != null){
		adapter.notifyDataSetChanged();
		}
	}
}
