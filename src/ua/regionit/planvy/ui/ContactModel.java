package ua.regionit.planvy.ui;

public class ContactModel {
	
	    

		private String id, name, phone, email;
	    private boolean selected, planvy_friend;

	    public ContactModel(String name) {
	        this.name = name;
	        this.selected = false;
	        planvy_friend = false;
	    }
	    
	    public ContactModel(String id, String name) {
	        this.name = name;
	        this.selected = false;
	        planvy_friend = false;
	    }
	    
	    public ContactModel(String name, String email, boolean planvy_friend, String phone) {
	        this.name = name;
	        this.phone = phone;
	        this.email= email;
	        this.planvy_friend = false;
	    }

	    public String getName() {
	        return name;
	    }

	    public void setName(String name) {
	        this.name = name;
	    }
	    
	    public String getPhone() {
	        return phone;
	    }

	    public void setPhone(String phone) {
	        this.phone = phone;
	    }

	    public String getEmail() {
	        return email;
	    }

	    public void setEmail(String email) {
	        this.email = email;
	    }
	    
	    public boolean isSelected() {
	        return selected;
	    }

	    public void setSelected(boolean selected) {
	        this.selected = selected;
	    }
	    
	    public boolean isPlanvy_friend() {
			return planvy_friend;
		}

		public void setPlanvy_friend(boolean planvy_friend) {
			this.planvy_friend = planvy_friend;
		}

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}
}
