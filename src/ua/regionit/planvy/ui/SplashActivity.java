package ua.regionit.planvy.ui;


import ua.regionit.planvy.R;
import ua.regionit.planvy.util.NetworkUtil;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.widget.Toast;

import com.parse.ParseAnalytics;
import com.parse.ParseFacebookUtils;
import com.parse.ParseUser;

public class SplashActivity extends Activity {
	private static int SPLASH_TIME_OUT = 2000;
	private static Boolean INTERNET_STATUS;
	ParseUser parseUser;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
		
		parseUser = ParseUser.getCurrentUser();
		ParseAnalytics.trackAppOpened(getIntent());
		new Handler().postDelayed(new Runnable() {
			 
          
 
            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
            	INTERNET_STATUS = NetworkUtil.isNetworkConnected(SplashActivity.this);
                if (INTERNET_STATUS){
                	//ParseFacebookUtils.initialize(getString(R.string.app_id));
                	if (parseUser != null){
                		Intent intent = new Intent (SplashActivity.this, ProfileActivity.class);
                		/*ParseAnonymousUtils.logIn(new LogInCallback() {
							
							@Override
							public void done(ParseUser user, ParseException e) {
								if (e==null){
									
								}
								else{
									e.printStackTrace();
								}
								
							}
						});*/
                		
                		startActivity(intent);
                		Log.d("CurrentUser","Logged");
                	}
                	else if (parseUser == null){
                		Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                		Log.d("CurrentUser","Not Logged");
                startActivity(i);}
                }
                else {
                	/*NotifyDialog dialogFragment = NotifyDialog.newInstance("No Internet",
                            "Please connect to Internet", NotifyDialog.ButtonsLayout.ONE_BUTTON);
                	dialogFragment.show();*/
                	Toast.makeText(SplashActivity.this, "No Internet", Toast.LENGTH_LONG).show();
                	finish();
                }
 
                // close this activity
                //finish();
            }
        }, SPLASH_TIME_OUT);
    
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.splash, menu);
		return true;
	}

}
