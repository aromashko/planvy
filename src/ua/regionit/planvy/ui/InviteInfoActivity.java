package ua.regionit.planvy.ui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.holoeverywhere.app.Activity;
import org.holoeverywhere.widget.Button;
import org.holoeverywhere.widget.GridView;

import ua.regionit.planvy.App;
import ua.regionit.planvy.ErrorCode;
import ua.regionit.planvy.R;
import ua.regionit.planvy.ui.dialog.ChangePhoneDialog;
import ua.regionit.planvy.ui.dialog.NotifyDialog;
import ua.regionit.planvy.util.Support;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParsePush;
import com.parse.ParseUser;

public class InviteInfoActivity extends Activity implements OnClickListener{
	

	GridView grid;
	EditText message;
	Button cancel,send;
	//ArrayAdapter<String> adapter;
	GridViewAdapter adapter;
	ArrayList<String> name, phone, email, registred_emails, mEmail, mPhone, mName;
	ContactModel cm;
	NotifyDialog dialogFragment, progressDialog, emptyDialog;
	ParseUser currentUser;
	int lineCount;
	ArrayList<ContactModel> mSelected;
	
	App mApp = new App();

	TextView tv;
	Typeface helveticaNeue;
	ImageView img_back;
	NotifyDialog notifyDialogInternet;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.invite);
		FlurryAgent.logEvent("Invite", true);
		helveticaNeue = Typeface.createFromAsset(getAssets(), "HelveticaNeue.ttf");
		tv= (TextView)findViewById(R.id.tv_send_invitation);
		tv.setTypeface(helveticaNeue);
		initUi();
		initListener();
		if(LoginActivity.skip_this_step==true){
			cancel.setText("Back to Main");
		}else{
			cancel.setText("Back to Profile");
		}
		currentUser =ParseUser.getCurrentUser();
		
		mSelected = ((App)getApplicationContext()).getArrayModel();
			
		adapter = new GridViewAdapter(this,getCheckedList(mSelected));
	    resize();
	    grid.setAdapter(adapter);

	    adapter.setSendButton(send);
	}	
	
	@Override
	protected void onStart()
	{
		super.onStart();
		FlurryAgent.onStartSession(this, "XWHZX63GBXQKSR9KJGQ9");
	}
	
	@Override
	protected void onStop()
	{
		super.onStop();		
		FlurryAgent.onEndSession(this);
	}
	
	
	public void initUi(){
		grid = (GridView)findViewById(R.id.grid);
		message = (EditText)findViewById(R.id.et_message);
		send = (Button)findViewById(R.id.btn_send);
		cancel = (Button)findViewById(R.id.btn_cancel);
		img_back = (ImageView)findViewById(R.id.img_back_button_invite);
		
	}
	public void initListener(){
		send.setOnClickListener(this);
		cancel.setOnClickListener(this);
		img_back.setOnClickListener(this);
		
	}
	
	public void resize(){
		int count = adapter.getCount();
	    double row = ((float)count)/3;
	    int result = (int)row;
	    double res2 = row - result;
	    Log.d("Count ==>", Integer.toString(count));
	    Log.d("Result ==>", Double.toString(res2));
	    Log.d("Row ==>", Double.toString(row));
	    if(res2>0){
	    	result=result+1;
	    }
	    int size = result*60;
	    Log.d("Result ==>", Integer.toString(result));
	    Log.d("Size ==>", Integer.toString(size));
	    ViewGroup.LayoutParams layoutParams = grid.getLayoutParams();
	    int height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, size, getResources().getDisplayMetrics());
	    layoutParams.height = height; //this is in pixels
	    
	    grid.setLayoutParams(layoutParams);
	}
	
	public int convertToPx(int dp) {
	    // Get the screen's density scale
	    final float scale = getResources().getDisplayMetrics().density;
	    // Convert the dps to pixels, based on density scale
	    return (int) (dp * scale + 0.5f);
	}
		
	@Override
	public void onClick(View v) {
		switch (v.getId()){
		case R.id.img_back_button_invite :
			onBackPressed();
			//startActivity(new Intent(InviteInfoActivity.this,AddPeopleActivity.class));
			
			  break;
		case R.id.btn_send:
			if (!Support.isNetworkConnected(InviteInfoActivity.this)){
				FragmentManager fm = getSupportFragmentManager();
		    	FragmentTransaction ft = fm.beginTransaction();
				notifyDialogInternet = NotifyDialog.newInstance(getString(R.string.no_internet_title), getString(R.string.no_internet_text), NotifyDialog.ButtonsLayout.ONE_BUTTON);
				notifyDialogInternet.show(ft, "dlg1", true);
				//notifyDialogInternet.show(InviteInfoActivity.this);
				break;
			}
			if( LoginActivity.skip_this_step == true){
				FragmentManager fm = getSupportFragmentManager();
		    	FragmentTransaction ft = fm.beginTransaction();
				NotifyDialog dialogFragment = NotifyDialog.newInstance("","Please register or log in to send invitation", NotifyDialog.ButtonsLayout.ONE_BUTTON);
		        dialogFragment.setListener(new DialogInterface.OnClickListener() {

		            @Override
		            public void onClick(DialogInterface dialog, int which) {
		                if (which == Dialog.BUTTON_POSITIVE) {
		                	
		                	startActivity(new Intent(InviteInfoActivity.this,LoginActivity.class));
		                }
		            }
		        });
		        dialogFragment.show(ft, "dlg1", true);
			}
		        if (currentUser!=null && currentUser.getString("phone") == null){
					Log.d("phone","null");
					startChangePhoneDialog();
				}
				else if (currentUser!=null && currentUser.getString("phone").equals("")){
					Log.d("phone","not null");
					startChangePhoneDialog();
					}
				
			else{
				sendInvite();
			}
		break;
		case R.id.btn_cancel:
			//name.clear();
			//finish();
			if (currentUser == null){
				startActivity(new Intent(InviteInfoActivity.this,InspireActivity.class));
				}
				else{
				startActivity(new Intent(InviteInfoActivity.this,ProfileActivity.class));
				}
			
		break;
		}
	}
	
	public void startChangePhoneDialog(){
		final ChangePhoneDialog changePhoneDialog = ChangePhoneDialog.newInstance("Please provide phone number\nin order to\nsend and receive invitations", ChangePhoneDialog.ButtonsLayout.TWO_BUTTONS);
		changePhoneDialog.setListener(new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				 if (which == Dialog.BUTTON_POSITIVE) {
					 changePhoneDialog.dismiss();
					 sendInvite();
				 }
				 else if (which == Dialog.BUTTON_NEGATIVE){
					 changePhoneDialog.dismiss();
				 }
				 				
			}
			
		});
		changePhoneDialog.show(InviteInfoActivity.this);
	}
	
	public void sendInvite(){

		if(adapter.isEmpty()){
			FragmentManager fm = getSupportFragmentManager();
	    	FragmentTransaction ft = fm.beginTransaction();
			emptyDialog = NotifyDialog.newInstance("","Please choose at least one person!", NotifyDialog.ButtonsLayout.ONE_BUTTON);
			emptyDialog.setListener(new DialogInterface.OnClickListener() {

	            @Override
	            public void onClick(DialogInterface dialog, int which) {
	                if (which == Dialog.BUTTON_POSITIVE) {
	                	emptyDialog.dismiss();
	                }
	            }
	        });
			emptyDialog.show(ft, "dlg1", true);
		}else{
			FragmentManager fm = getSupportFragmentManager();
	    	FragmentTransaction ft = fm.beginTransaction();
		progressDialog = NotifyDialog.newInstance("","", NotifyDialog.ButtonsLayout.ONE_BUTTON);
		progressDialog.show(ft, "dlg1", true);
		
		mName = new ArrayList<String>();
		mEmail = new ArrayList<String>();
		mPhone = new ArrayList<String>();
		
		for(int i=0;i<adapter.getCount();i++){
			if(adapter.getItem(i).isSelected()){
			mName.add(adapter.getItem(i).getName());
			Log.d("NAME ==>", adapter.getItem(i).getName());
			if(adapter.getItem(i).getEmail()!=null && adapter.getItem(i).getPhone()!=null){
				if(adapter.getItem(i).isPlanvy_friend()){
					mEmail.add(adapter.getItem(i).getEmail());
				}else{
					mPhone.add(adapter.getItem(i).getPhone());
				}
				
				Log.d("EMAIL ==>", adapter.getItem(i).getEmail());
			}else{
			if(adapter.getItem(i).getEmail()==null && adapter.getItem(i).getPhone()!=null){
				mPhone.add(adapter.getItem(i).getPhone());
				Log.d("PHONE ==>", adapter.getItem(i).getPhone());
			}
			if(adapter.getItem(i).getEmail()!=null && adapter.getItem(i).getPhone()==null){
				mEmail.add(adapter.getItem(i).getEmail());
				Log.d("EMAIL ==>", adapter.getItem(i).getEmail());
			}
			}
			}
		}
		
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("emails", mEmail);
		params.put("phones", mPhone);
		params.put("inviteDescription", message.getText().toString());
		params.put("donateAmount", 1);
		
		for (int i=0; i<mEmail.size();i++){
			Log.e("Email", mEmail.get(i));
		}
		for (int i=0; i<mPhone.size();i++){
			Log.e("Phone", mPhone.get(i));
		}
		
		ParseCloud.callFunctionInBackground("sendInvite", params, new FunctionCallback<String>() {
		    public void done(String success, ParseException e) {
		       if (e == null) {
		          Log.d("INVITE SUCCESS ==>", success);
		          int count_people_send= mEmail.size()+mPhone.size();
		          Map<String, String> people_count_params = new HashMap<String, String>();
		          people_count_params.put("people count", Integer.toString(count_people_send)); 
		          FlurryAgent.logEvent("User send Invite", people_count_params);
		          FragmentManager fm = getSupportFragmentManager();
			    	FragmentTransaction ft = fm.beginTransaction();
		          dialogFragment = NotifyDialog.newInstance("","Your invitation sent successfully!", NotifyDialog.ButtonsLayout.ONE_BUTTON);
			      dialogFragment.setListener(new DialogInterface.OnClickListener() {

			            @Override
			            public void onClick(DialogInterface dialog, int which) {
			                if (which == Dialog.BUTTON_POSITIVE) {
			                	dialogFragment.dismiss();
			                	finish();
			                	startActivity(new Intent(InviteInfoActivity.this, ProfileActivity.class));
			        			overridePendingTransition(R.anim.slide_left_in, R.anim.slide_left_out);
			                }
			            }
			        });
			        dialogFragment.show(ft, "dlg1", true);
		       }
		       else{
		    	   progressDialog.dismiss();
		    	   int err = e.getCode();
		    	   Log.d("Error code:", Integer.toString(err));
		    	   switch(err){
				    case ErrorCode.CONNECTION_FAILED:
				    	FragmentManager fm = getSupportFragmentManager();
				    	FragmentTransaction ft = fm.beginTransaction();
				    	notifyDialogInternet = NotifyDialog.newInstance(getString(R.string.no_internet_title), getString(R.string.no_internet_text), NotifyDialog.ButtonsLayout.ONE_BUTTON);
				    	notifyDialogInternet.show(ft, "dlg1", true);
				    	//tv_warning_profile_activity.setText("Problem with Internet. Please try again.");
				    	//tv_warning_profile_activity.setVisibility(View.VISIBLE);
       				break;
				    case ErrorCode.SCRIPT_ERROR:
				    	/*if (e.getMessage().equals("User are not authorized") ){
				    		startActivity(new Intent(ProfileActivity.this, LoginActivity.class));
				    	}*/
				    break;
				    default:
        	    		/*tv_warning_profile_activity.setText(e.getMessage());
        	    		tv_warning_profile_activity.setVisibility(View.VISIBLE);*/
            	    	}
		    	  
		    	  // progress_bar_profile.setVisibility(View.GONE);

		    	   e.printStackTrace();
		       }
		   }
		});
		return;
		
		}
	}
	
	@Override
	public void onBackPressed() {
		ArrayList<ContactModel> tmp = new ArrayList<ContactModel>();
		for(int i=0;i<adapter.getCount();i++){
			tmp.add(adapter.getItem(i));
		}
		mSelected = ((App)getApplicationContext()).getArrayModel();
		((App)getApplicationContext()).setArrayModel(/*syncArrays(tmp,*/ mSelected)/*)*/;
		super.onBackPressed();
	}
	
	public ArrayList<ContactModel> getCheckedList(ArrayList<ContactModel> list){
		ArrayList<ContactModel> mList = new ArrayList<ContactModel>();
		for(ContactModel cModel:list){
			if(cModel.isSelected())mList.add(cModel);
		}
		return mList;
	}
	
	public ArrayList<ContactModel> syncArrays(ArrayList<ContactModel> list,
											  ArrayList<ContactModel> slist){
		ArrayList<ContactModel> tmp = new ArrayList<ContactModel>();
		for(ContactModel cModol:list){
			for(ContactModel mModol:slist){
				if(cModol.getName().equals(mModol.getName())){
					mModol.setSelected(false);
				}
				mModol.setSelected(false);
				tmp.add(mModol);
			}
		}
		return tmp;
		
	}
}
