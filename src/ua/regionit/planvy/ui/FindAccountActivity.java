package ua.regionit.planvy.ui;

import java.util.List;

import org.holoeverywhere.app.Activity;
import org.holoeverywhere.widget.Button;
import org.holoeverywhere.widget.EditText;
import org.holoeverywhere.widget.ProgressBar;
import org.holoeverywhere.widget.TextView;

import ua.regionit.planvy.ErrorCode;
import ua.regionit.planvy.R;
import ua.regionit.planvy.ui.dialog.NotifyDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

import com.flurry.android.FlurryAgent;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;


public class FindAccountActivity extends Activity implements OnClickListener {
	Button btn_find_account;
	ImageView imgbackButton;
	EditText edt_username_or_email;
	TextView tv_warning_password_recovery;
	String username_or_email;
	ProgressBar progressBarPasswordRecovery;
	public static final String EMAIL ="email";
	public static final String NICKNAME ="nickname";
	ParseQuery<ParseUser> query;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.password_recovery);
		FlurryAgent.logEvent("FindAccount", true);
		initUi();
		initListener();
	}
	
	@Override
	protected void onStart()
	{
		super.onStart();
		FlurryAgent.onStartSession(this, "XWHZX63GBXQKSR9KJGQ9");
	}
	
	@Override
	protected void onStop()
	{
		super.onStop();		
		FlurryAgent.onEndSession(this);
	}

	public void initUi(){
		btn_find_account = (Button) findViewById(R.id.btn_find_account);		
		edt_username_or_email = (EditText) findViewById(R.id.username_or_email);
		tv_warning_password_recovery = (TextView) findViewById(R.id.tv_warning_password_recovery);
		progressBarPasswordRecovery = (ProgressBar) findViewById(R.id.progressBarPasswordRecovery);
		imgbackButton = (ImageView) findViewById(R.id.imgbackButton);
	} 
	public void initListener(){
		btn_find_account.setOnClickListener(this);
		imgbackButton.setOnClickListener(this);
	}
	
	public void getTextFromPasswordRecoveryView(){
		username_or_email = edt_username_or_email.getText().toString();
		
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()){
		case R.id.imgbackButton:
			onBackPressed();
			break;
		case R.id.btn_find_account:
			tv_warning_password_recovery.setVisibility(View.GONE);
			progressBarPasswordRecovery.setVisibility(View.VISIBLE);
			getTextFromPasswordRecoveryView();
			if(username_or_email.equals("")){
				NotifyDialog notifyDialog = NotifyDialog.newInstance("", "Please enter username or email", NotifyDialog.ButtonsLayout.ONE_BUTTON );
				notifyDialog.show(this);
				/*tv_warning_password_recovery.setText("Empty email or username");
				tv_warning_password_recovery.setVisibility(View.VISIBLE);*/
				progressBarPasswordRecovery.setVisibility(View.GONE);
				break;
			}
			
			query = ParseUser.getQuery();
			query.whereEqualTo("email", username_or_email);
			
		
			query.findInBackground(new FindCallback<ParseUser>() {
			  public void done(List<ParseUser> users, ParseException e) {
			    if (e == null) {
			    	
			    	
			    	Log.d("Object count",Integer.toString(users.size()));
			    	
			    	if (users.size() == 0){
			    		query = ParseUser.getQuery();
			    		query.whereEqualTo("nickname", username_or_email);
			    		query.findInBackground(new FindCallback<ParseUser>(){

							@Override
							public void done(List<ParseUser> users1,
									ParseException e1) {
								 if (e1 == null) {
									 progressBarPasswordRecovery.setVisibility(View.GONE);
									 if (users1.size()==0){
										 Log.e("It's","here1");
										 Log.d("Length user1",Integer.toString(users1.size()));
										 NotifyDialog notifyDialogValidName = NotifyDialog.newInstance("", "No users were found", NotifyDialog.ButtonsLayout.ONE_BUTTON );
										 notifyDialogValidName.show(FindAccountActivity.this);
										 //tv_warning_password_recovery.setText("User or email " + username_or_email+" not found");
										 //tv_warning_password_recovery.setVisibility(View.VISIBLE);
									 }
									 else{
								    	String email = (String) users1.get(0).get(EMAIL);
								    	Intent intent = new Intent(FindAccountActivity.this, PasswordRecoveryActivity.class );
								    	intent.putExtra(EMAIL, email);
								    	intent.putExtra(NICKNAME, username_or_email);
								    	startActivity(intent);}
								 }
								 else{
									 progressBarPasswordRecovery.setVisibility(View.GONE);
								        e1.printStackTrace();
								        int err = e1.getCode();
										Log.d("Error code:", Integer.toString(err));
					        	    	switch(err){
					        	    	case ErrorCode.INVALID_EMAIL_ADDRESS:
					        	    		tv_warning_password_recovery.setText("Invalid email");
					        	    		tv_warning_password_recovery.setVisibility(View.VISIBLE);
					        				break;
					        	    	case ErrorCode.EMAIL_NOT_FOUND:
					        	    		NotifyDialog notifyDialogValidName = NotifyDialog.newInstance("", "No users were found", NotifyDialog.ButtonsLayout.ONE_BUTTON );
											 notifyDialogValidName.show(FindAccountActivity.this);
					        	    		/*tv_warning_password_recovery.setText("User or email " + username_or_email+" not found");
					        	    		tv_warning_password_recovery.setVisibility(View.VISIBLE);*/
					        	    		break;
					        	    	case ErrorCode.CONNECTION_FAILED:
					        	    		tv_warning_password_recovery.setText("Problem with Internet. Please try again.");
					        	    		tv_warning_password_recovery.setVisibility(View.VISIBLE);
				            				break;
					        	    	default:
					        	    		tv_warning_password_recovery.setText(e1.getMessage());
					        	    		tv_warning_password_recovery.setVisibility(View.VISIBLE);
					        	    
					        	    	}
					        	    	e1.printStackTrace();
					        	    	String message = e1.getMessage();
					        	    	Log.d("Error Message:",message);
								 }
								
							}
			    			
			    		});
			    		/*tv_warning_password_recovery.setText("User or email " + username_or_email+" not found");
        	    		tv_warning_password_recovery.setVisibility(View.VISIBLE);*/
        	    		
			    	}
			    	else{
			    	progressBarPasswordRecovery.setVisibility(View.GONE);
			    	String nickname = (String) users.get(0).get(NICKNAME);
			    	Intent intent = new Intent(FindAccountActivity.this, PasswordRecoveryActivity.class );
			    	intent.putExtra(EMAIL, username_or_email);
			    	intent.putExtra(NICKNAME, nickname);
			    	startActivity(intent); }
			      
			    } else {
			    	progressBarPasswordRecovery.setVisibility(View.GONE);
			        e.printStackTrace();
			        int err = e.getCode();
					Log.d("Error code:", Integer.toString(err));
        	    	switch(err){
        	    	case ErrorCode.INVALID_EMAIL_ADDRESS:
        	    		tv_warning_password_recovery.setText("Invalid email");
        	    		tv_warning_password_recovery.setVisibility(View.VISIBLE);
        				break;
        	    	case ErrorCode.EMAIL_NOT_FOUND:
        	    		NotifyDialog notifyDialogValidName = NotifyDialog.newInstance("", "No users were found", NotifyDialog.ButtonsLayout.ONE_BUTTON );
						 notifyDialogValidName.show(FindAccountActivity.this);
        	    		/*tv_warning_password_recovery.setText("User or email " + username_or_email+" not found");
        	    		tv_warning_password_recovery.setVisibility(View.VISIBLE);*/
        	    		break;
        	    	default:
        	    		tv_warning_password_recovery.setText(e.getMessage());
        	    		tv_warning_password_recovery.setVisibility(View.VISIBLE);
        	    	}
        	    	e.printStackTrace();
        	    	String message = e.getMessage();
        	    	Log.d("Error Message:",message);
			    }
			  }
			});
			break;
		}
		
	}
	
}
