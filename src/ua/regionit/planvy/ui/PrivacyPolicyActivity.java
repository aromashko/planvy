package ua.regionit.planvy.ui;

import org.holoeverywhere.app.Activity;

import com.flurry.android.FlurryAgent;

import ua.regionit.planvy.R;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

public class PrivacyPolicyActivity extends Activity implements OnClickListener{

	ImageView back;
	TextView tv_1, tv_2, tv_3, tv_4, tv_5, tv_6, tv_7, tv_8, tv_9, tv_10,
			 tv_11, tv_12, tv_13, tv_14, tv_15, tv_16, tv_17, tv_18, tv_19, tv_20,
			 tv_21, tv_22, tv_23, tv_24, tv_25, tv_26, tv_27, tv_28, tv_29, tv_30;
	Typeface helveticaNeue, helveticaLight, helveticaUltraLight, helveticaLigthItalic;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_privacy_policy);
		FlurryAgent.logEvent("Privacy", true);
		initUi();
		initListener();
		helveticaNeue = Typeface.createFromAsset(getAssets(), "HelveticaNeue.ttf");
		helveticaLight = Typeface.createFromAsset(getAssets(), "HelveticaNeueLight.ttf");
		helveticaUltraLight = Typeface.createFromAsset(getAssets(), "HelveticaNeueUltraLight.ttf");
		helveticaLigthItalic = Typeface.createFromAsset(getAssets(), "HelveticaNeueLightItalic.ttf");
		
//		tv_1.setTypeface(helveticaNeue);
//		tv_2.setTypeface(helveticaNeue);
	}
	
	@Override
	protected void onStart()
	{
		super.onStart();
		FlurryAgent.onStartSession(this, "XWHZX63GBXQKSR9KJGQ9");
	}
	
	@Override
	protected void onStop()
	{
		super.onStop();		
		FlurryAgent.onEndSession(this);
	}
	public void initUi(){
		back = (ImageView)findViewById(R.id.btn_privacy_back);
		tv_1 = (TextView)findViewById(R.id.tv_privacy_1);
		tv_2 = (TextView)findViewById(R.id.tv_privacy_2);
		tv_3 = (TextView)findViewById(R.id.tv_privacy_3);
		tv_4 = (TextView)findViewById(R.id.tv_privacy_4);
		tv_5 = (TextView)findViewById(R.id.tv_privacy_5);
		tv_6 = (TextView)findViewById(R.id.tv_privacy_6);
		tv_7 = (TextView)findViewById(R.id.tv_privacy_7);
		tv_8 = (TextView)findViewById(R.id.tv_privacy_8);
		tv_9 = (TextView)findViewById(R.id.tv_privacy_9);
		tv_10 = (TextView)findViewById(R.id.tv_privacy_10);
		tv_11 = (TextView)findViewById(R.id.tv_privacy_11);
		tv_12 = (TextView)findViewById(R.id.tv_privacy_12);
		tv_13 = (TextView)findViewById(R.id.tv_privacy_13);
		tv_14 = (TextView)findViewById(R.id.tv_privacy_14);
		tv_15 = (TextView)findViewById(R.id.tv_privacy_15);
		tv_16 = (TextView)findViewById(R.id.tv_privacy_16);
		tv_17 = (TextView)findViewById(R.id.tv_privacy_17);
		tv_18 = (TextView)findViewById(R.id.tv_privacy_18);
		tv_19 = (TextView)findViewById(R.id.tv_privacy_19);
		tv_20 = (TextView)findViewById(R.id.tv_privacy_20);
		tv_21 = (TextView)findViewById(R.id.tv_privacy_21);
		tv_22 = (TextView)findViewById(R.id.tv_privacy_22);
		tv_23 = (TextView)findViewById(R.id.tv_privacy_23);
		tv_24 = (TextView)findViewById(R.id.tv_privacy_24);
		tv_25 = (TextView)findViewById(R.id.tv_privacy_25);
		tv_26 = (TextView)findViewById(R.id.tv_privacy_26);
		tv_27 = (TextView)findViewById(R.id.tv_privacy_27);
		tv_28 = (TextView)findViewById(R.id.tv_privacy_28);
		tv_29 = (TextView)findViewById(R.id.tv_privacy_29);
		tv_30 = (TextView)findViewById(R.id.tv_privacy_30);
	}
	
	public void initListener(){
		back.setOnClickListener(this);
	}
	
	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.btn_privacy_back:
			finish();
		break;
		
	}
	}
}
