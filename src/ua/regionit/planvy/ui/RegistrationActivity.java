package ua.regionit.planvy.ui;



import org.holoeverywhere.app.Activity;
import org.holoeverywhere.widget.Button;
import org.holoeverywhere.widget.EditText;
import org.holoeverywhere.widget.ProgressBar;
import org.holoeverywhere.widget.TextView;

import ua.regionit.planvy.AppLog;
import ua.regionit.planvy.ErrorCode;
import ua.regionit.planvy.R;
import ua.regionit.planvy.ui.dialog.NotifyDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

import com.flurry.android.FlurryAgent;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SignUpCallback;


public class RegistrationActivity extends Activity implements OnClickListener, ErrorCode {
	TextView tv_term_of_use, tv_privacy_policy, tv_warning_registration;
	Button btn_create_acount;
	String userName, email, password, confirm_password, phone, mPhoneNumber = "";
	EditText edt_email, edt_register_name, edt_password, edt_password_confirm, edt_phone_registration;
	ProgressBar registration_progress;
	ImageView imgBackButton;
	NotifyDialog notifyDialogError, notifyDialogInternet;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_registration);
		FlurryAgent.logEvent("Register", true);
		initUi();
		initListener();
		SpannableString content = new SpannableString("Terms of Service");
		content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
		tv_term_of_use.setText(content);
		
		SpannableString content2 = new SpannableString("Privacy Policy");
		content2.setSpan(new UnderlineSpan(), 0, content2.length(), 0);
		tv_privacy_policy.setText(content2);
		TelephonyManager tMgr =(TelephonyManager)getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE);
		 mPhoneNumber = tMgr.getLine1Number();		 
		 if (mPhoneNumber != null &&  mPhoneNumber.length()>2 && mPhoneNumber.substring(0, 2).equals("+1")){
		 AppLog.e("My phone number", mPhoneNumber);
		 edt_phone_registration.setText(mPhoneNumber);}
		 edt_phone_registration.setSelection(edt_phone_registration.getText().length());
		 
		 edt_phone_registration.addTextChangedListener(new TextWatcher() {
			   public void afterTextChanged(Editable s) {
				   Log.d("after", "called");
				   
				 
				  if (edt_phone_registration.getText().length()<2){
					 
						  edt_phone_registration.setText("+1");
						  edt_phone_registration.setSelection(edt_phone_registration.getText().length());
					  
				   }
				  else{
					   if (edt_phone_registration.getText().charAt(0) != '+'){
						   edt_phone_registration.setText("+1");
						   edt_phone_registration.setSelection(edt_phone_registration.getText().length());
						   }
					   if (edt_phone_registration.getText().charAt(1) !='1'){
						   edt_phone_registration.setText("+1");
						   edt_phone_registration.setSelection(edt_phone_registration.getText().length());
					   }
				   }
				  			 
				  
			   }
			   public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				  
			   }
			   public void onTextChanged(CharSequence s, int start, int before, int count) {
			   }
			 });
		 
		
	}
	
	@Override
	protected void onStart()
	{
		super.onStart();
		FlurryAgent.onStartSession(this, "XWHZX63GBXQKSR9KJGQ9");
	}
	
	@Override
	protected void onStop()
	{
		super.onStop();		
		FlurryAgent.onEndSession(this);
	}
	
	 
	public void initUi(){
		tv_term_of_use = (TextView) findViewById(R.id.tv_term_of_use);
		tv_privacy_policy = (TextView) findViewById(R.id.tv_privacy_policy);
		tv_warning_registration= (TextView) findViewById(R.id.tv_warning_registration);
		btn_create_acount = (Button) findViewById(R.id.btn_create_acount);
		edt_email = (EditText) findViewById(R.id.edt_email);
		edt_register_name= (EditText) findViewById(R.id.edt_username_registration);
		edt_password= (EditText) findViewById(R.id.edt_password);
		registration_progress = (ProgressBar) findViewById(R.id.registration_progress);
		imgBackButton = (ImageView) findViewById(R.id.imgbackButton);
		edt_phone_registration = (EditText) findViewById(R.id.edt_phone_registration);
	}
	
	public void initListener(){
		btn_create_acount.setOnClickListener(this);
		tv_term_of_use.setOnClickListener(this);
		tv_privacy_policy.setOnClickListener(this);
		imgBackButton.setOnClickListener(this);
	}
	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.btn_create_acount:
			getTextFromRegistrationView();
			tv_warning_registration.setVisibility(View.GONE);
			if (userName.equals("")){
				tv_warning_registration.setText("Username cannot be blank");
				tv_warning_registration.setVisibility(View.VISIBLE);
				break;
			}
			
			if (phone.equals("")){
				tv_warning_registration.setText("Phone number cannot be blank");
				tv_warning_registration.setVisibility(View.VISIBLE);
				break;
				
			}
			
			if (phone.charAt(0) != '+'){
				phone = "+" + phone;
				edt_phone_registration.setText(phone);
				
			}
			
			if (phone.length()<=11){
				tv_warning_registration.setText("Phone number is too short");
				tv_warning_registration.setVisibility(View.VISIBLE);
				break;
				
			}
			
			if (phone.length()>=16){
				tv_warning_registration.setText("Phone number is too long");
				tv_warning_registration.setVisibility(View.VISIBLE);
				break;
				
			}
			
			if (email.equals("")){
				tv_warning_registration.setText("Email cannot be blank");
				tv_warning_registration.setVisibility(View.VISIBLE);
				break;
				
			}
			
			if (password.equals("")){
				tv_warning_registration.setText("Password cannot be blank");
				tv_warning_registration.setVisibility(View.VISIBLE);
				break;
			}
			
			createCheckDialog(email, phone);
			
			break;
		case R.id.tv_term_of_use:
			startActivity(new Intent(RegistrationActivity.this, TermOfUseActivity.class));
			break;
		case R.id.tv_privacy_policy:
			startActivity(new Intent(RegistrationActivity.this, PrivacyPolicyActivity.class));
			break;
		case R.id.imgbackButton:
			finish();
			break;
		}
		
	}
	
	public void createCheckDialog(String email, String phone_of_user){
		NotifyDialog dialogFragment = NotifyDialog.newInstance("Is this correct?",
                "You entered your email &\n phone as:\n"+email+ "\n"+phone_of_user, NotifyDialog.ButtonsLayout.TWO_BUTTONS);
        dialogFragment.setListener(new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == Dialog.BUTTON_POSITIVE) {
                	registration_progress.setVisibility(View.VISIBLE);
                	//ParseObject myParseObject = new ParseObject("AndroidUser");
                	ParseUser user = new ParseUser();
                	user.setUsername(RegistrationActivity.this.email);
                	user.setPassword(password);
                	user.setEmail(RegistrationActivity.this.email);
                	user.put("nickname", userName);
                	user.put("phone", phone);
                	
                	user.signUpInBackground(new SignUpCallback() {
                	  public void done(ParseException e) {
                	    if (e == null) {
                	    	ParseUser currentUser = ParseUser.getCurrentUser();
                	    	ParseInstallation installation = ParseInstallation.getCurrentInstallation();
	        		    	installation.put("owner", currentUser);
	        		    	installation.saveInBackground(new SaveCallback() {
								
								@Override
								public void done(ParseException e) {
									if (e!=null){
										e.printStackTrace();
									}
									
								}
							});
                	    	FlurryAgent.logEvent("User Register", true);
                	    	registration_progress.setVisibility(View.GONE);
                	    	startActivity(new Intent(RegistrationActivity.this, InspireActivity.class));
                	    	finish();
                	      
                	    } else {
                	    	registration_progress.setVisibility(View.GONE);
                	    	int err = e.getCode();
                	    	Log.d("Error code:", Integer.toString(err));
                	    	switch(err){
                	    	/*case ErrorCode.INVALID_EMAIL_ADDRESS:
                	    		tv_warning_registration.setText("Invalid email");
                				tv_warning_registration.setVisibility(View.VISIBLE);
                				break;
                	    	case ErrorCode.EMAIL_TAKEN:
                	    		tv_warning_registration.setText("The email " +RegistrationActivity.this.email + " is already taken");
                				tv_warning_registration.setVisibility(View.VISIBLE);
                				break;
                	    	case ErrorCode.VALIDATION_ERROR:
                	    		tv_warning_registration.setText("The username " +userName + " is not available");
                				tv_warning_registration.setVisibility(View.VISIBLE);
                				break;
                	    	case ErrorCode.USERNAME_TAKEN:
                	    		tv_warning_registration.setText("The email " +RegistrationActivity.this.email + " is already taken");
                				tv_warning_registration.setVisibility(View.VISIBLE);
                				break;*/
                	    	case ErrorCode.CONNECTION_FAILED:
                	    		notifyDialogInternet = NotifyDialog.newInstance(getString(R.string.no_internet_title), getString(R.string.no_internet_text), NotifyDialog.ButtonsLayout.ONE_BUTTON);
                		    	notifyDialogInternet.show(RegistrationActivity.this);
                				break;
                	    	default:
                	    		notifyDialogError =  NotifyDialog.newInstance("", e.getMessage(), NotifyDialog.ButtonsLayout.ONE_BUTTON);
                	    		notifyDialogError.show(RegistrationActivity.this);
                	    	}
                	    	
                	    	registration_progress.setVisibility(View.GONE);
                	    	e.printStackTrace();
                	    	String message = e.getMessage();
                	    	Log.d("Error Message",message);
                	    }
                	  }
                	});
               
                        }
            }
        });
        dialogFragment.show(this);
	}
	
	public void getTextFromRegistrationView(){
		userName = edt_register_name.getText().toString();
		email = edt_email.getText().toString();
		password = edt_password.getText().toString();
		phone = edt_phone_registration.getText().toString();
		
	}

}
