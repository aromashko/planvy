package ua.regionit.planvy.ui;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import org.holoeverywhere.app.Activity;
import org.holoeverywhere.widget.LinearLayout;
import org.holoeverywhere.widget.ListView;
import org.holoeverywhere.widget.ProgressBar;
import org.holoeverywhere.widget.TextView;
import org.holoeverywhere.widget.Toast;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import ua.regionit.planvy.AppLog;
import ua.regionit.planvy.ErrorCode;
import ua.regionit.planvy.R;
import ua.regionit.planvy.ui.dialog.ChangePictureDialog;
import ua.regionit.planvy.ui.dialog.DropDownListProfile;
import ua.regionit.planvy.ui.dialog.NotifyDialog;
import ua.regionit.planvy.ui.dialog.ProfileItemDialog;
import ua.regionit.planvy.util.InvitesProfileAdapter;
import ua.regionit.planvy.util.InvitesProfileModel;
import ua.regionit.planvy.util.MD5Hash;
import ua.regionit.planvy.util.MyBroadcastReceiver;
import ua.regionit.planvy.util.MySwitch;
import ua.regionit.planvy.util.NotificationProfileAdapter;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.net.http.HttpResponseCache;
import android.os.Build;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;

import com.flurry.android.FlurryAgent;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.PauseOnScrollListener;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;
import com.parse.FunctionCallback;
import com.parse.GetDataCallback;
import com.parse.Parse;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseInstallation;
import com.parse.ParseUser;
import com.parse.PushService;
import com.parse.SaveCallback;





public class ProfileActivity extends Activity implements OnClickListener {
	TextView tv_username_profile, tv_count_not_paid, tv_sent, tv_received, tv_warning_profile_activity, tv_give_profile;
	ImageView img_user_picture_profile, img_add_people_profile,img_settings_profile, img_notification_profile;
	ProgressBar progress_bar_profile;
	LinearLayout pic_here_profile;
	Bitmap bitmap, resizedbitmap, thumbnail, resizedbitmap_from_camera, usersPhoto, resizedbitmap_from_gallery, resizedbitmap_avatar;
	ListView lv_invites_profile;
	String nickname = "", email = "", description, timeDiffrence, title;
	InvitesProfileModel p;
	int sdk;
	private static final int REQUEST_CODE_GALLERY=1;
	private static final int REQUEST_CODE_CAMERA=2;
	public static final int REQUEST_CODE_LOG_OUT = 3;
	//public static String GO_FROM_P
	ParseUser currentUser;
	ParseFile file;
	InvitesProfileAdapter mAdapterSent, mAdapterSentNew, mAdapterReceived, mAdapterReceivedNew;
	InvitesProfileModel invitesProfileModel;
	MySwitch switch_profile;
	DisplayImageOptions options;
	String[] imageUrls;
	 String urlToPhoto="", toPhone = "", selectedImagePath;
	 boolean pauseOnScroll = false; 
	 boolean pauseOnFling = true; 
	 ImageLoader imageLoader; 
	 int defaultTextColor;
	 int value_width_height, mCountSendToMe =0;
	 Bitmap mLoadedImageSent, mLoadedImageReceived;
	 Boolean isPaid= false, isRead=false;
	 int number_send_to_me =0, number_send_by_me=0;
	 NotificationProfileAdapter notificationProfileAdapter;
	 DropDownListProfile dropDownListProfile;
	 int value_width_height_avatar, amountPaid;
	 SharedPreferences sPref;
	 Boolean countReceived= false, listSendByMeReceived= false, listSendToMeReceived= false;
	 View top_list_line_profile;
	 String hashKeySendByMe, hashKeySendToMe;
	 MyBroadcastReceiver receiver;
	 Boolean mChecked = true;
	 NotifyDialog notifyDialogInternet;
	 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.profile_activity);
		FlurryAgent.logEvent("Profile", true);
		sdk = android.os.Build.VERSION.SDK_INT;
		initUi();
		initListener();
		currentUser = ParseUser.getCurrentUser();
		
		//notifyDialogInternet.show(this);
		mAdapterSent = new InvitesProfileAdapter(this);
		mAdapterReceived = new InvitesProfileAdapter(this, InvitesProfileAdapter.SEND_TO_ME);
		mAdapterSentNew = new InvitesProfileAdapter(this);
		mAdapterReceivedNew = new InvitesProfileAdapter(this, InvitesProfileAdapter.SEND_TO_ME);
		notificationProfileAdapter = new NotificationProfileAdapter(this);
		tv_username_profile.setText(currentUser.getString("nickname"));
		imageLoader = ImageLoader.getInstance();
		dropDownListProfile = DropDownListProfile.newInstance(ProfileActivity.this);
		PauseOnScrollListener listener = new PauseOnScrollListener(imageLoader, pauseOnScroll, pauseOnFling);
		
		lv_invites_profile.setOnScrollListener(listener);
		options = new DisplayImageOptions.Builder()
        .showImageOnLoading(R.drawable.prf_cell_icon_placeholder)
        .showImageForEmptyUri(R.drawable.prf_cell_icon_placeholder)
        .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
        .cacheInMemory(true)
        .cacheOnDisc(true)
        .build();
		//receiver = new MyBroadcastReceiver();
		receiver = new MyBroadcastReceiver(){
			 @Override
	            public void onReceive(Context context, Intent intent) {
				 Log.e("Receive in activity","Push");
				 mAdapterReceived.clear(); 
				 notificationProfileAdapter.clear();
				 listSendToMeReceived= false;
				 	getInvitesSendToMe(0,20);
					getNotPaidInvitesCountSendToMe();
	            }
		};
		
		loadAvatar();
		
		getNotPaidInvitesCountSendToMe();
		getInvitesSendToMe(0,20);
		getInvitesSendByMe(0,20);
		
		addSwitchListener();
		  
	}
	
	
	@Override
	protected void onStart() {
		super.onStart();
		LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter("REFRESH_LIST"));
		FlurryAgent.onStartSession(this, "XWHZX63GBXQKSR9KJGQ9");
	}

	@Override
	protected void onStop() {
		super.onStop();
		LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
		FlurryAgent.onEndSession(this);
	}

	public void initUi(){
		tv_username_profile = (TextView) findViewById(R.id.tv_username_profile);
		img_user_picture_profile = (ImageView) findViewById(R.id.img_user_picture_profile_item);
		pic_here_profile = (LinearLayout) findViewById(R.id.pic_here_profile_item_dialog);
		progress_bar_profile = (ProgressBar) findViewById(R.id.progress_bar_profile);
		lv_invites_profile = (ListView) findViewById(R.id.lv_invites_profile);
		switch_profile = (MySwitch) findViewById(R.id.switch_profile);
		img_add_people_profile = (ImageView) findViewById(R.id.img_add_people_profile);
		img_settings_profile =(ImageView) findViewById(R.id.img_settings_profile);
		tv_count_not_paid = (TextView) findViewById(R.id.tv_count_not_paid);
		tv_sent = (TextView) findViewById(R.id.tv_sent);
		tv_received = (TextView) findViewById(R.id.tv_received);
		img_notification_profile = (ImageView) findViewById(R.id.img_notification_profile);
		tv_warning_profile_activity = (TextView) findViewById(R.id.tv_warning_profile_activity);
		tv_give_profile = (TextView) findViewById(R.id.tv_give_profile);
		top_list_line_profile = findViewById(R.id.top_list_line_profile);
	}
	public void initListener(){
		pic_here_profile.setOnClickListener(this);
		img_add_people_profile.setOnClickListener(this);
		img_settings_profile.setOnClickListener(this);
		img_notification_profile.setOnClickListener(this);
		tv_give_profile.setOnClickListener(this);
		
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.pic_here_profile_item_dialog:
			ChangePictureDialog changePictureDialog = ChangePictureDialog.newInstance();
			changePictureDialog.setListener(new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					if (resizedbitmap != null){
						resizedbitmap.recycle();
		            }
					 if (which == ChangePictureDialog.TAKE_PICTURE) {
						 Log.d("Choose Picture", "True");
						 dispatchTakePictureIntent(REQUEST_CODE_CAMERA);
						 
					 }
					 if (which == ChangePictureDialog.CHOOSE_PICTURE) {	 
						 Log.d("Take Picture", "true");
						 pickImage();	
					 }
				}
	        
	        });
			changePictureDialog.show(this);
	        break;
		case R.id.img_add_people_profile:
			AddPeopleActivity.goFromProfile =true;
			Intent intent =new Intent(ProfileActivity.this, AddPeopleActivity.class);
			startActivity(intent);
			finish();
			break;
		case R.id.img_settings_profile:
			Intent intentSetting =new Intent(ProfileActivity.this, SettingsActivity.class);
			startActivityForResult(intentSetting, REQUEST_CODE_LOG_OUT);
			finish();
			
			break;
		case R.id.img_notification_profile:
			number_send_to_me = 0;
			notificationProfileAdapter.clear();
			getInvitesSendToMeNotPaid(0,20);
			if (countReceived){
			 sPref = getPreferences(MODE_PRIVATE);
        	 Editor ed = sPref.edit();
        	 ed.putInt(currentUser.getEmail(), mCountSendToMe);
        	 ed.commit();
        	 tv_count_not_paid.setVisibility(View.GONE);
			}
			dropDownListProfile.show(this);
			break;
		case R.id.tv_give_profile:
			startActivity(new Intent(ProfileActivity.this, DonateWithOutInvite.class));
			break;
		}
		
	}
	
	public void addSwitchListener(){
		defaultTextColor = tv_sent.getTextColors().getDefaultColor();
		tv_sent.setTextColor(Color.parseColor("#00AAED"));
		switch_profile.setChecked(true);
		  switch_profile.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked){
					mChecked = true;
					lv_invites_profile.setAdapter(mAdapterSent);
					//mAdapterSent.clear();
					tv_sent.setTextColor(Color.parseColor("#00AAED"));
					tv_received.setTextColor(defaultTextColor);
					number_send_by_me=0;
					
					lv_invites_profile.setOnItemClickListener(new OnItemClickListener() {

						@Override
						public void onItemClick(AdapterView<?> adapterView, View view,
								int position, long id) {
							getBitmapToItemDialogSent(position);
							ProfileItemDialog profileItemDialog = ProfileItemDialog.newInstance(ProfileActivity.this, mAdapterSent.getItem(position).getName(), mAdapterSent.getItem(position).getDescription(), mAdapterSent.getItem(position).getUrlToPhoto(),  mLoadedImageSent, mAdapterSent.getItem(position).getCharityTitle(), mAdapterSent.getItem(position).getAmountPaid(), mAdapterSent.getItem(position).getDateOfMessage(), ProfileItemDialog.SEND_BY_ME, ProfileItemDialog.ButtonsLayout.ONE_BUTTON);
							profileItemDialog.show(ProfileActivity.this);
							
						}
			    		   
					});
					
					/*if(listSendByMeReceived){
						
						if (mAdapterSentNew.getCount()>0 && mAdapterSent.getCount() == mAdapterSentNew.getCount()){
							for (int i=0;i<mAdapterSent.getCount();i++){
								if (!mAdapterSent.getItem(i).equals(mAdapterSentNew.getItem(i))){
									mAdapterSent = mAdapterSentNew;
									Log.e("New", "Adapter");
								}
							}
						}
						else{
							mAdapterSent = mAdapterSentNew;
						}
						mAdapterSentNew.clear();
						getInvitesSendByMe(0,20);
					lv_invites_profile.setAdapter(mAdapterSent);
					}
					else{
						getInvitesSendByMe(0,20);
					}*/
				}
				else{
					mChecked = false;
					lv_invites_profile.setOnItemClickListener(new OnItemClickListener() {

						@Override
						public void onItemClick(AdapterView<?> adapterView, View view,
								int position, long id) {
							getBitmapToItemDialogReceived(position);
							//String donatedMessage = "Donated " + mAdapterReceived.getItem(position).getAmountPaid() + "$" + "for" + mAdapterReceived.getItem(position).getCharityTitle();
							Log.e("Charity dialog",mAdapterReceived.getItem(position).getCharityTitle()+ "charity title");
							ProfileItemDialog profileItemDialog = ProfileItemDialog.newInstance(ProfileActivity.this, mAdapterReceived.getItem(position).getName(), mAdapterReceived.getItem(position).getDescription(), mAdapterReceived.getItem(position).getUrlToPhoto(),  mLoadedImageReceived, mAdapterReceived.getItem(position).getCharityTitle(), mAdapterReceived.getItem(position).getAmountPaid(),  mAdapterReceived.getItem(position).getDateOfMessage(), ProfileItemDialog.SEND_TO_ME, ProfileItemDialog.ButtonsLayout.ONE_BUTTON);
							profileItemDialog.show(ProfileActivity.this);
							
						}
			    		   
					});
					lv_invites_profile.setAdapter(mAdapterReceived);
					//mAdapterReceived.clear();
					tv_sent.setTextColor(defaultTextColor);
					tv_received.setTextColor(Color.parseColor("#E25260"));
					number_send_to_me = 0;
					/*if (listSendToMeReceived){
						//getInvitesSendToMe(0,20);
						if (mAdapterReceivedNew.getCount()>0 && mAdapterReceivedNew.getCount() == mAdapterReceived.getCount()){
							for (int i=0;i<mAdapterReceived.getCount();i++){
								if (!mAdapterReceived.getItem(i).equals(mAdapterReceivedNew.getItem(i))){
									mAdapterReceived = mAdapterReceivedNew;
									Log.e("New", "Adapter");
								}
							}
						}
						else{
							mAdapterReceived = mAdapterReceivedNew;
						}
						mAdapterReceivedNew.clear();
						getInvitesSendToMe(0,20);
					lv_invites_profile.setAdapter(mAdapterReceived);
					}
					else{
						getInvitesSendToMe(0,20);
					}*/
				}
				
			}
		});
	}
	
	private void pickImage(){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(intent, REQUEST_CODE_GALLERY);
    }
	
	
	@SuppressWarnings("deprecation")
	@SuppressLint("NewApi")
	@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        InputStream stream=null;
        /*if (resultCode == SettingsFragment.RESULT_LOG_OUT){
        	Intent intent = new Intent(ProfileActivity.this, LoginActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); 
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			
			startActivity(intent);
        	finish();
        }*/
        if (requestCode == REQUEST_CODE_CAMERA && resultCode == Activity.RESULT_OK && data!= null) {
        	if (thumbnail != null){
        		thumbnail.recycle();
            }
        	thumbnail = (Bitmap) data.getExtras().get("data");
        	
        	ByteArrayOutputStream stream_camera = new ByteArrayOutputStream();
        	thumbnail.compress(Bitmap.CompressFormat.PNG, 100, stream_camera);
        	int value_width = (int) dipToPixels(this, 70);
        	int value_height = (int) dipToPixels(this, 70);
        	
        	if (thumbnail.getWidth() >= thumbnail.getHeight()){

        		resizedbitmap = Bitmap.createBitmap(
        				thumbnail, 
        				thumbnail.getWidth()/2 - thumbnail.getHeight()/2,
        		     0,
        		     thumbnail.getHeight(), 
        		     thumbnail.getHeight()
        		     );

        		}else{

        			resizedbitmap = Bitmap.createBitmap(
        					thumbnail,
        		     0, 
        		     thumbnail.getHeight()/2 - thumbnail.getWidth()/2,
        		     thumbnail.getWidth(),
        		     thumbnail.getWidth() 
        		     );
        		}
        	resizedbitmap_from_camera=Bitmap.createScaledBitmap(resizedbitmap, value_width, value_height, true);
        	savePhotoOnParse(resizedbitmap_from_camera);
            Drawable drawable = new BitmapDrawable(getResources(),resizedbitmap_from_camera);
            
            if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            	pic_here_profile.setBackgroundDrawable(drawable);
            } else {
            	pic_here_profile.setBackground(drawable);
            }
        	}
        
        if (requestCode == REQUEST_CODE_GALLERY && resultCode == Activity.RESULT_OK && data!= null){
            try
            {
            	 
                     Uri selectedImageUri = data.getData();
                     if (Build.VERSION.SDK_INT < 19) {
                         selectedImagePath = getPath(selectedImageUri);
                         bitmap = BitmapFactory.decodeFile(selectedImagePath);
                     }
                     else {
                         ParcelFileDescriptor parcelFileDescriptor;
                         try {
                             parcelFileDescriptor = getContentResolver().openFileDescriptor(selectedImageUri, "r");
                             FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
                             bitmap = BitmapFactory.decodeFileDescriptor(fileDescriptor);
                             parcelFileDescriptor.close();

                         } catch (FileNotFoundException e) {
                             e.printStackTrace();
                         } catch (IOException e) {
                             // TODO Auto-generated catch block
                             e.printStackTrace();
                         }
                     }
                 
             
                
               /* Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getContentResolver().query(
                                   selectedImage, filePathColumn, null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String filePath = cursor.getString(columnIndex);
                cursor.close();*/

               // bitmap = BitmapFactory.decodeFile(filePath);
                
                
                //stream=getContentResolver().openInputStream(data.getData());
               // bitmap = BitmapFactory.decodeStream(stream);
                int value_width_height = (int) dipToPixels(this, 70);
                if (bitmap != null){
                if (bitmap.getWidth() >= bitmap.getHeight()){

            		resizedbitmap = Bitmap.createBitmap(
            				bitmap, 
            				bitmap.getWidth()/2 - bitmap.getHeight()/2,
            		     0,
            		     bitmap.getHeight(), 
            		     bitmap.getHeight()
            		     );

            		}else{

            			resizedbitmap = Bitmap.createBitmap(
            					bitmap,
            		     0, 
            		     bitmap.getHeight()/2 - bitmap.getWidth()/2,
            		     bitmap.getWidth(),
            		     bitmap.getWidth() 
            		     );
            		}
                
                resizedbitmap_from_gallery=Bitmap.createScaledBitmap(resizedbitmap, value_width_height, value_width_height, true);
                savePhotoOnParse(resizedbitmap_from_gallery);
                Drawable drawable = new BitmapDrawable(getResources(),resizedbitmap_from_gallery);
                
               // pic_here_profile.setLayou
                if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                	pic_here_profile.setBackgroundDrawable(drawable);
                } else {
                	pic_here_profile.setBackground(drawable);
                }
                }
            }
            /*catch (FileNotFoundException e)
            {
                e.printStackTrace();
            }*/
        finally {
                if (stream!=null)
                    try{
                        stream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                   }
                   }
        		}
         }
	
        public String getPath(Uri uri) {
            if( uri == null ) {
                return null;
            }
            String[] projection = { MediaStore.Images.Media.DATA };
            Cursor cursor = managedQuery(uri, projection, null, null, null);
            if( cursor != null ){
                int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();
                return cursor.getString(column_index);
            }
            return uri.getPath();
    }
        
	public static float dipToPixels(Context context, float dipValue) {
	    DisplayMetrics metrics = context.getResources().getDisplayMetrics();
	    return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dipValue, metrics);
	}
	
	private void dispatchTakePictureIntent(int actionCode) {
	    Intent takePictureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
	    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
	    startActivityForResult(takePictureIntent, actionCode);}
	    else{
	    	Toast.makeText(ProfileActivity.this, "No camera find", Toast.LENGTH_LONG).show();
	    }
	}
	
	/*public Bitmap optimizeBitmap(Bitmap bmp){
		Bitmap bitmap;
		if (bmp.getWidth() >= bmp.getHeight()){

			bitmap = Bitmap.createBitmap(
    				bmp, 
    				bmp.getWidth()/2 - bmp.getHeight()/2,
    		     0,
    		     bmp.getHeight(), 
    		     bmp.getHeight()
    		     );

    		}else{

    			bitmap = Bitmap.createBitmap(
    					bmp,
    		     0, 
    		     bmp.getHeight()/2 - bmp.getWidth()/2,
    		     bmp.getWidth(),
    		     bmp.getWidth() 
    		     );
    		}
		return bitmap;
		
	}*/
	
	public void loadAvatar(){
		value_width_height_avatar = (int) ProfileActivity.dipToPixels(this, 70);
    	
        	Bitmap dnt_cellmask = BitmapFactory.decodeResource(this.getResources(),
                    R.drawable.dnt_cellmask);
        	Bitmap dnt_cellmask_resized = Bitmap.createScaledBitmap(dnt_cellmask, value_width_height_avatar, value_width_height_avatar, true);
        	img_user_picture_profile.setImageBitmap(dnt_cellmask_resized);
		ParseFile parseFile = currentUser.getParseFile("photo");
		  if(parseFile != null){
			 
			parseFile.getDataInBackground(new GetDataCallback() {
				@SuppressWarnings("deprecation")
				@SuppressLint("NewApi")
				public void done(byte[] data, ParseException e) {
					
				    if (e == null) {
				    
				    Log.d("ParseByte",data.toString());
				    Bitmap bmp = BitmapFactory.decodeByteArray(data, 0, data.length);
				    if (bmp.getWidth() >= bmp.getHeight()){

				    	resizedbitmap_avatar = Bitmap.createBitmap(
		        				bmp, 
		        				bmp.getWidth()/2 - bmp.getHeight()/2,
		        		     0,
		        		     bmp.getHeight(), 
		        		     bmp.getHeight()
		        		     );

		        		}else{

		        			resizedbitmap_avatar = Bitmap.createBitmap(
		        					bmp,
		        		     0, 
		        		     bmp.getHeight()/2 - bmp.getWidth()/2,
		        		     bmp.getWidth(),
		        		     bmp.getWidth() 
		        		     );
		        		}
				    Bitmap bmp_resized = Bitmap.createScaledBitmap(resizedbitmap_avatar, value_width_height_avatar, value_width_height_avatar, true);
				    
			    	Drawable drawable = new BitmapDrawable(getResources(),bmp_resized);
			    	 if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
			            	pic_here_profile.setBackgroundDrawable(drawable);
			            } else {
			            	pic_here_profile.setBackground(drawable);
			            }
				    } else {
				    	
				      e.printStackTrace();
				    }
				  }
			});
		  }
	}
	
	public void savePhotoOnParse(Bitmap bitmap){
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
		byte[] byteArray = stream.toByteArray();
		
		file = new ParseFile("picture.png", byteArray);
		file.saveInBackground(new SaveCallback() {
			
			@Override
			public void done(ParseException e) {
				if (e==null){
					Log.d("File","Saved");
					ParseUser currentUser = ParseUser.getCurrentUser();
					currentUser.put("photo", file);
					
					currentUser.saveInBackground(new SaveCallback() {
						
						@Override
						public void done(ParseException e) {
							if (e==null){
								Log.d("File","Saved on Parse");
							}
							else{
								
								e.printStackTrace();
								int err = e.getCode();
	                	    	Log.d("Error code:", Integer.toString(err));
	                	    	switch(err){
	                	    	default:
	                	    		tv_warning_profile_activity.setText(e.getMessage());
	                	    		tv_warning_profile_activity.setVisibility(View.VISIBLE);
	                	    	
	                	    	}
							}
							
						}
					});
				}
				else{
					Log.d("File","Not saved");
					e.printStackTrace();
				}
				
			}
		});
	}
	
	
	public void getInvitesSendByMe(int offset, int limit) {
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("offset", offset);
		params.put("limit", limit);
		//switch_profile.setEnabled(false);
		//progress_bar_profile.setVisibility(View.VISIBLE);
		tv_warning_profile_activity.setVisibility(View.GONE);
		
		 ParseCloud.callFunctionInBackground("getInvitesSendByMe", params, new FunctionCallback<JSONArray>() {
			    @SuppressLint("NewApi")
				public void done(JSONArray invite , ParseException e) {
			       if (e == null) {
			    	   
			    	   parseInvitesSendByMe(invite);
			    	   Boolean switch_enabled= listSendByMeReceived&& listSendToMeReceived;
			    	   //switch_profile.setEnabled(switch_enabled);
			    	   
			    	   
			    	  /* if (!firstListSendByMeReceived){
			    		   if (hashKeySendByMe != MD5Hash.md5Java(invite.toString())){
			    			   parseInvitesSendByMe(invite);
			    		   }
			    	   hashKeySendByMe =MD5Hash.md5Java(invite.toString());
			    	   firstListSendByMeReceived = true;
			    	   }*/
			    	   progress_bar_profile.setVisibility(View.GONE);
			    	  // if(!listSendByMeReceived){
			    	   if (mChecked){
			    		   //Log.e("Checked", "set");
			    	   lv_invites_profile.setAdapter(mAdapterSent);
			    	   }
			    	  // }
			    	   lv_invites_profile.setOnItemClickListener(new OnItemClickListener() {

						@Override
						public void onItemClick(AdapterView<?> adapterView, View view,
								int position, long id) {
							getBitmapToItemDialogSent(position);
							ProfileItemDialog profileItemDialog = ProfileItemDialog.newInstance(ProfileActivity.this, mAdapterSent.getItem(position).getName(), mAdapterSent.getItem(position).getDescription(), mAdapterSent.getItem(position).getUrlToPhoto(),  mLoadedImageSent, mAdapterSent.getItem(position).getCharityTitle(), mAdapterSent.getItem(position).getAmountPaid(),  mAdapterSent.getItem(position).getDateOfMessage(), ProfileItemDialog.SEND_BY_ME, ProfileItemDialog.ButtonsLayout.ONE_BUTTON);
							profileItemDialog.show(ProfileActivity.this);
							
						}
			    		   
					});
			         // Log.d("Invite",invite.toString());
			        //AppLog.e("Invites", invite.toString());
			        
			       }
			       else{
			    	   int err = e.getCode();
			    	   Log.d("Error code:", Integer.toString(err));
			    	   switch(err){
					    case ErrorCode.CONNECTION_FAILED:
					    	/*if (notifyDialogInternet != null){
					    		notifyDialogInternet.dismiss();
					    	}*/
					    	FragmentManager fm = getSupportFragmentManager();
					    	FragmentTransaction ft = fm.beginTransaction();
					    	notifyDialogInternet = NotifyDialog.newInstance(getString(R.string.no_internet_title), getString(R.string.no_internet_text), NotifyDialog.ButtonsLayout.ONE_BUTTON);
					    	notifyDialogInternet.show(ft, "dlg1", true);
					    	//tv_warning_profile_activity.setText("Problem with Internet. Please try again.");
					    	//tv_warning_profile_activity.setVisibility(View.VISIBLE);
           				break;
					    case ErrorCode.SCRIPT_ERROR:
					    	if (e.getMessage().equals("User are not authorized") ){
					    		ParseInstallation installation = ParseInstallation.getCurrentInstallation();
					            installation.remove("owner");
					            installation.saveInBackground(new SaveCallback() {
									
									@Override
									public void done(ParseException e) {
										if (e!=null){
											
											e.printStackTrace();
										}
										
									}
								});
					    		startActivity(new Intent(ProfileActivity.this, LoginActivity.class));
					    	}
					    break;
					    default:
            	    		tv_warning_profile_activity.setText(e.getMessage());
            	    		tv_warning_profile_activity.setVisibility(View.VISIBLE);
	            	    	}
			    	   switch_profile.setEnabled(true);
			    	   progress_bar_profile.setVisibility(View.GONE);
			    	   e.printStackTrace();
			       }
			   }
			});
	}
	
	
	
	public void getBitmapToItemDialogSent(int position){
		InvitesProfileAdapter adapter= (InvitesProfileAdapter) lv_invites_profile.getAdapterSource();
		value_width_height = (int) dipToPixels(ProfileActivity.this, 150);
		try{
    	if (!adapter.getItem(position).getUrlToPhoto().equals("")){
    		
        	
    	imageLoader.loadImage(adapter.getItem(position).getUrlToPhoto(), options, new SimpleImageLoadingListener() {
    		
    	    @Override
    	    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
    	    	mLoadedImageSent = loadedImage;
    	    	
    	    }
    	});
    	}
    	else{
    		mLoadedImageSent = null;
    	}
		}
		catch(IndexOutOfBoundsException e){
			mLoadedImageSent = null;
			e.printStackTrace();
		}
	}
	
	
	public void parseInvitesSendByMe(JSONArray jsonArray){
		try{
			if (jsonArray.length()>19){
				
				number_send_by_me=number_send_by_me+20;
				getInvitesSendByMe(number_send_by_me,number_send_by_me+20);
			}
			else{
				//if (list)
				listSendByMeReceived = true;
			}
			for(int i = 0; i < jsonArray.length(); i++){
				
		    	JSONObject jsonObject = jsonArray.getJSONObject(i);
		    	//Log.d("Json Object " + Integer.toString(i), jsonObject.toString());
		    	isPaid = jsonObject.getBoolean("isPaid");
		    	 Log.d("isPaid",isPaid.toString());
		    	 JSONObject event = jsonObject.getJSONObject("event");
		    	 try {
		    		 JSONObject toUser = jsonObject.getJSONObject("toUser");
		    		 nickname = toUser.getString("nickname");
				     Log.d("nickname", nickname);
				     email = toUser.getString("email");
		    		 try{
		    		 JSONObject photo = toUser.getJSONObject("photo");
		    		 urlToPhoto = photo.getString("url");
		    		 }
		    		 catch(JSONException e3){
		    			 urlToPhoto = "";
		    			 e3.printStackTrace();
		    		 }
				       
		    	 }
		    	 catch(JSONException e2){
		    		 try{
		    		 nickname = jsonObject.getString("toEmail");
		    		 Log.d("nickname1", nickname);
		    		 email = jsonObject.getString("toEmail");
		    		 }
		    		 catch (JSONException e){
		    			 nickname = "";
		    			 email = "";
		    		 }
		    		 urlToPhoto ="";
		    		 Log.d("To email",nickname);
		    		 e2.printStackTrace();
		    	 }
		    	 
		    	try {
		    		toPhone = jsonObject.getString("toPhone");
		    	}
		    	catch(JSONException e){
		    		toPhone = "";
		    		e.printStackTrace();
		    	}
		    	
		    	 try{
	    			 JSONObject charity = jsonObject.getJSONObject("charity");
	    			 title = charity.getString("title");
	    			 amountPaid = jsonObject.getInt("amountPaid");
	    		 }
	    		 catch(JSONException e){
	    			 title = "";
	    			 amountPaid = 0;
	    			 e.printStackTrace();
	    		 }
		    	
		       JSONObject sendDate = event.getJSONObject("sendDate");
		       
		       String date = sendDate.getString("iso");
		       
		       //String photo_url = photo.getString("url");
		       description = event.getString("inviteDescription");
		       SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		       
		       Date date_uncut= null;
			try {
				date_uncut = sdf.parse(date);
				 Log.d("Old Date", date_uncut + "");
			} catch (java.text.ParseException e1) {
				e1.printStackTrace();
			}
			Calendar cal = Calendar.getInstance();
			cal.setTime(date_uncut);
			 int year = cal.get(Calendar.YEAR);
			 int month = cal.get(Calendar.MONTH)+1;
			 int day = cal.get(Calendar.DAY_OF_MONTH);
			 String month_str=Integer.toString(month);
			 String day_str=Integer.toString(day);
			 if (month<10){
				 month_str = "0"+month;
			 }
			 if (day<10){
				 day_str = "0"+day;
			 }
			String dateOfMessage = month_str+"."+day_str+"."+year;
			
			
		       timeDiffrence = getDifference(date_uncut);
		       Log.d("Time Diffrence", timeDiffrence);
		       Log.e("Charity title",title);
		       invitesProfileModel = new InvitesProfileModel(nickname, description, timeDiffrence, urlToPhoto, isPaid, toPhone, false, "", title, dateOfMessage ,amountPaid);
		      /* if (listSendByMeReceived && mAdapterSent.getCount()>0){
		    	   mAdapterSentNew.add(invitesProfileModel);
		       }*/
		      // else{
		    	   mAdapterSent.add(invitesProfileModel);
		       //}		 
		       }
		    
		    
		} catch (JSONException e) {
		    e.printStackTrace();
		}
	}
	
	public static String getDifference(Date startTime) {
	    String timeDiff="";
	    if (startTime == null)
	        return "[corrupted]";
	    Calendar startDateTime= new GregorianCalendar();
	    startDateTime.setTime(startTime);
	    Log.d("Start Date", startDateTime+"");
	    Calendar endDateTime= new GregorianCalendar(TimeZone.getTimeZone("GMT+00:00"));
	    Log.d("End Date",endDateTime+"");
	  

	    long offset = endDateTime.get(Calendar.ZONE_OFFSET) +
	    		endDateTime.get(Calendar.DST_OFFSET);
	    long offset_end = startDateTime.get(Calendar.ZONE_OFFSET) +
	    		startDateTime.get(Calendar.DST_OFFSET);
	    long milliseconds2 = endDateTime.getTimeInMillis() + offset;
	    long milliseconds1 = startDateTime.getTimeInMillis()+ offset_end;
	    long diff = milliseconds2 - milliseconds1;
	    int diffhours = (int) (diff / (60 * 60 * 1000));
	    Log.d("diffhours",Integer.toString(diffhours));
	    int diffminutes = (int) (diff / (60 * 1000));
	    Log.d("diffminutes",Integer.toString(diffminutes));
	    int diffDays = (int) (diff / (24 * 60 * 60 * 1000));
	    Log.d("diffDays",Integer.toString(diffDays));
	    int diffSecond = (int) (diff / (1000));
	    Log.d("diffSecond",Integer.toString(diffSecond));
	    
	    int minutes = (int) (diffminutes - 60 * diffhours);
	    Log.d("Minutes",Integer.toString(minutes));
	    if (diffhours>=1 && diffhours<24){
	    timeDiff = diffhours+"h";
	    }
	    else if (diffhours>=24){
	    	 timeDiff = diffDays+"d";
	    }
	    else if (diffhours<1){
	    	timeDiff = diffminutes+"m";	
	    }
	   if (diffhours<1 && diffminutes<1){
	    	timeDiff = diffminutes+"m";
	    }
	    return timeDiff; 
	    
	}
	
	public void getInvitesSendToMeNotPaid(int offset, int limit) {
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("offset", offset);
		params.put("limit", limit);
		
		tv_warning_profile_activity.setVisibility(View.GONE);
		//progress_bar_profile.setVisibility(View.VISIBLE);
		 ParseCloud.callFunctionInBackground("getInvitesSendToMe", params, new FunctionCallback<JSONArray>() {
			    public void done(JSONArray invite , ParseException e) {
			       if (e == null) {
			    	   
			    	   parseInvitesSendToMeNotPaid(invite);
			    	  progress_bar_profile.setVisibility(View.GONE);
			    	  
			    	  
			    	  dropDownListProfile.setAdapterDropDownList(notificationProfileAdapter);
			         
			        //AppLog.e("Invites Received", invite.toString());
			        
			       }
			       else{
			    	   int err = e.getCode();
			    	   Log.d("Error code:", Integer.toString(err));
			    	   switch(err){
					    case ErrorCode.CONNECTION_FAILED:
					    	/*if (notifyDialogInternet != null){
					    		notifyDialogInternet.dismiss();
					    	}*/
					    	FragmentManager fm = getSupportFragmentManager();
					    	FragmentTransaction ft = fm.beginTransaction();
					    	notifyDialogInternet = NotifyDialog.newInstance(getString(R.string.no_internet_title), getString(R.string.no_internet_text), NotifyDialog.ButtonsLayout.ONE_BUTTON);
					    	notifyDialogInternet.show(ft, "dlg1", true);
					    	/*notifyDialogInternet = NotifyDialog.newInstance(getString(R.string.no_internet_title), getString(R.string.no_internet_text), NotifyDialog.ButtonsLayout.ONE_BUTTON);
					    	notifyDialogInternet.show(ProfileActivity.this);*/
					    	//tv_warning_profile_activity.setText("Problem with Internet. Please try again.");
					    	//tv_warning_profile_activity.setVisibility(View.VISIBLE);
           				break;
					    default:
            	    		tv_warning_profile_activity.setText(e.getMessage());
            	    		tv_warning_profile_activity.setVisibility(View.VISIBLE);
	            	    	}
			    	   progress_bar_profile.setVisibility(View.GONE);
			    	   
			    	   e.printStackTrace();
			       }
			   }
			});
	}
	
public void parseInvitesSendToMeNotPaid(JSONArray jsonArray){
		
		if (jsonArray.length()>19){
			number_send_to_me=number_send_to_me+20;
			getInvitesSendToMeNotPaid(number_send_to_me,number_send_to_me+20);
		}
		try{
			for(int i = 0; i < jsonArray.length(); i++){
				
		    	JSONObject jsonObject = jsonArray.getJSONObject(i);
		    	Log.d("Json Object " + Integer.toString(i), jsonObject.toString());
		    	isPaid = jsonObject.getBoolean("isPaid");
		    	 Log.d("isPaid",isPaid.toString());
		    	 String objectId = jsonObject.getString("objectId");
		    	 JSONObject event = jsonObject.getJSONObject("event");
		    	
		    		 JSONObject fromUser = event.getJSONObject("fromUser");
		    		 try{
		    		 JSONObject photo = fromUser.getJSONObject("photo");
		    		 urlToPhoto = photo.getString("url");}
		    		 catch(JSONException e){
		    			 e.printStackTrace();
		    			 urlToPhoto = "";
		    		 }
		    		 try {
		    			 isRead =jsonObject.getBoolean("isRead");
		    		 }
		    		 catch(JSONException e){
		    			 isRead = false;
		    			 e.printStackTrace();
		    		 }
				       nickname = fromUser.getString("nickname");
				       Log.d("nickname", nickname);
				       email = fromUser.getString("email");
				      
		    	
		    	
		    		/* nickname = jsonObject.getString("toEmail");
		    		 Log.d("nickname1", nickname);
		    		 email = jsonObject.getString("toEmail");
		    		 urlToPhoto ="";
		    		 Log.d("To email",nickname);
		    		 e2.printStackTrace();*/
		    	 
		    	 
		    	
		    	
		       JSONObject sendDate = event.getJSONObject("sendDate");
		       
		       String date = sendDate.getString("iso");
		       
		       //String photo_url = photo.getString("url");
		       description = event.getString("inviteDescription");
		       SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		       
		       Date date_uncut= null;
			try {
				date_uncut = sdf.parse(date);
				 Log.d("Old Date", date_uncut + "");
			} catch (java.text.ParseException e1) {
				e1.printStackTrace();
			}
			
		       timeDiffrence = getDifference(date_uncut);
		       Log.d("Time Diffrence", timeDiffrence);
		       invitesProfileModel = new InvitesProfileModel(nickname, description, timeDiffrence, urlToPhoto, isPaid, "", isRead, objectId, "","", 0);
					 if (!isPaid){
						 Log.e("Added to notif", "+1");
						 notificationProfileAdapter.add(invitesProfileModel);
					 }	 
		       }
		    
		    
		} catch (JSONException e) {
		    e.printStackTrace();
		}
	}

	public void getInvitesSendToMe(int offset, int limit) {
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("offset", offset);
		params.put("limit", limit);
		//switch_profile.setEnabled(false);
		tv_warning_profile_activity.setVisibility(View.GONE);
		//progress_bar_profile.setVisibility(View.VISIBLE);
		 ParseCloud.callFunctionInBackground("getInvitesSendToMe", params, new FunctionCallback<JSONArray>() {
			    public void done(JSONArray invite , ParseException e) {
			       if (e == null) {
			    	 
			    	   
			    	   
			    	  parseInvitesSendToMe(invite);
			    	  Boolean switch_enabled= listSendByMeReceived&& listSendToMeReceived;
			    	  //switch_profile.setEnabled(switch_enabled);  
			    	/*  if (!firstListSendToMeReceived){
			    		   if (hashKeySendToMe != MD5Hash.md5Java(invite.toString())){
			    			  
			    			   hashKeySendToMe = hashKeySendToMe +MD5Hash.md5Java(invite.toString());
			    		   }
			    		   hashKeySendToMe = MD5Hash.md5Java(invite.toString());
			    	   firstListSendToMeReceived = true;
			    	   }*/
			    	  
			    	  
			    	  progress_bar_profile.setVisibility(View.GONE);
			    	  
			    	  dropDownListProfile.setAdapterDropDownList(notificationProfileAdapter);
			    	  
			    	  if (!mChecked){
			    		  Log.e("Not Checked", "set");
			    		  // avoid duplication
			    		 /* ArrayList<InvitesProfileModel> al = new ArrayList<InvitesProfileModel>();
			    		  for (int i=0; i<mAdapterReceived.getCount(); i++){
			    			  InvitesProfileModel model = mAdapterReceived.getItem(i);
			    			  al.add(model);
			    			 
			    		  }
			    		  
							// add elements to al, including duplicates
							HashSet<InvitesProfileModel> hs = new HashSet<InvitesProfileModel>();
							hs.addAll(al);
							al.clear();
							al.addAll(hs);
							//sort without duplicate
							Collections.sort(al);
							
							mAdapterReceived.clear();
							for (int i=0; i<al.size(); i++){
								mAdapterReceived.add(al.get(i));
							}*/
			    	   lv_invites_profile.setAdapter(mAdapterReceived);
			    	  }
			    	   /*lv_invites_profile.setOnItemClickListener(new OnItemClickListener() {

							@Override
							public void onItemClick(AdapterView<?> adapterView, View view,
									int position, long id) {
								getBitmapToItemDialogReceived(position);
								//String donatedMessage = "Donated " + mAdapterReceived.getItem(position).getAmountPaid() + "$" + "for" + mAdapterReceived.getItem(position).getCharityTitle();
								Log.e("Charity dialog",mAdapterReceived.getItem(position).getCharityTitle()+ "charity title");
								ProfileItemDialog profileItemDialog = ProfileItemDialog.newInstance(ProfileActivity.this, mAdapterReceived.getItem(position).getName(), mAdapterReceived.getItem(position).getDescription(), mAdapterReceived.getItem(position).getUrlToPhoto(),  mLoadedImageReceived, mAdapterReceived.getItem(position).getCharityTitle(), mAdapterReceived.getItem(position).getAmountPaid(), ProfileItemDialog.SEND_TO_ME, ProfileItemDialog.ButtonsLayout.ONE_BUTTON);
								profileItemDialog.show(ProfileActivity.this);
								
							}
				    		   
						});*/
			         
			        AppLog.e("Invites Received", invite.toString());
			        
			       }
			       else{
			    	   int err = e.getCode();
			    	   Log.d("Error code:", Integer.toString(err));
			    	   Log.d("Error message", e.getMessage());
			    	   switch(err){
					    case ErrorCode.CONNECTION_FAILED:
					    	/*if (notifyDialogInternet != null){
					    		notifyDialogInternet.dismiss();
					    	}*/
					    	FragmentManager fm = getSupportFragmentManager();
					    	FragmentTransaction ft = fm.beginTransaction();
					    	notifyDialogInternet = NotifyDialog.newInstance(getString(R.string.no_internet_title), getString(R.string.no_internet_text), NotifyDialog.ButtonsLayout.ONE_BUTTON);
					    	notifyDialogInternet.show(ft, "dlg1", true);
					    	/*notifyDialogInternet = NotifyDialog.newInstance(getString(R.string.no_internet_title), getString(R.string.no_internet_text), NotifyDialog.ButtonsLayout.ONE_BUTTON);
					    	notifyDialogInternet.show(ProfileActivity.this);*/
					    	//tv_warning_profile_activity.setText("Problem with Internet. Please try again.");
					    	//tv_warning_profile_activity.setVisibility(View.VISIBLE);
           				break;
					    case ErrorCode.SCRIPT_ERROR:
					    	if (e.getMessage().equals("User are not authorized") ){
					    		ParseInstallation installation = ParseInstallation.getCurrentInstallation();
					            installation.remove("owner");
					            installation.saveInBackground(new SaveCallback() {
									
									@Override
									public void done(ParseException e) {
										if (e!=null){
											
											e.printStackTrace();
										}
										
									}
								});
					    		startActivity(new Intent(ProfileActivity.this, LoginActivity.class));
					    	}
           				break;
					    default:
            	    		tv_warning_profile_activity.setText(e.getMessage());
            	    		tv_warning_profile_activity.setVisibility(View.VISIBLE);
	            	    	}
			    	   progress_bar_profile.setVisibility(View.GONE);
			    	   switch_profile.setEnabled(true);
			    	   e.printStackTrace();
			       }
			   }
			});
	}
	
	public void getBitmapToItemDialogReceived(int position){
		InvitesProfileAdapter adapter= (InvitesProfileAdapter) lv_invites_profile.getAdapterSource();
		value_width_height = (int) dipToPixels(ProfileActivity.this, 150);
		try{
    	if (!adapter.getItem(position).getUrlToPhoto().equals("")){
    		
    	imageLoader.loadImage(adapter.getItem(position).getUrlToPhoto(), options, new SimpleImageLoadingListener() {
    		
    	    @Override
    	    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
    	    	mLoadedImageReceived = loadedImage;
    	    	
    	    }
    	});
    	}
    	else{
    		mLoadedImageReceived = null;
    	}
		}
		catch(IndexOutOfBoundsException e){
			mLoadedImageReceived = null;
			e.printStackTrace();
		}
	}
	
	public void parseInvitesSendToMe(JSONArray jsonArray){
		
		if (jsonArray.length()>19){
			number_send_to_me=number_send_to_me+20;
			getInvitesSendToMe(number_send_to_me,number_send_to_me+20);
		}
		else{
			listSendToMeReceived = true;
		}
		try{
			for(int i = 0; i < jsonArray.length(); i++){
				
		    	JSONObject jsonObject = jsonArray.getJSONObject(i);
		    	//Log.d("Json Object " + Integer.toString(i), jsonObject.toString());
		    	isPaid = jsonObject.getBoolean("isPaid");
		    	 Log.d("isPaid",isPaid.toString());
		    	 
		    	 JSONObject event = jsonObject.getJSONObject("event");
		    	
		    		 JSONObject fromUser = event.getJSONObject("fromUser");
		    		 try{
		    		 JSONObject photo = fromUser.getJSONObject("photo");
		    		 urlToPhoto = photo.getString("url");}
		    		 catch(JSONException e){
		    			 e.printStackTrace();
		    			 urlToPhoto = "";
		    		 }
		    		 
		    		 try{
		    			 JSONObject charity = jsonObject.getJSONObject("charity");
		    			 title = charity.getString("title");
		    			 amountPaid = jsonObject.getInt("amountPaid");
		    		 }
		    		 catch(JSONException e){
		    			 title = "";
		    			 amountPaid = 0;
		    			 e.printStackTrace();
		    		 }
				       nickname = fromUser.getString("nickname");
				       Log.d("nickname", nickname);
				       email = fromUser.getString("email");
		    		 
		    	
		    	
		    		/* nickname = jsonObject.getString("toEmail");
		    		 Log.d("nickname1", nickname);
		    		 email = jsonObject.getString("toEmail");
		    		 urlToPhoto ="";
		    		 Log.d("To email",nickname);
		    		 e2.printStackTrace();*/
		    	 
		    	 
		    	
		    	
		       JSONObject sendDate = event.getJSONObject("sendDate");
		       
		       String date = sendDate.getString("iso");
		       
		       //String photo_url = photo.getString("url");
		       description = event.getString("inviteDescription");
		       SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		       
		       Date date_uncut= null;
			try {
				date_uncut = sdf.parse(date);
				 Log.d("Old Date", date_uncut + "");
			} catch (java.text.ParseException e1) {
				e1.printStackTrace();
			}
			
			Calendar cal = Calendar.getInstance();
			cal.setTime(date_uncut);
			 int year = cal.get(Calendar.YEAR);
			 int month = cal.get(Calendar.MONTH)+1;
			 int day = cal.get(Calendar.DAY_OF_MONTH);
			 String month_str=Integer.toString(month);
			 String day_str=Integer.toString(day);
			 if (month<10){
				 month_str = "0"+month;
			 }
			 if (day<10){
				 day_str = "0"+day;
			 }
			String dateOfMessage = month_str+"."+day_str+"."+year;
			
			
		       timeDiffrence = getDifference(date_uncut);
		       Log.d("Time Diffrence", timeDiffrence);
		       Log.e("Charity title",title+" charity");
		       
		       invitesProfileModel = new InvitesProfileModel(nickname, description, timeDiffrence, urlToPhoto, isPaid, "", false, "", title,dateOfMessage, amountPaid);
		      /* if (listSendToMeReceived && mAdapterReceived.getCount()>0){
		    	   mAdapterReceivedNew.add(invitesProfileModel);
		       }*/
		      // else{
					 mAdapterReceived.add(invitesProfileModel);
					
		      // }
					 if (!isPaid){
						 notificationProfileAdapter.add(invitesProfileModel);
					 }
					 
		       }
		    
		    
		} catch (JSONException e) {
		    e.printStackTrace();
		}
	}
	
	public void getNotPaidInvitesCountSendToMe() {
		HashMap<String, Object> params = new HashMap<String, Object>();
		 ParseCloud.callFunctionInBackground("getNotPaidInvitesCountSendToMe", params, new FunctionCallback<Integer>() {
			    public void done(Integer countSendToMe , ParseException e) {
			       if (e == null) {
			        AppLog.e("Count Send To Me", countSendToMe.toString());
			        if (countSendToMe>0){
			        	mCountSendToMe = countSendToMe;
			        	countReceived = true;
			        	 sPref = getPreferences(MODE_PRIVATE);
			        	    int count_load = sPref.getInt(currentUser.getEmail(), 0);
			        	    Log.e("Count load",count_load+"");
			        	    if (count_load != countSendToMe){
			        	    	 tv_count_not_paid.setVisibility(View.VISIBLE);
			 			        tv_count_not_paid.setText(countSendToMe.toString());
			        	    }
			        	
			       }
			       }
			       else{
			    	   e.printStackTrace();
			       }
			   }  
			});
		 
	}
}
