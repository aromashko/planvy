package ua.regionit.planvy.ui;


import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.holoeverywhere.app.Activity;
import org.holoeverywhere.widget.Button;
import org.holoeverywhere.widget.EditText;
import org.holoeverywhere.widget.ProgressBar;
import org.holoeverywhere.widget.TextView;
import org.json.JSONArray;
import org.json.JSONObject;

import ua.regionit.planvy.ErrorCode;
import ua.regionit.planvy.R;
import ua.regionit.planvy.ui.dialog.ChangePhoneDialog;
import ua.regionit.planvy.ui.dialog.NotifyDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.facebook.FacebookRequestError;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.model.GraphUser;
import com.facebook.widget.ProfilePictureView;
import com.flurry.android.FlurryAgent;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.parse.LogInCallback;
import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseFile;
import com.parse.ParseInstallation;
import com.parse.ParseUser;
import com.parse.SaveCallback;



public class LoginActivity extends Activity implements OnClickListener{

	TextView tv_forgot_your_password, register, tv_warning;
	EditText edt_password, edt_email ;
	Button btn_enter, btn_skip_step,enter_with_facebook, btn_save_file_on_parse, go_to_settings, go_to_profile, go_to_share;
	String email, password;
	ProgressBar login_progress;
	ParseUser currentUser;
	public Dialog progressDialogFacebook;
	Bitmap userFacebookAvatar;
	ProfilePictureView profPicture;
	InputStream is = null;
    JSONObject jObj = null;
    String json = "";
    JSONArray contacts = null;
    ParseFile file;
    String facebook_email;
    ChangePhoneDialog changePhoneDialog;
    public static boolean skip_this_step = false;
    public static int REQUEST_CODE_FB =3;
    SharedPreferences sPref;
    final String LOGIN_EMAIL = "login_email";
    DisplayImageOptions options;
    protected ImageLoader imageLoader = ImageLoader.getInstance();
    
    
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		FlurryAgent.logEvent("Login", true);
		//ParseInstallation.getCurrentInstallation().saveInBackground();
		initUi();
		initListener();
		SpannableString forgor_text = new SpannableString("Forgot password?");
		forgor_text.setSpan(new UnderlineSpan(), 0, forgor_text.length(), 0);
		tv_forgot_your_password.setText(forgor_text);
		
		SpannableString register_text = new SpannableString("Register");
		register_text.setSpan(new UnderlineSpan(), 0, register_text.length(), 0);
		register.setText(register_text);
		
		SpannableString sp = new SpannableString("Enter with Facebook");
		sp.setSpan(new StyleSpan(Typeface.BOLD), 10, sp.length(), 0);		
		enter_with_facebook.setText(sp);
		
		loadLoginEmail();
		
		/*edt_email.setText("andrewlotar9@mail.ru");
		edt_password.setText("90333");*/
		
		options = new DisplayImageOptions.Builder()
		.showImageForEmptyUri(R.drawable.ic_empty)
		.showImageOnFail(R.drawable.ic_error)
		.resetViewBeforeLoading(true)
		.cacheInMemory(true)
		.cacheOnDisc(true)
		.imageScaleType(ImageScaleType.EXACTLY)
		.bitmapConfig(Bitmap.Config.RGB_565)
		.considerExifParams(true)
		.displayer(new FadeInBitmapDisplayer(300))
		.build();
		
		
	
	}
	
	@Override
	protected void onStart()
	{
		super.onStart();
		FlurryAgent.onStartSession(this, "XWHZX63GBXQKSR9KJGQ9");
	}
	
	@Override
	protected void onStop()
	{
		super.onStop();		
		FlurryAgent.onEndSession(this);
	}


	public void initUi(){
		tv_forgot_your_password = (TextView) findViewById(R.id.tv_forgot_your_password);
		register = (TextView) findViewById(R.id.register);
		enter_with_facebook = (Button) findViewById(R.id.enter_with_facebook);
		edt_password = (EditText) findViewById(R.id.edt_password);
		edt_email =(EditText) findViewById(R.id.edt_name);
		btn_enter = (Button) findViewById(R.id.btn_enter);
		btn_skip_step = (Button) findViewById(R.id.btn_skip_step);
		tv_warning =(TextView) findViewById(R.id.tv_warning);
		login_progress = (ProgressBar) findViewById(R.id.login_progress);
		
	}
	
	public void initListener(){
		register.setOnClickListener(this);
		tv_forgot_your_password.setOnClickListener(this);
		enter_with_facebook.setOnClickListener(this);
		btn_enter.setOnClickListener(this);
		btn_skip_step.setOnClickListener(this);
		
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()){
		case R.id.register:
			skip_this_step = false;
				startActivity(new Intent(LoginActivity.this,RegistrationActivity.class));	
			break;
		case R.id.tv_forgot_your_password:
			startActivity(new Intent(LoginActivity.this,FindAccountActivity.class));
			break;
		case R.id.enter_with_facebook:
			skip_this_step = false;
			onLoginButtonClicked();
			break;
		case R.id.btn_enter:
			skip_this_step = false;
			getTextFromLoginView();
			if (email.equals("")){
				tv_warning.setText("Email cannot be blank");
				tv_warning.setVisibility(View.VISIBLE);
				break;
			}
			if (password.equals("")){
				tv_warning.setText("Password cannot be blank");
				tv_warning.setVisibility(View.VISIBLE);
				break;
				
			}
			
			login_progress.setVisibility(View.VISIBLE);
        	ParseUser.logInInBackground(email, password, new LogInCallback() {
        		  public void done(ParseUser user, ParseException e) {
        		    if (user != null) {
        		    	 Map<String, String> login_params = new HashMap<String, String>();
    	            	 login_params.put("Login with ", "credentials"); 
    			         FlurryAgent.logEvent("User login", login_params);
    			        
        		    	ParseInstallation installation = ParseInstallation.getCurrentInstallation();
        		    	installation.put("owner",ParseUser.getCurrentUser());
        		    	installation.saveInBackground(new SaveCallback() {
							
							@Override
							public void done(ParseException e) {
								if (e!= null){
									e.printStackTrace();
								}
								
							}
						});
        		    	
        		    	
        		    	
        		    	login_progress.setVisibility(View.GONE);
        		    	//subscribeToNotification();
        		    	tv_warning.setVisibility(View.GONE);
        		    	saveLoginEmail();
        		    	startActivity(new Intent(LoginActivity.this, InspireActivity.class));
        		    	
        		    } else {
        		    	login_progress.setVisibility(View.GONE);
        		    	int err = e.getCode();
        		    	Log.d("Error code:", Integer.toString(err));
            	    	switch(err){
            	    	case ErrorCode.OBJECT_NOT_FOUND:
            	    		final NotifyDialog dialogFragment = NotifyDialog.newInstance("","Invalid email or password", NotifyDialog.ButtonsLayout.ONE_BUTTON);
					        dialogFragment.setListener(new DialogInterface.OnClickListener() {

					            @Override
					            public void onClick(DialogInterface dialog, int which) {
					                if (which == Dialog.BUTTON_POSITIVE) {
					                	dialogFragment.dismiss();
					                	
					                }
					            }
					        });
					        dialogFragment.show(LoginActivity.this);
//            	    		tv_warning.setText("You entered an incorrect username or password");
//            	    		tv_warning.setVisibility(View.VISIBLE);
            				break;
            	    	case ErrorCode.CONNECTION_FAILED:
            	    		tv_warning.setText("Problem with Internet. Please try again.");
            	    		tv_warning.setVisibility(View.VISIBLE);
            				break;
            	    	default:
            	    		tv_warning.setText(e.getMessage());
            	    		tv_warning.setVisibility(View.VISIBLE);
            	    	}
            	    	e.printStackTrace();
            	    	String message = e.getMessage();
            	    	Log.d("Error Message:",message);
        		    }
        		  }
        		});
        	
			break;
		case R.id.btn_skip_step:
			skip_this_step = true;
			startActivity(new Intent(LoginActivity.this,InspireActivity.class));
			overridePendingTransition(R.anim.slide_left_in, R.anim.slide_left_out);
			break;
		/*case R.id.go_to_settings:
			startActivity(new Intent(LoginActivity.this, SettingsActivity.class));
		break;
		case R.id.go_to_profile:
			startActivity(new Intent(LoginActivity.this, ProfileActivity.class));
			break;*/
		
		}
		
	}
	
	public void getTextFromLoginView(){
		email = edt_email.getText().toString();
		password = edt_password.getText().toString();
	}
	
	
	@Override
	public void onBackPressed() {
		// android.os.Process.killProcess(android.os.Process.myPid());
		super.onBackPressed();
		
	}

	void saveLoginEmail() {
	    sPref = getPreferences(MODE_PRIVATE);
	    Editor ed = sPref.edit();
	    ed.putString(LOGIN_EMAIL, edt_email.getText().toString());
	    ed.commit();
	  }
	  
	  void loadLoginEmail() {
	    sPref = getPreferences(MODE_PRIVATE);
	    String savedText = sPref.getString(LOGIN_EMAIL, "");
	    edt_email.setText(savedText);
	  }
	
	
	private void onLoginButtonClicked() {
		  
	     progressDialogFacebook = ProgressDialog.show(
	             LoginActivity.this, "", "Logging in...", true);
	                     
	     List<String> permissions = Arrays.asList("basic_info", "email" );
	    
	     ParseFacebookUtils.logIn(permissions, LoginActivity.this, REQUEST_CODE_FB, new LogInCallback() {
	         @Override
	         public void done(ParseUser user, ParseException err) {
	        	 if (err!= null){
	        		Log.e("Exception", err.getMessage());
	        	 }
	             //progressDialogFacebook.dismiss();
	             if (user == null) {
	            	 Log.d("User","Null");
	            	// Log.d("Err",err.toString());
	             } else if (user.isNew()) {
	            	 Map<String, String> login_params = new HashMap<String, String>();
	            	 login_params.put("Login with ", "facebook"); 
			          FlurryAgent.logEvent("User login", login_params);
	                 Log.e("User","New");
	                 Session session = ParseFacebookUtils.getSession();
	         		if (session != null && session.isOpened()) {
	         			
	                 registerWithFacebook();
	         		}
	         		
	             } else {
	            	/* Session session = ParseFacebookUtils.getSession();
	            	 if (session != null && session.isOpened()) {
	            	 registerWithFacebook();
	            	 }*/
	            	  Log.e("User","Not new");
	            	 Map<String, String> login_params = new HashMap<String, String>();
	            	 login_params.put("Login with ", "facebook"); 
			         FlurryAgent.logEvent("User login", login_params);	            	  
	            	  updateDataWithFacebook();
	            	  /*user.put("phone", "+390987555772");
	            	  user.saveInBackground(new SaveCallback() {
							
							@Override
							public void done(ParseException e) {
								if (e==null){
									Log.d("User","Updated");
								}
								else{
									
									e.printStackTrace();
								}
								
								
							}
						});*/
	            	 
	             }
	         }
	     });
	 }

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		
		if (requestCode == REQUEST_CODE_FB){
			if(resultCode == RESULT_OK){
				Log.e("Result", "ok");
				 //progressDialogFacebook.dismiss();
			}
			if(resultCode == RESULT_CANCELED){
				 progressDialogFacebook.dismiss();
				Log.e("Result", "cancaled");
			}
			if(resultCode == RESULT_FIRST_USER){
				 //progressDialogFacebook.dismiss();
				Log.e("Result", "first user");
			}
	 Log.e("Result Code", resultCode+" code");
	}
		if (requestCode ==REQUEST_CODE_FB){
			
	 ParseFacebookUtils.finishAuthentication(requestCode, resultCode, data);}
		super.onActivityResult(requestCode, resultCode, data);
	}
	
	 
	private void registerWithFacebook() {
		Request request = Request.newMeRequest(ParseFacebookUtils.getSession(),
				new Request.GraphUserCallback() {
					@Override
					public void onCompleted(GraphUser user, Response response) {
						
						if (user != null) {
								currentUser = ParseUser.getCurrentUser();
							
								Log.d("User Id",user.getId());
								Log.d("Email",user.asMap().get("email")+"");
								Log.d("name",user.getName());
								
								String id = user.getId();
								downloadAvatar(id);
								
								ParseInstallation installation = ParseInstallation.getCurrentInstallation();
		        		    	installation.put("owner",currentUser);
		        		    	installation.saveInBackground(new SaveCallback() {
									
									@Override
									public void done(ParseException e) {
										if (e!=null){
											e.printStackTrace();
										}
										
									}
								});
								
								currentUser.setEmail((String) user.asMap().get("email"));
								currentUser.setUsername((String) user.asMap().get("email"));
								
								currentUser.put("nickname", user.getName());
								currentUser.saveInBackground(new SaveCallback() {
									
									@Override
									public void done(ParseException e) {
										if (e==null){
											Log.d("User","Registred");
											startChangePhoneDialog();
											//startActivity(new Intent(LoginActivity.this,InspireActivity.class));
										}
										else{
											progressDialogFacebook.dismiss();
											int err = e.getCode();
									    	   Log.d("Error code:", Integer.toString(err));
									    	   Log.d("Error message", e.getMessage());
				                	    	switch(err){
				                	    	case ErrorCode.EMAIL_TAKEN:
				                	    		tv_warning.setText("The email " +facebook_email + " is already taken by user of Planvy. If you have account on Planvy please log in with him or try to recover password on planvy account.");
				                	    		tv_warning.setVisibility(View.VISIBLE);
				                				break;
				                	    	/*case ErrorCode.VALIDATION_ERROR:
				                	    		tv_warning.setText("The email " +facebook_email + " is already taken by user of Planvy. If you have account on Planvy please log in with him or try to recover password on planvy account.");
				                	    		tv_warning.setVisibility(View.VISIBLE);
				                				break;*/
				                	    	default:
				                	    		tv_warning.setText(e.getMessage());
				                	    		tv_warning.setVisibility(View.VISIBLE);
				                	    	break;
				                	    	}
										}
									}
								});

						} else if (response.getError() != null) {
							if ((response.getError().getCategory() == FacebookRequestError.Category.AUTHENTICATION_RETRY)
									|| (response.getError().getCategory() == FacebookRequestError.Category.AUTHENTICATION_REOPEN_SESSION)) {
								Log.e("MyApp",
										"The facebook session was invalidated.");
								onLogoutButtonClicked();
							} else {
								Log.e("MyaApp",
										"Some other error: "
												+ response.getError()
														.getErrorMessage());
							}
						}
					}
				});
		request.executeAsync();

	}
	
	
	public void startChangePhoneDialog(){
		changePhoneDialog = ChangePhoneDialog.newInstance("Please provide phone number\nin order to\nsend and receive invitations", ChangePhoneDialog.ButtonsLayout.TWO_BUTTONS);
		changePhoneDialog.setListener(new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				 if (which == Dialog.BUTTON_POSITIVE) {
					 if (progressDialogFacebook !=null){
							progressDialogFacebook.dismiss();}
					 changePhoneDialog.dismiss();
					 startActivity(new Intent(LoginActivity.this, InspireActivity.class));
				 }
				 else if (which == Dialog.BUTTON_NEGATIVE){
					 if (progressDialogFacebook !=null){
							progressDialogFacebook.dismiss();}
					 changePhoneDialog.dismiss();
					 startActivity(new Intent(LoginActivity.this, InspireActivity.class));
					 
				 }
				
			}
			
		});
		
		changePhoneDialog.show(LoginActivity.this);
	}
	private void updateDataWithFacebook() {
		
				Request request = Request.newMeRequest(ParseFacebookUtils.getSession(),
						new Request.GraphUserCallback() {
							@Override
							public void onCompleted(GraphUser user, Response response) {
								Log.e("Response", response.toString());
								if (user != null) {
									currentUser = ParseUser.getCurrentUser();
									if (currentUser == null)
									{
										Log.e("CurrentUser","null");
										 //registerWithFacebook();
										 //return;
									}
									else{
										Log.e("CurrentUser","not null");
										/*currentUser.deleteInBackground(new DeleteCallback() {
											
											@Override
											public void done(ParseException e) {
												if (e != null){
													e.printStackTrace();
												}
												
											}
										});*/
										/*try{
										//Log.e("Current email", currentUser.getObjectId());
										Log.e("Current email", currentUser.getSessionToken());
										}
										catch(NullPointerException e){
											e.printStackTrace();
										}*/
										
										//Log.e("CurrentUser Email",currentUser.getEmail());
									}
									/*if (!ParseFacebookUtils.isLinked(currentUser)) {
										  ParseFacebookUtils.link(currentUser, MyLoginActivity.this, new SaveCallback() {
										    @Override
										    public void done(ParseException ex) {
										      if (ParseFacebookUtils.isLinked(currentUser)) {
										        Log.d("MyApp", "Woohoo, user logged in with Facebook!");
										      }
										    }
										  });
										}*/
									
									/*ChangePhoneDialog changePhoneDialog = ChangePhoneDialog.newInstance("Please provide phone number\nin order to\nsend and receive invitations", ChangePhoneDialog.ButtonsLayout.TWO_BUTTONS);
									changePhoneDialog.show(LoginActivity.this);*/
									
									
									ParseInstallation installation = ParseInstallation.getCurrentInstallation();
			        		    	installation.put("owner",currentUser);
			        		    	installation.saveInBackground(new SaveCallback() {
										
										@Override
										public void done(ParseException e) {
											if(e != null){
												e.printStackTrace();
											}
											
										}
									});
									
									facebook_email = (String) user.asMap().get("email");
									String facebookName =user.getName();
									currentUser.setEmail(facebook_email);
									currentUser.setUsername(facebook_email);
									currentUser.put("nickname", facebookName);
									String id = user.getId();
									downloadAvatar(id);
									currentUser.saveInBackground(new SaveCallback() {
										
										@Override
										public void done(ParseException e) {
											if (e==null){
												Log.d("User","Updated");
											}
											else{
												
												e.printStackTrace();
											}
											
											
										}
									});
									Log.d("id", user.getId());
									Log.d("Email",(String) user.asMap().get("email"));
									Log.d("User Name", user.getName());
									
									if (currentUser.getString("phone") == null){
										Log.d("phone","null");
										
										startChangePhoneDialog();
										
									}
									else if (currentUser.getString("phone").equals("")){
										Log.d("phone","not null");
										startChangePhoneDialog();
										}
									else{
										if (progressDialogFacebook !=null){
											progressDialogFacebook.dismiss();}
									 startActivity(new Intent(LoginActivity.this, InspireActivity.class));
									}
									
								
									
									
								}
								else if (response.getError() != null) {
									if ((response.getError().getCategory() == FacebookRequestError.Category.AUTHENTICATION_RETRY)
											|| (response.getError().getCategory() == FacebookRequestError.Category.AUTHENTICATION_REOPEN_SESSION)) {
										Log.e("MyApp",
												"The facebook session was invalidated.");
										onLogoutButtonClicked();
									} else {
										Log.e("MyaApp",
												"Some other error: "
														+ response.getError()
																.getErrorMessage());
									}
								}
							}
				});
				request.executeAsync();
				
	}

	
	

	@Override
	protected void onPause() {
		super.onPause();
	}


	private void onLogoutButtonClicked() {
		// Log the user out
		ParseUser.logOut();
		Toast.makeText(LoginActivity.this, "Log Out", Toast.LENGTH_SHORT).show();
		// Go to the login view
		startLoginActivity();
	}
	private void startLoginActivity() {
		Intent intent = new Intent(this, LoginActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
	}
	public static Bitmap getBitmap(String url) {
	    try {
	        InputStream is = (InputStream) new URL(url).getContent();
	        Bitmap d = BitmapFactory.decodeStream(is);
	        is.close();
	        return d;
	    } catch (Exception e) {
	        return null;
	    }
	}
	private synchronized void downloadAvatar(String id) {
		imageLoader.loadImage("http://graph.facebook.com/"+id+"/picture?type=large", options, new SimpleImageLoadingListener(){
			
			@Override
			public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
				String message = null;
				switch (failReason.getType()) {
					case IO_ERROR:
						message = "Input/Output error";
						break;
					case DECODING_ERROR:
						message = "Image can't be decoded";
						break;
					case NETWORK_DENIED:
						message = "Downloads are denied";
						break;
					case OUT_OF_MEMORY:
						message = "Out Of Memory error";
						break;
					case UNKNOWN:
						message = "Unknown error";
						break;
				}
				Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();

			}
			
			public void onLoadingComplete(String imageUri, View view, Bitmap result) {
				Bitmap resizedbitmap_avatar;
				if (result.getWidth() >= result.getHeight()){

			    	resizedbitmap_avatar = Bitmap.createBitmap(
			    			result, 
			    			result.getWidth()/2 - result.getHeight()/2,
	        		     0,
	        		     result.getHeight(), 
	        		     result.getHeight()
	        		     );

	        		}else{

	        			resizedbitmap_avatar = Bitmap.createBitmap(
	        					result,
	        		     0, 
	        		     result.getHeight()/2 - result.getWidth()/2,
	        		     result.getWidth(),
	        		     result.getWidth() 
	        		     );
	        		}
			    //Bitmap bmp_resized = Bitmap.createScaledBitmap(resizedbitmap_avatar, value_width_height_avatar, value_width_height_avatar, true);
				ByteArrayOutputStream stream = new ByteArrayOutputStream();
				resizedbitmap_avatar.compress(Bitmap.CompressFormat.PNG, 100, stream);
				byte[] byteArray = stream.toByteArray();
				
				file = new ParseFile("picture.png", byteArray);
				file.saveInBackground(new SaveCallback() {
					
					@Override
					public void done(ParseException e) {
						if (e==null){
							Log.d("File","Saved");
							ParseUser currentUser = ParseUser.getCurrentUser();
							currentUser.put("photo", file);
							currentUser.saveInBackground(new SaveCallback() {
								
								@Override
								public void done(ParseException e) {
									if (e==null){
										Log.d("File","Saved on Parse");
									}
									else{
										e.printStackTrace();
										int err = e.getCode();
			                	    	Log.d("Error code:", Integer.toString(err));
			                	    	switch(err){
			                	    	case ErrorCode.EMAIL_TAKEN:
			                	    		tv_warning.setText("The email " +facebook_email + " is already taken by user of Planvy. If you have account on Planvy please log in with him or try to recover password on planvy account.");
			                	    		tv_warning.setVisibility(View.VISIBLE);
			                				break;
			                	    	default:
			                	    		tv_warning.setText(e.getMessage());
			                	    		tv_warning.setVisibility(View.VISIBLE);
			                	    	
			                	    	}
									}
									
								}
							});
						}
						else{
							Log.d("File","Not saved");
							e.printStackTrace();
						}
						
					}
				});
			}
		});
	}
	
	
}
