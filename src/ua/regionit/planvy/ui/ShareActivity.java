package ua.regionit.planvy.ui;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.holoeverywhere.app.Activity;
import org.holoeverywhere.widget.Button;
import org.holoeverywhere.widget.TextView;

import ua.regionit.planvy.Constants;
import ua.regionit.planvy.R;
import ua.regionit.planvy.assist.FacebookEventObserver;
import ua.regionit.planvy.assist.TwitterEventObserver;
import ua.regionit.planvy.ui.dialog.ChangePhoneDialog;
import ua.regionit.planvy.ui.dialog.FacebookShareTextDialog;
import ua.regionit.planvy.ui.dialog.NotifyDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ShareCompat;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.plus.GooglePlusUtil;
import com.nostra13.socialsharing.common.AuthListener;
import com.nostra13.socialsharing.facebook.FacebookFacade;
import com.nostra13.socialsharing.twitter.TwitterFacade;


public class ShareActivity extends Activity implements OnClickListener{
	
	private static final int TWEET_POST = 1;
	TextView shareText;
	Button facebook_btn, twitter_btn, google_btn, mail_btn;
	public static String shareMsg = "",shareTxt="";
	ImageView back;
	
	private FacebookFacade facebook;
	private FacebookEventObserver facebookEventObserver;
	
	private TwitterFacade twitter;
	private TwitterEventObserver twitterEventObserver;

	private Map<String, String> actionsMap;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.share_donation);
		FlurryAgent.logEvent("Share", true);
		initUi();
		initListener();
		
		facebook = new FacebookFacade(this, Constants.FACEBOOK_APP_ID);
		facebookEventObserver = FacebookEventObserver.newInstance();
		
		twitter = new TwitterFacade(this, Constants.TWITTER_CONSUMER_KEY, Constants.TWITTER_CONSUMER_SECRET);
		twitterEventObserver = TwitterEventObserver.newInstance();
		
		Intent intent = getIntent();
		String amount_paid = intent.getStringExtra("AMOUNT_PAID");
		String description = intent.getStringExtra("DESCRIPTION");
		
		shareMsg = "Friends, I just gave $"+ amount_paid +" on www.planvy.com to \""+description+"\". I challenge you to give more than I did.";
		shareTxt = "Thank you for giving $"+ amount_paid +" to \"" + description+ "\".";
		
		Bundle bundle = getIntent().getExtras();
		if (bundle != null) {
			actionsMap = new HashMap<String, String>();
			actionsMap.put(Constants.FACEBOOK_SHARE_ACTION_NAME, Constants.FACEBOOK_SHARE_ACTION_LINK);
		
		}
		shareText.setText(shareTxt);
	}
	
	@Override
	protected void onStart()
	{
		super.onStart();
		FlurryAgent.onStartSession(this, "XWHZX63GBXQKSR9KJGQ9");
	}
	
	@Override
	protected void onStop()
	{
		super.onStop();		
		FlurryAgent.onEndSession(this);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(resultCode == RESULT_OK){
			twitterDialog();
		}
	}
	
	public void initUi(){
		shareText = (TextView)findViewById(R.id.donate_text);
		facebook_btn = (Button)findViewById(R.id.share_by_facebook);
		twitter_btn = (Button)findViewById(R.id.share_by_twitter);
		google_btn = (Button)findViewById(R.id.share_by_google);
		mail_btn = (Button)findViewById(R.id.share_by_email);
		back = (ImageView)findViewById(R.id.imgback_share);
	}
	
	public void initListener(){
		facebook_btn.setOnClickListener(this);
		twitter_btn.setOnClickListener(this);
		google_btn.setOnClickListener(this);
		mail_btn.setOnClickListener(this);
		back.setOnClickListener(this);
	}
	
	@Override
	public void onClick(View v) {
		switch (v.getId()){
			case R.id.share_by_facebook:
				final Map<String, String> shareParamsFacebook = new HashMap<String, String>();
				shareParamsFacebook.put("Shared via ", "facebook"); 
		        
				if (facebook.isAuthorized()) {
					publishMessage();

					FlurryAgent.logEvent("User did share", shareParamsFacebook);
				} else {
					// Start authentication dialog and publish message after successful authentication
					facebook.authorize(new AuthListener() {
						@Override
						public void onAuthSucceed() {
							publishMessage();
							FlurryAgent.logEvent("User did share", shareParamsFacebook);
						}

						@Override
						public void onAuthFail(String error) { // Do noting
						}
					});
				}
			break;
			
			case R.id.share_by_twitter:
				Map<String, String> shareParamsTwitter = new HashMap<String, String>();
				shareParamsTwitter.put("Shared via ", "Twitter"); 
				FlurryAgent.logEvent("User did share", shareParamsTwitter);
				Intent tweetIntent = new Intent(Intent.ACTION_SEND);
				tweetIntent.putExtra(Intent.EXTRA_TEXT, shareMsg);
				tweetIntent.setType("text/plain");

				PackageManager packManager = getPackageManager();
				List<ResolveInfo> resolvedInfoList = packManager.queryIntentActivities(tweetIntent,  PackageManager.MATCH_DEFAULT_ONLY);

				boolean resolved = false;
				for(ResolveInfo resolveInfo: resolvedInfoList){
				    if(resolveInfo.activityInfo.packageName.startsWith("com.twitter.android")){
				        tweetIntent.setClassName(
				            resolveInfo.activityInfo.packageName, 
				            resolveInfo.activityInfo.name );
				        resolved = true;
				        break;
				    }
				}
				if(resolved){
					try{
					    startActivityForResult(tweetIntent, TWEET_POST);
					}catch(Throwable e){
						tweetIntent.setClassName("com.twitter.android", "com.twitter.applib.PostActivity");
					    try{
					        startActivityForResult(tweetIntent, TWEET_POST);
					    }catch(Throwable e2){
					    }}
//				    twitterDialog();
				}else{
				    Toast.makeText(this, "Twitter app isn't found", Toast.LENGTH_LONG).show();
				    String url = "http://www.twitter.com/intent/tweet?url=YOURURL&text="+shareMsg;
				    Intent i = new Intent(Intent.ACTION_VIEW);
				    i.setData(Uri.parse(url));
				    startActivity(i);
				    twitterDialog();
				}
			break;
				
			case R.id.share_by_google:
				Map<String, String> shareParamsGoogle = new HashMap<String, String>();
				shareParamsGoogle.put("Shared via ", "Google"); 
				FlurryAgent.logEvent("User did share", shareParamsGoogle);
				final int errorCode = GooglePlusUtil.checkGooglePlusApp(this);
				if (errorCode == GooglePlusUtil.SUCCESS) {
				// Invoke the ACTION_SEND intent to share to Google+ with attribution.
				Intent shareIntent = ShareCompat.IntentBuilder.from(this)
				                                .setText(shareMsg)
				                                .setType("text/plain")
				                                .getIntent()
				                                .setPackage("com.google.android.apps.plus");
				    startActivity(shareIntent);
				} else {
				    Toast.makeText(this, "Google plus not installed", Toast.LENGTH_LONG).show();
				}
			break;
				
			case R.id.share_by_email:
				Map<String, String> shareParamsEmail = new HashMap<String, String>();
				shareParamsEmail.put("Shared via ", "Email"); 
				FlurryAgent.logEvent("User did share", shareParamsEmail);
				Intent mailIntent = new Intent(android.content.Intent.ACTION_SEND);
				mailIntent.setType("message/rfc822");
				mailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Planvy.com");
				mailIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareMsg);
		        startActivity(Intent.createChooser(mailIntent, "Share with:"));
			break;
			case R.id.imgback_share:
				Intent i = new Intent(ShareActivity.this,ProfileActivity.class);
				i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(i);
			break;
		}
	}
	
	private void publishMessage() {
		
			FacebookShareTextDialog facebookShareTextDialog = FacebookShareTextDialog.newInstance("Share on Facebook", ChangePhoneDialog.ButtonsLayout.TWO_BUTTONS);
			facebookShareTextDialog.setListener(new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					 if (which == Dialog.BUTTON_POSITIVE) {
						 facebook.publishMessage(FacebookShareTextDialog.edt_change_share_text.getText().toString(), "https://www.planvy.com", "Planvy.com", "iOS and Android app available now!", "http://planvy.com/images/planvy_logo.png", actionsMap);
						 NotifyDialog dialogFragment = NotifyDialog.newInstance("","Shared on facebook", NotifyDialog.ButtonsLayout.ONE_BUTTON);
					        dialogFragment.setListener(new DialogInterface.OnClickListener() {

					            @Override
					            public void onClick(DialogInterface dialog, int which) {
					                if (which == Dialog.BUTTON_POSITIVE) {
//					                	finish();
					                }
					            }
					        });
					        dialogFragment.show(ShareActivity.this);				 
					 }
					 				
				}
				
			});
			facebookShareTextDialog.show(ShareActivity.this);
		
		
	}
	
	private void twitterDialog(){
		NotifyDialog dialogFragment = NotifyDialog.newInstance("","Shared on twitter", NotifyDialog.ButtonsLayout.ONE_BUTTON);
        dialogFragment.setListener(new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == Dialog.BUTTON_POSITIVE) {

                }
            }
        });
        dialogFragment.show(ShareActivity.this);
	}
	
//	@Override
//	  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//		if (resultCode == RESULT_OK) {
//	    twitterDialog();
//	  }
//	}
}
