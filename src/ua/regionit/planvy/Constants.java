package ua.regionit.planvy;

/**
 * @author Sergey Tarasevich (nostra13[at]gmail[dot]com)
 */
public final class Constants {
	private Constants() {
	}

//	public static final String FACEBOOK_APP_ID = "___YOUR_FACEBOOK_APP_ID___";
//
//	public static final String TWITTER_CONSUMER_KEY = "___YOUR_TWITTER_CONSUMER_KEY___";
//	public static final String TWITTER_CONSUMER_SECRET = "___YOUR_TWITTER_CONSUMER_SECRET___";

	public static final String FACEBOOK_APP_ID = "225684934264737";

	public static final String TWITTER_CONSUMER_KEY = "bBlUYgSqNS3yXXOtIus8Q";
	public static final String TWITTER_CONSUMER_SECRET = "LfOKHeag8GFApZvIdqY7CsSV03S7jj4RqwOxIvHvB4";

	public static final String FACEBOOK_SHARE_MESSAGE = "Look at this great App!";
	public static final String FACEBOOK_SHARE_LINK = "https://www.planvy.com";
	public static final String FACEBOOK_SHARE_LINK_NAME = "Visit our web-site!";
	public static final String FACEBOOK_SHARE_LINK_DESCRIPTION = "iOS and Android app available now...";
	public static final String FACEBOOK_SHARE_PICTURE = "http://planvy.com/images/planvy_logo.png";
	public static final String FACEBOOK_SHARE_ACTION_NAME = "Android Simple Social Sharing";
	public static final String FACEBOOK_SHARE_ACTION_LINK = "https://www.planvy.com";
	public static final String FACEBOOK_SHARE_IMAGE_CAPTION = "Great image";

	public static final String TWITTER_SHARE_MESSAGE = "Look at this great App!";

	public static final class Extra {
		public static final String POST_MESSAGE = "ua.regionit.planvy.POST_MESSAGE";
		public static final String POST_LINK = "ua.regionit.planvy.POST_LINK";
		public static final String POST_PHOTO = "ua.regionit.planvy.POST_PHOTO";
		public static final String POST_PHOTO_DATE = "ua.regionit.planvy.POST_PHOTO_DATE";
		public static final String POST_LINK_NAME = "ua.regionit.planvy.POST_LINK_NAME";
		public static final String POST_LINK_DESCRIPTION = "ua.regionit.planvy.POST_LINK_DESCRIPTION";
		public static final String POST_PICTURE = "ua.regionit.planvy.POST_PICTURE";
	}
}
