
package ua.regionit.planvy;

import android.util.Log;

public class AppLog {
	public static final boolean DEBUG = true;
    private static final int MAX_LOG_SIZE = 4000;

    public static void e(String tag, String msg) {
        if (DEBUG) {
            for (int i = 0; i <= msg.length() / MAX_LOG_SIZE; i++) {
                int start = i * MAX_LOG_SIZE;
                int end = (i + 1) * MAX_LOG_SIZE;
                end = end > msg.length() ? msg.length() : end;
                Log.e(tag, msg.substring(start, end));
            }
        }
    }

    public static void e(String tag, Throwable tr) {
        if (DEBUG) {
            Log.e(tag, tr.getMessage(), tr);
        }
    }

}
